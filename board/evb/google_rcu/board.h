/**
*********************************************************************************************************
*               Copyright(c) 2020, Realtek Semiconductor Corporation. All rights reserved.
*********************************************************************************************************
* @file      board.h
* @brief     header file of Keypad demo.
* @details
* @author    chenjie_jin
* @date      2020-02-24
* @version   v1.3
* *********************************************************************************************************
*/


#ifndef _BOARD_H_
#define _BOARD_H_

#include "otp_config.h"

#ifdef __cplusplus
extern "C" {
#endif

#define REMOTE_G10                            1
#define REMOTE_G20                            2

/*******************************************************
*                 Log Config
*******************************************************/
#define IS_RELEASE_VERSION      0  /* 1: close log   0: open log */

/*******************************************************
*                 GAP Parameter Config
*******************************************************/
#define VID  0x005D
#define PID  0x0302

/*******************************************************
*                 OTA Config
*******************************************************/
#define SUPPORT_TEMP_COMBINED_OTA   1  /* set 1 to support temp combined OTA */
#define SUPPORT_SILENT_OTA          1  /* set 1 to enable silent OTA */
#define SUPPORT_NORMAL_OTA          0  /* set 1 to enable normal OTA */

#define DFU_BUFFER_CHECK_ENABLE     1  /* set 1 to enable buffer check feature */
#define DFU_TEMP_BUFFER_SIZE        2048  /* dfu max buffer size */

#define SUPPORT_OTA_PROTOCOL_TYPE_CHARACTERISTIC  1

#define OTA_AES_FLAG_EN  1

/*******************************************************
*                 RCU Feature Config
*******************************************************/
#define FEAUTRE_SUPPORT_FLASH_2_BIT_MODE                1  /* set 1 to enable flash 2-bit mode */

#define FEATURE_SUPPORT_REPORT_MULTIPLE_HID_KEY_CODES   1  /* set 1 to support to report multiple HID key codes */

#define FEATURE_SUPPORT_NO_ACTION_DISCONN               0  /* set 1 to enable NO_ACTION_DISCONN after timeout */

#define FEATURE_SUPPORT_REMOVE_LINK_KEY_BEFORE_PAIRING  0 /* set 1 to enable remove link key before paring */

#define FEATURE_SUPPORT_MP_TEST_MODE                    0  /* set 1 to enable MP test */

#define FEATURE_SUPPORT_PRIVACY                         1  /* set 1 to enable privacy feature */

#define FEATURE_SUPPORT_DATA_LENGTH_EXTENSION           1  /* set 1 to enable data length extension feature */

#define FEATURE_SUPPORT_HIDS_CHAR_AUTHEN_REQ            0  /* set 1 to enable HIDS char authentication request */

#define FEATURE_SUPPORT_KEY_LONG_PRESS_PROTECT          1  /*set 1 to stop scan when press one key too long*/

#define FEAUTRE_SUPPORT_IR_OVER_BLE                     1  /* set 1 to support IR over BLE feature */

/*******************************************************
*                 FTL Address Config
*******************************************************/
/*note: if the FTL size is 16K, the range of FTL address here should be less than 3056*/
#define FTL_MP_TEST_PARAMS_BASE_ADDR        0
#define FTL_MP_TEST_PARAMS_MAX_LEN          12

#define FTL_WAKEUP_KEY_COUNT_ADDR           24
#define FTL_WAKEUP_KEY_COUNT_LEN            4

#define APP_STATIC_RANDOM_ADDR_OFFSET       32
#define FTL_RPA_LEN                         8

#define FTL_IR_TABLE_BASE_ADDR              256
#define FTL_IR_TABLE_ITEM_MAX_LEN           512
#define FTL_IR_TABLE_ITEM_HEADER_LEN        4

#define FTL_IR_TABLE_SEC_0_ADDR              (FTL_IR_TABLE_BASE_ADDR + FTL_IR_TABLE_ITEM_MAX_LEN * 0)
#define FTL_IR_TABLE_SEC_1_ADDR              (FTL_IR_TABLE_BASE_ADDR + FTL_IR_TABLE_ITEM_MAX_LEN * 1)
#define FTL_IR_TABLE_SEC_2_ADDR              (FTL_IR_TABLE_BASE_ADDR + FTL_IR_TABLE_ITEM_MAX_LEN * 2)
#define FTL_IR_TABLE_SEC_3_ADDR              (FTL_IR_TABLE_BASE_ADDR + FTL_IR_TABLE_ITEM_MAX_LEN * 3)
#define FTL_IR_TABLE_SEC_4_ADDR              (FTL_IR_TABLE_BASE_ADDR + FTL_IR_TABLE_ITEM_MAX_LEN * 4)
/*******************************************************
*                 MP Test Config
*******************************************************/

#if FEATURE_SUPPORT_MP_TEST_MODE

#define MP_TEST_FTL_PARAMS_TEST_MODE_FLG_OFFSET (FTL_MP_TEST_PARAMS_BASE_ADDR)
#define MP_TEST_FTL_PARAMS_TEST_MODE_FLG_LEN    4

#define MP_TEST_FTL_PARAMS_LOCAL_BD_ADDR_OFFSET (MP_TEST_FTL_PARAMS_TEST_MODE_FLG_OFFSET + MP_TEST_FTL_PARAMS_TEST_MODE_FLG_LEN)
#define MP_TEST_FTL_PARAMS_LOCAL_BD_ADDR_LEN    8

#define MP_TEST_MODE_SUPPORT_HCI_UART_TEST      1  /* set 1 to support HCI Uart Test Mode */
#define MP_TEST_MODE_SUPPORT_DATA_UART_TEST     1  /* set 1 to support Data Uart Test Mode */
#define MP_TEST_MODE_SUPPORT_SINGLE_TONE_TEST   1  /* set 1 to support SingleTone Test Mode */
#define MP_TEST_MODE_SUPPORT_FAST_PAIR_TEST     1  /* set 1 to support Fast Pair Test */
#define MP_TEST_MODE_SUPPORT_AUTO_K_RF          0  /* set 1 to support Auto K RF */
#define MP_TEST_MODE_SUPPORT_DATA_UART_DOWNLOAD 0  /* set 1 to support Data UART download */

#define MP_TEST_MODE_TRIG_BY_GPIO               0x0001  /* GPIO signal while power on to trigger MP test mode */
#define MP_TEST_MODE_TRIG_BY_COMBINE_KEYS       0x0002  /* Combine keys to trigger MP test mode */

#define MP_TEST_MODE_TRIG_SEL                   (MP_TEST_MODE_TRIG_BY_GPIO | MP_TEST_MODE_TRIG_BY_COMBINE_KEYS)

#if (MP_TEST_MODE_TRIG_SEL & MP_TEST_MODE_TRIG_BY_GPIO)

#define MP_TEST_TRIG_PIN_1          P0_6
#define MP_TEST_TRIG_PIN_2          P3_3

#endif

#if MP_TEST_MODE_SUPPORT_DATA_UART_TEST
#define MP_TEST_UART_TX_PIN         P3_0
#define MP_TEST_UART_RX_PIN         P3_1
#endif

#endif

/*******************************************************
*                 Voice Module Config
*******************************************************/
#define SUPPORT_VOICE_FEATURE               1  /* set 1 to support voice feature */

#if SUPPORT_VOICE_FEATURE

#define SUPPORT_MIC_BIAS_OUTPUT             1  /* set 1 to enable MIC bias output */

#define SUPPORT_SW_EQ                       1  /* set 1 to support software equalizer */

#define SUPPORT_UART_DUMP_FEATURE           0  /* set 1 to support UART dump PCM data */

#if SUPPORT_UART_DUMP_FEATURE
#define VOICE_UART_TEST_TX          P0_0
#endif

/* mic type definitions */
#define AMIC_TYPE                   0  /* Analog MIC */
#define DMIC_TYPE                   1  /* digital MIC */

/* amic input type definitions */
#define AMIC_INPUT_TYPE_DIFF        0  /* differential analog input */
#define AMIC_INPUT_TYPE_SINGLE      1  /* single-end analog input */

/* dmic data latch type definitions */
#define DMIC_DATA_LATCH_FALLING_EDGE    0  /* falling clock edge */
#define DMIC_DATA_LATCH_RISING_EDGE     1  /* rising clock edge */

/* codec sample rate type definitions */
#define CODEC_SAMPLE_RATE_8KHz        1  /* 8KHz codec sample rate */
#define CODEC_SAMPLE_RATE_16KHz       2  /* 16KHz codec sample rate */

#define CODEC_SAMPLE_RATE_SEL       CODEC_SAMPLE_RATE_16KHz

/* single MIC configuration */
#define VOICE_MIC0_TYPE             AMIC_TYPE

#if (VOICE_MIC0_TYPE == AMIC_TYPE)
#define AMIC_MIC_N_PIN              P2_6  /* MIC_N is fixed to P2_6 */
#define AMIC_MIC_P_PIN              P2_7  /* MIC_P is fixed to P2_7 */
#define AMIC_MIC_BIAS_PIN           H_0   /* MICBIAS is fixed to H_0 */
#define AMIC_INPUT_TYPE_SEL         AMIC_INPUT_TYPE_DIFF
#elif (VOICE_MIC0_TYPE == DMIC_TYPE)
#define DMIC_CLK_PIN                P2_0  /* DMIC clock PIN, support PINMUX */
#define DMIC_DATA_PIN               P2_1  /* DMIC data PIN, support PINMUX */
#define DMIC0_DATA_LATCH_TYPE       DMIC_DATA_LATCH_FALLING_EDGE
#endif


#define VOICE_FLOW_SEL              ATV_GOOGLE_VOICE_FLOW

/* voice encode type */
#define SW_IMA_ADPCM_ENC            1  /* software IMA/DVI adpcm encode */


/* ATV_GOOGLE_VOICE_FLOW must use SW_IMA_ADPCM_ENC */
#define VOICE_ENC_TYPE              SW_IMA_ADPCM_ENC


/* ATV Google voice version type */
#define ATV_VERSION_0_4             1  /* v0.4 */
#define ATV_VERSION_1_0             2  /* v1.0 */

/* ATV Google voice assistant interaction type */
#define ATV_INTERACTION_MODEL_ON_REQUEST            1  /* on request */
#define ATV_INTERACTION_MODEL_PRESS_TO_TALK         2  /* PTT */
#define ATV_INTERACTION_MODEL_HOLD_TO_TALK          3  /* HTT */

#define ATV_VOICE_VERSION           ATV_VERSION_1_0  /* default version is v1.0 */
#define ATV_VOICE_INTERACTION_MODEL ATV_INTERACTION_MODEL_HOLD_TO_TALK  /* default assistant interaction */

#endif

/*******************************************************
*                 DTM Config
*******************************************************/
#define DATA_UART_TX_PIN    P3_0
#define DATA_UART_RX_PIN    P3_1

#define MODE_PIN P4_1

/*******************************************************
*                 RCU Keyscan Config
*******************************************************/
/* if set KEYSCAN_FIFO_LIMIT larger than 3, need to caution ghost key issue */
#define KEYSCAN_FIFO_LIMIT    3  /* value range from 1 to 8 */

/* keypad row and column */
#define KEYPAD_MAX_ROW_SIZE           6
#define KEYPAD_MAX_COLUMN_SIZE        7

#define KEYPAD_MAX_ROW_SIZE_G20       6
#define KEYPAD_MAX_COLUMN_SIZE_G20    7

#define KEYPAD_MAX_ROW_SIZE_G10       4
#define KEYPAD_MAX_COLUMN_SIZE_G10    7

#define ROW0                  P0_4
#define ROW1                  P3_3
#define ROW2                  P4_3
#define ROW3                  P0_2
#define ROW4                  P0_1
#define ROW5                  P0_0

#define COLUMN0               P2_2
#define COLUMN1               P5_2
#define COLUMN2               P5_1
#define COLUMN3               P3_2
#define COLUMN4               P0_5
#define COLUMN5               P0_6
#define COLUMN6               P4_0

/*******************************************************
*                 IR Module Config
*******************************************************/
#define SUPPORT_IR_TX_FEATURE               1

#if SUPPORT_IR_TX_FEATURE

#define IR_SEND_PIN             P2_3

#endif

/*******************************************************
*              LED Module Config
*******************************************************/
#define SUPPORT_LED_INDICATION_FEATURE          1

#if SUPPORT_LED_INDICATION_FEATURE

#define  LED_PWM_CTRL_FEATURE

#define  LED_R_PIN          P2_4
#define  LED_G_PIN          P2_5

#define  LED_ACTIVE_LEVEL   1  /* 0: low level active, 1: high level active */

#endif

/*******************************************************
*              Battery Module Config
*******************************************************/
#define SUPPORT_BAT_DETECT_FEATURE  1  /* set 1 to enable batttery detect feature */

#if SUPPORT_BAT_DETECT_FEATURE

#define SUPPORT_BAT_KEY_PRESS_DETECT_FEATURE 1  /* set 1 to enable key press battery detect feature */

#define SUPPORT_BAT_PERIODIC_DETECT_FEATURE  0  /* set 1 to enable periodic battery detect feature */

/* Note: make sure BAT_ENTER_NORMAL_MODE_THRESHOLD > BAT_ENTER_NORMAL_MODE_THRESHOLD */
#define BAT_ENTER_LOW_POWER_THRESHOLD     2000  /* enter low power mode threshold, unit: mV */
#define BAT_ENTER_NORMAL_MODE_THRESHOLD   2200  /* enter normal mode threshold, unit: mV */
#define BAT_ENTER_OTA_MODE_THRESHOLD      2500  /* enter ota mode threshold, unit: mV. 2.5v = 30%*/

#define SUPPORT_BAT_LPC_FEATURE     1  /* set 1 to enable LPC */
#if SUPPORT_BAT_LPC_FEATURE
#define BAT_LPC_COMP_VALUE          LPC_1840_mV /* lpc detect bat threshold value */
#endif

#endif

/*******************************************************
*              Buzzer Module Config
*******************************************************/
#define SUPPORT_BUZZER_FEATURE  1  /* set 1 to enable buzzer feature */

#if SUPPORT_BUZZER_FEATURE
#define BUZZER_PWM_OUTPUT_PIN    P4_2
#endif

/*******************************************************
*                 DLPS Module Config
*******************************************************/
#define DLPS_EN             1

/** @defgroup IO Driver Config
  * @note user must config it firstly!! Do not change macro names!!
  * @{
  */
/* if use user define dlps enter/dlps exit callback function */
#define USE_USER_DEFINE_DLPS_EXIT_CB    1
#define USE_USER_DEFINE_DLPS_ENTER_CB   1

/* if use any peripherals below, #define it 1 */
#define USE_I2C0_DLPS       0
#define USE_I2C1_DLPS       0
#if (ROM_WATCH_DOG_ENABLE == 1)
#define USE_TIM_DLPS        1 //must be 1 if enable watch dog
#else
#define USE_TIM_DLPS        0
#endif
#define USE_QDECODER_DLPS   0
#define USE_IR_DLPS         1
#define USE_ADC_DLPS        0
#define USE_CTC_DLPS        0
#define USE_SPI0_DLPS       0
#define USE_SPI1_DLPS       0
#define USE_SPI2W_DLPS      0
#define USE_KEYSCAN_DLPS    1
#define USE_GPIO_DLPS       1
#define USE_CODEC_DLPS      0
#define USE_I2S0_DLPS       0
#define USE_ENHTIM_DLPS     0
#define USE_UART0_DLPS      0
#define USE_UART1_DLPS      0

/* do not modify USE_IO_DRIVER_DLPS macro */
#define USE_IO_DRIVER_DLPS  (USE_I2C0_DLPS | USE_I2C1_DLPS | USE_TIM_DLPS | USE_QDECODER_DLPS\
                             | USE_IR_DLPS | USE_ADC_DLPS | USE_CTC_DLPS | USE_SPI0_DLPS\
                             | USE_SPI1_DLPS | USE_SPI2W_DLPS | USE_KEYSCAN_DLPS\
                             | USE_GPIO_DLPS | USE_CODEC_DLPS | USE_I2S0_DLPS\
                             | USE_ENHTIM_DLPS | USE_UART0_DLPS | USE_UART1_DLPS\
                             | USE_USER_DEFINE_DLPS_ENTER_CB\
                             | USE_USER_DEFINE_DLPS_EXIT_CB)


#ifdef __cplusplus
}
#endif

#endif  /* _BOARD_H_ */

