/******************************************************************************
 * @file     app_custom.c
 *
 * @brief    for TLSR chips
 *
 * @author   public@telink-semi.com;
 * @date     Sep. 30, 2010
 *
 * @attention
 *
 *  Copyright (C) 2019-2020 Telink Semiconductor (Shanghai) Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *****************************************************************************/
#include "board.h"
#include "string.h"
#include "app_custom.h"
#include "frm_vpk_log.h"
#include "frm_vpk_hal_sflash.h"
#include "key_handle.h"
#include "rcu_application.h"

const u8 *pREMOTE_G10 = (u8 *)("RemoteG10");
const u8 *pREMOTE_G20 = (u8 *)("RemoteG20");

#define LAYOUT_MASK_BIT0               0X01
#define LAYOUT_MASK_BIT1               0X02
#define LAYOUT_MASK_BIT2               0X04
#define LAYOUT_MASK_BIT5               0X20


void app_custom_ui_layout(u8 ui_layout)
{
    uint8_t row_id;
    uint8_t col_id;

    frm_vpk_print("ui_layout=%x\r\n", ui_layout);
    if ((ui_layout == 0xff) || (ui_layout == 0))
    {
        return;
    }
    if (app_global_data.device_type == REMOTE_G10)
    {
        if ((ui_layout & LAYOUT_MASK_BIT0))
        {
            row_id = Kb_Map_devicelayout_Correspend_Index_G10[0] / KEYPAD_MAX_COLUMN_SIZE_G10;
            col_id = Kb_Map_devicelayout_Correspend_Index_G10[0] % KEYPAD_MAX_COLUMN_SIZE_G10;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_Dashboard;
            Ir_Override_Table_Index_G10[Ir_Table_Correspend_Index_G10[0]] = KEY_ID_Dashboard;
        }
        else
        {
            row_id = Kb_Map_devicelayout_Correspend_Index_G10[0] / KEYPAD_MAX_COLUMN_SIZE_G10;
            col_id = Kb_Map_devicelayout_Correspend_Index_G10[0] % KEYPAD_MAX_COLUMN_SIZE_G10;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_Settings;
            Ir_Override_Table_Index_G10[Ir_Table_Correspend_Index_G10[0]] = KEY_ID_Settings;
        }

        if ((ui_layout & LAYOUT_MASK_BIT1))
        {
            row_id = Kb_Map_devicelayout_Correspend_Index_G10[1] / KEYPAD_MAX_COLUMN_SIZE_G10;
            col_id = Kb_Map_devicelayout_Correspend_Index_G10[1] % KEYPAD_MAX_COLUMN_SIZE_G10;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_Live;
            Ir_Override_Table_Index_G10[Ir_Table_Correspend_Index_G10[1]] = KEY_ID_Live;
        }
        else
        {
            row_id = Kb_Map_devicelayout_Correspend_Index_G10[1] / KEYPAD_MAX_COLUMN_SIZE_G10;
            col_id = Kb_Map_devicelayout_Correspend_Index_G10[1] % KEYPAD_MAX_COLUMN_SIZE_G10;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_Guide;
            Ir_Override_Table_Index_G10[Ir_Table_Correspend_Index_G10[1]] = KEY_ID_Guide;
        }

        if ((ui_layout & 0x18) == 0x08)
        {
            row_id = Kb_Map_devicelayout_Correspend_Index_G10[2] / KEYPAD_MAX_COLUMN_SIZE_G10;
            col_id = Kb_Map_devicelayout_Correspend_Index_G10[2] % KEYPAD_MAX_COLUMN_SIZE_G10;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_Profile;
            Ir_Override_Table_Index_G10[Ir_Table_Correspend_Index_G10[2]] = KEY_ID_Profile;
        }
        else if ((ui_layout & 0x18) == 0x10)
        {
            row_id = Kb_Map_devicelayout_Correspend_Index_G10[2] / KEYPAD_MAX_COLUMN_SIZE_G10;
            col_id = Kb_Map_devicelayout_Correspend_Index_G10[2] % KEYPAD_MAX_COLUMN_SIZE_G10;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_AllApps;
            Ir_Override_Table_Index_G10[Ir_Table_Correspend_Index_G10[2]] = KEY_ID_AllApps;
        }
    }
    else
    {
        if ((ui_layout & LAYOUT_MASK_BIT0))
        {
            row_id = Kb_Map_devicelayout_Correspend_Index_G20[0] / KEYPAD_MAX_COLUMN_SIZE_G20;
            col_id = Kb_Map_devicelayout_Correspend_Index_G20[0] % KEYPAD_MAX_COLUMN_SIZE_G20;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_Dashboard;
            Ir_Override_Table_Index_G20[Ir_Table_Correspend_Index_G20[0]] = KEY_ID_Dashboard;
        }
        else
        {
            row_id = Kb_Map_devicelayout_Correspend_Index_G20[0] / KEYPAD_MAX_COLUMN_SIZE_G20;
            col_id = Kb_Map_devicelayout_Correspend_Index_G20[0] % KEYPAD_MAX_COLUMN_SIZE_G20;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_Settings;
            Ir_Override_Table_Index_G20[Ir_Table_Correspend_Index_G20[0]] = KEY_ID_Settings;
        }

        if ((ui_layout & LAYOUT_MASK_BIT1))
        {
            row_id = Kb_Map_devicelayout_Correspend_Index_G20[1] / KEYPAD_MAX_COLUMN_SIZE_G20;
            col_id = Kb_Map_devicelayout_Correspend_Index_G20[1] % KEYPAD_MAX_COLUMN_SIZE_G20;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_Live;
            Ir_Override_Table_Index_G20[Ir_Table_Correspend_Index_G20[1]] = KEY_ID_Live;
        }
        else
        {
            row_id = Kb_Map_devicelayout_Correspend_Index_G20[1] / KEYPAD_MAX_COLUMN_SIZE_G20;
            col_id = Kb_Map_devicelayout_Correspend_Index_G20[1] % KEYPAD_MAX_COLUMN_SIZE_G20;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_Guide;
            Ir_Override_Table_Index_G20[Ir_Table_Correspend_Index_G20[1]] = KEY_ID_Guide;
        }

        if ((ui_layout & LAYOUT_MASK_BIT2))
        {
            row_id = Kb_Map_devicelayout_Correspend_Index_G20[2] / KEYPAD_MAX_COLUMN_SIZE_G20;
            col_id = Kb_Map_devicelayout_Correspend_Index_G20[2] % KEYPAD_MAX_COLUMN_SIZE_G20;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_TEXT;
            Ir_Override_Table_Index_G20[Ir_Table_Correspend_Index_G20[2]] = KEY_ID_TEXT;
        }
        else
        {
            row_id = Kb_Map_devicelayout_Correspend_Index_G20[2] / KEYPAD_MAX_COLUMN_SIZE_G20;
            col_id = Kb_Map_devicelayout_Correspend_Index_G20[2] % KEYPAD_MAX_COLUMN_SIZE_G20;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_Subtitles;
            Ir_Override_Table_Index_G20[Ir_Table_Correspend_Index_G20[2]] = KEY_ID_Subtitles;
        }

        if ((ui_layout & 0x18) == 0x08)
        {
            row_id = Kb_Map_devicelayout_Correspend_Index_G20[3] / KEYPAD_MAX_COLUMN_SIZE_G20;
            col_id = Kb_Map_devicelayout_Correspend_Index_G20[3] % KEYPAD_MAX_COLUMN_SIZE_G20;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_Profile;
            Ir_Override_Table_Index_G20[Ir_Table_Correspend_Index_G20[3]] = KEY_ID_Profile;
        }
        else if ((ui_layout & 0x18) == 0x10)
        {
            row_id = Kb_Map_devicelayout_Correspend_Index_G20[3] / KEYPAD_MAX_COLUMN_SIZE_G20;
            col_id = Kb_Map_devicelayout_Correspend_Index_G20[3] % KEYPAD_MAX_COLUMN_SIZE_G20;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_AllApps;
            Ir_Override_Table_Index_G20[Ir_Table_Correspend_Index_G20[3]] = KEY_ID_AllApps;
        }

        if ((ui_layout & LAYOUT_MASK_BIT5))
        {
            row_id = Kb_Map_devicelayout_Correspend_Index_G20[4] / KEYPAD_MAX_COLUMN_SIZE_G20;
            col_id = Kb_Map_devicelayout_Correspend_Index_G20[4] % KEYPAD_MAX_COLUMN_SIZE_G20;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_FastRewind;
            row_id = Kb_Map_devicelayout_Correspend_Index_G20[5] / KEYPAD_MAX_COLUMN_SIZE_G20;
            col_id = Kb_Map_devicelayout_Correspend_Index_G20[5] % KEYPAD_MAX_COLUMN_SIZE_G20;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_Record;
            row_id = Kb_Map_devicelayout_Correspend_Index_G20[6] / KEYPAD_MAX_COLUMN_SIZE_G20;
            col_id = Kb_Map_devicelayout_Correspend_Index_G20[6] % KEYPAD_MAX_COLUMN_SIZE_G20;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_PlayPause;
            row_id = Kb_Map_devicelayout_Correspend_Index_G20[7] / KEYPAD_MAX_COLUMN_SIZE_G20;
            col_id = Kb_Map_devicelayout_Correspend_Index_G20[7] % KEYPAD_MAX_COLUMN_SIZE_G20;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_FastForward;

            Ir_Override_Table_Index_G20[Ir_Table_Correspend_Index_G20[4]] = KEY_ID_FastRewind;
            Ir_Override_Table_Index_G20[Ir_Table_Correspend_Index_G20[5]] = KEY_ID_Record;
            Ir_Override_Table_Index_G20[Ir_Table_Correspend_Index_G20[6]] = KEY_ID_PlayPause;
            Ir_Override_Table_Index_G20[Ir_Table_Correspend_Index_G20[7]] = KEY_ID_FastForward;
        }
        else
        {
            row_id = Kb_Map_devicelayout_Correspend_Index_G20[4] / KEYPAD_MAX_COLUMN_SIZE_G20;
            col_id = Kb_Map_devicelayout_Correspend_Index_G20[4] % KEYPAD_MAX_COLUMN_SIZE_G20;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_Red;
            row_id = Kb_Map_devicelayout_Correspend_Index_G20[5] / KEYPAD_MAX_COLUMN_SIZE_G20;
            col_id = Kb_Map_devicelayout_Correspend_Index_G20[5] % KEYPAD_MAX_COLUMN_SIZE_G20;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_Green;
            row_id = Kb_Map_devicelayout_Correspend_Index_G20[6] / KEYPAD_MAX_COLUMN_SIZE_G20;
            col_id = Kb_Map_devicelayout_Correspend_Index_G20[6] % KEYPAD_MAX_COLUMN_SIZE_G20;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_Yellow;
            row_id = Kb_Map_devicelayout_Correspend_Index_G20[7] / KEYPAD_MAX_COLUMN_SIZE_G20;
            col_id = Kb_Map_devicelayout_Correspend_Index_G20[7] % KEYPAD_MAX_COLUMN_SIZE_G20;
            key_handle_global_data.cur_key_mapping_table[row_id][col_id] = KEY_ID_Blue;

            Ir_Override_Table_Index_G20[Ir_Table_Correspend_Index_G20[4]] = KEY_ID_Red;
            Ir_Override_Table_Index_G20[Ir_Table_Correspend_Index_G20[5]] = KEY_ID_Green;
            Ir_Override_Table_Index_G20[Ir_Table_Correspend_Index_G20[6]] = KEY_ID_Yellow;
            Ir_Override_Table_Index_G20[Ir_Table_Correspend_Index_G20[7]] = KEY_ID_Blue;
        }
    }
}

void app_custom_set_new_ir_table(void)
{
    IR_KEY_CODE ir_key_code;
    u8 dat[192];
    u8 i;
    u8 total_loop_cnt;
    uint8_t *p_index_table;

    if (app_global_data.device_type == REMOTE_G10)
    {
        total_loop_cnt = 24;
        p_index_table = Ir_Override_Table_Index_G10;
    }
    else if (app_global_data.device_type == REMOTE_G20)
    {
        total_loop_cnt = 48;
        p_index_table = Ir_Override_Table_Index_G20;
    }
    else
    {
        total_loop_cnt = 24;
        p_index_table = Ir_Override_Table_Index_G10;
    }

    frm_vpk_flash_read_page(APP_NEC_IR_CODE_TABLE, 48 * 4, dat);

    for (i = 0; i < total_loop_cnt; i++)
    {
        if ((dat[i * 4 + 2] != 0xff) && (p_index_table[i] != KEY_ID_NONE))
        {
            ir_key_code.ir_data =
                key_handle_global_data.cur_key_code_table[p_index_table[i]].ir_key_code.ir_data;

            ir_key_code.ir_cmd1 = dat[i * 4 + 2];
            if (dat[i * 4 + 3] != 0xff)
            {
                ir_key_code.ir_cmd2 = dat[i * 4 + 3];
            }
            else
            {
                ir_key_code.ir_cmd2 = ~ir_key_code.ir_cmd1;
            }

            if (dat[i * 4] != 0xff)
            {
                ir_key_code.ir_addr1 = dat[i * 4];
                if (dat[i * 4 + 1] != 0xff)
                {
                    ir_key_code.ir_addr2 = dat[i * 4 + 1];
                }
                else
                {
                    ir_key_code.ir_addr2 = ~ir_key_code.ir_addr1;
                }
            }

            key_handle_global_data.cur_key_code_table[p_index_table[i]].ir_key_code.ir_data =
                ir_key_code.ir_data;
        }
    }
}

void app_custom_param_init(u8 device_layout)
{
    if (app_global_data.device_type == REMOTE_G10)
    {
        key_handle_global_data.keyscan_row_size = KEYPAD_MAX_ROW_SIZE_G10;
        key_handle_global_data.keyscan_column_size = KEYPAD_MAX_COLUMN_SIZE_G10;
        memcpy(key_handle_global_data.cur_key_mapping_table, KEY_MAPPING_TABLE_G10,
               sizeof(KEY_MAPPING_TABLE_G10));
    }
    else if (app_global_data.device_type == REMOTE_G20)
    {
        key_handle_global_data.keyscan_row_size = KEYPAD_MAX_ROW_SIZE_G20;
        key_handle_global_data.keyscan_column_size = KEYPAD_MAX_COLUMN_SIZE_G20;
        memcpy(key_handle_global_data.cur_key_mapping_table, KEY_MAPPING_TABLE_G20,
               sizeof(KEY_MAPPING_TABLE_G20));
    }
    app_custom_ui_layout(device_layout);
    app_custom_set_new_ir_table();
}

void app_custom_wakeup_key_load(u16 mask1, u16 mask2)
{
    u8 j = 0, i;
    u8 wakeupkey_num = 0;
    u8 *p_kb_map;

    if ((mask1 != 0xffff) && (mask2 != 0xffff) && ((mask1 & mask2) == 0))
    {
        if (app_global_data.device_type == REMOTE_G10)
        {
            wakeupkey_num = APP_CUSTOM_WAKEUPKEY_NUM - 4;
            p_kb_map = (u8 *)Wakeup_Map_Correspend_Index_G10;
        }
        else
        {
            wakeupkey_num = APP_CUSTOM_WAKEUPKEY_NUM;
            p_kb_map = (u8 *)Wakeup_Map_Correspend_Index_G20;
        }
        for (i = 0; i < wakeupkey_num; i++)
        {
            if (mask1 & 0x01)
            {
                app_global_data.wakeup_key1[j] = p_kb_map[i];
                j++;
            }
            mask1 = mask1 >> 1;
        }
        j = 0;
        for (i = 0; i < wakeupkey_num; i++)
        {
            if (mask2 & 0x01)
            {
                app_global_data.wakeup_key2[j] = p_kb_map[i];
                j++;
            }
            mask2 = mask2 >> 1;
        }
    }
}

void app_custom_init(void)
{
    u8 buffer[128];
    u8 addr_pos = 0;
//    u8 *pREMOTE_B046 = (u8 *)("RemoteB046");

    frm_vpk_flash_read_page(APP_CUSTOM_ADDR, 128, buffer);

    if (buffer[0] == 0xff)
    {
        app_global_data.device_ui_layout_type = 0;
        frm_vpk_print("default custom info\r\n");
        app_global_data.device_type = REMOTE_G20;
        app_global_data.device_name_len = 10;
        memcpy(app_global_data.device_name, pREMOTE_G20, 10);
        app_custom_param_init(app_global_data.device_ui_layout_type);
        app_global_data.fw_version[3] = (app_global_data.device_type) + 0x30;
        app_global_data.fw_version[4] = 0x30;
        app_global_data.fw_version[5] = 0x30;
        app_global_data.wakeup_adv_format = WAKEUP_FORMAT_GOOGLE_ONLY;
        return;
    }
    else
    {
        memcpy(app_global_data.pnp_id, &buffer[1], 7);
        app_global_data.device_name_len = buffer[8];
        app_global_data.device_type = buffer[0];
        if ((app_global_data.device_type != REMOTE_G10) && (app_global_data.device_type != REMOTE_G20))
        {
            app_global_data.device_ui_layout_type = 0;
            app_global_data.device_type = REMOTE_G20;
            app_global_data.device_name_len = 10;
            memcpy(app_global_data.device_name, pREMOTE_G20, 10);
            app_custom_param_init(app_global_data.device_ui_layout_type);
            app_global_data.fw_version[3] = (app_global_data.device_type) + 0x30;
            app_global_data.fw_version[4] = 0x30;
            app_global_data.fw_version[5] = 0x30;
            app_global_data.wakeup_adv_format = WAKEUP_FORMAT_GOOGLE_ONLY;
            return;
        }
        if (app_global_data.device_name_len > 16)
        {
            memcpy(app_global_data.device_name, &buffer[9], 16);
        }
        else
        {
            memcpy(app_global_data.device_name, &buffer[9], app_global_data.device_name_len);
        }

        addr_pos = 9 + app_global_data.device_name_len;
        app_global_data.device_ui_layout_type = buffer[addr_pos];
        addr_pos = addr_pos + 1;

        //wakeup key
        app_global_data.wakeupkey1_mask = (u16)(((u16)buffer[addr_pos] << 8) | ((u16)buffer[addr_pos + 1]));
        app_global_data.wakeupkey2_mask = (u16)(((u16)buffer[addr_pos + 2] << 8) | ((
                                                                                        u16)buffer[addr_pos + 3]));
        frm_vpk_print("wakeupkey1_mask=%x,wakeupkey2_mask=%x\r\n", app_global_data.wakeupkey1_mask,
                      app_global_data.wakeupkey2_mask);
        app_custom_wakeup_key_load(app_global_data.wakeupkey1_mask, app_global_data.wakeupkey2_mask);
        addr_pos += 4;

        if (app_global_data.device_name_len > 16)
        {
            app_global_data.device_name_len = 16;
        }
    }
    app_custom_param_init(app_global_data.device_ui_layout_type);
    app_global_data.fw_version[3] = (app_global_data.device_type) + 0x30;

    uint8_t base_val;
    uint8_t char_val;

    char_val = app_global_data.device_ui_layout_type >> 4;
    base_val = (char_val > 9) ? ('A' - 10) : 0x30;
    app_global_data.fw_version[4] = char_val + base_val;

    char_val = app_global_data.device_ui_layout_type & 0x0F;
    base_val = (char_val > 9) ? ('A' - 10) : 0x30;
    app_global_data.fw_version[5] = char_val + base_val;

    /* read APP_CUSTOM_ADDR+0xF9 and set 16 bit wakeup_interval value */
    u8 buffer_wakeup_interval[2] = {0, 0};
    frm_vpk_flash_read_page(APP_FMS_WAKEUP_INTERVAL, 2, buffer_wakeup_interval);
    BE_ARRAY_TO_UINT16(app_global_data.find_me_wakeup_timeout, buffer_wakeup_interval);

    /* read APP_CUSTOM_ADDR+0xFB and set en_find_me value */
    u8 buffer_find_me[1] = {0};
    frm_vpk_flash_read_page(APP_EN_FIND_ME, 1, buffer_find_me);
    if (buffer_find_me[0] == 0)
    {
        app_global_data.en_find_me = 1;  //enable find me
    }
    if (buffer_find_me[0] == 0xFF)
    {
        app_global_data.en_find_me = 0;  //default:disable find me
    }

    /* read APP_CUSTOM_ADDR+0xFC and set en_rpa value */
    u8 buffer_rpa[1] = {0};
    frm_vpk_flash_read_page(APP_EN_RPA, 1, buffer_rpa);
    if (buffer_rpa[0] == 0)
    {
        app_global_data.en_rpa = 1;  //enable RPA
    }
    else if (buffer_rpa[0] == 0xFF)
    {
        app_global_data.en_rpa = 0;  //default:disable RPA
    }

    /* read APP_CUSTOM_ADDR+0xFD and set en_ble_feature value */
    u8 buffer_ble_feature[1] = {0};
    frm_vpk_flash_read_page(APP_EN_BLE_FEATURE, 1, buffer_ble_feature);
    if (buffer_ble_feature[0] == 0xFF)
    {
        app_global_data.en_ble_feature = 1;  //default:enable BLE
    }
    else if (buffer_ble_feature[0] == 0)
    {
        app_global_data.en_ble_feature = 0;  //disable BLE, only use IR
    }

    /* read APP_CUSTOM_ADDR+0xFE and set en_google_wakeuppack value */
    u8 buffer_wakeuppack[1] = {0};
    frm_vpk_flash_read_page(APP_EN_GOOGLE_WAKEUPPACK, 1, buffer_wakeuppack);
    if (buffer_wakeuppack[0] == 0xFF)
    {
        app_global_data.en_google_wakeuppack = 1;  //default:hybrid wakeup format
    }
    if (buffer_wakeuppack[0] == 0)
    {
        app_global_data.en_google_wakeuppack = 0;  //customized wakeup format
    }

    /* read APP_CUSTOM_ADDR+0xFF and set en_powerkey_cache value */
    u8 buffer_cachekey[1] = {0};
    frm_vpk_flash_read_page(APP_EN_CACHEKEY, 1, buffer_cachekey);
    if (buffer_cachekey[0] == 0xFF)
    {
        app_global_data.en_powerkey_cache = 1;  //default:cache power key
    }
    if (buffer_cachekey[0] == 0)
    {
        app_global_data.en_powerkey_cache = 0;  //not cache power key
    }

    if ((buffer[addr_pos] == 0) || (buffer[addr_pos] > 31))
    {
        app_global_data.wakeup_adv_format = WAKEUP_FORMAT_GOOGLE_ONLY;
    }
    else
    {
        if (app_global_data.en_google_wakeuppack == 0)
        {
            app_global_data.wakeup_adv_format = WAKEUP_FORMAT_CUSTOM_ONLY;
        }
        else
        {
            app_global_data.wakeup_adv_format = WAKEUP_FORMAT_CUSTOM_AND_GOOGLE;
        }

        memcpy(app_global_data.wakeup_adv_custom_data, &buffer[addr_pos], MAX_WAKE_CUSTOM_DATA_LEN);
    }
}

u8 app_custom_is_wakeup_key(u8 keyid)
{
    u8 i;

    for (i = 0; i < APP_CUSTOM_WAKEUPKEY_NUM; i++)
    {
        if ((keyid == app_global_data.wakeup_key1[i]) || (keyid == app_global_data.wakeup_key2[i]))
        {
            frm_vpk_print("wakeup_key =%x\r\n", keyid);
            return 1;
        }
    }
    return 0;
}

u8 app_custom_is_enable_wakeup_key(void)
{
    if ((app_global_data.wakeupkey1_mask != 0xffff) && (app_global_data.wakeupkey2_mask != 0xffff) &&
        ((app_global_data.wakeupkey1_mask & app_global_data.wakeupkey2_mask) == 0))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

u8  app_custom_wakeupkey_packet_index(u8 keyid)
{
    u8 i;

    for (i = 0; i < APP_CUSTOM_WAKEUPKEY_NUM; i++)
    {
        if (keyid == app_global_data.wakeup_key1[i])
        {
            return 1;
        }
        else if (keyid == app_global_data.wakeup_key2[i])
        {
            return 2;
        }
    }
    return 2;
}

void app_custom_test(void)
{
    u8 buffer[65] = {/* device type */0x02,
                                      /* PnP Id */ 0x02, 0x8a, 0x24, 0x66, 0x82, 0x01, 0x00,
                                      /* device name and length */ 0x09, 0x52, 0x65, 0x6d, 0x6f, 0x74, 0x65, 0x47, 0x32, 0x30,
                                      /* ui layout */ 0x00,
                                      /* wake up key */ 0x00, 0x04, 0x00, 0x70,
                                      /* wake up adv */ 0x08, 0x00, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
                    };
    frm_vpk_flash_erase_sector(APP_CUSTOM_ADDR);
    frm_vpk_flash_write_page(APP_CUSTOM_ADDR, sizeof(buffer), buffer);

    u8 buffer2[2] = {0x00, 0x05};
    frm_vpk_flash_write_page(APP_FMS_WAKEUP_INTERVAL, 2, buffer2);

    buffer2[0] = 1;
    frm_vpk_flash_write_page(APP_EN_FIND_ME, 1, buffer2);

    buffer2[0] = 0;
    frm_vpk_flash_write_page(APP_EN_GOOGLE_WAKEUPPACK, 1, buffer2);
    frm_vpk_flash_write_page(APP_EN_BLE_FEATURE, 1, buffer2);   //test:write 0 to APP_EN_BLE_FEATURE

    u8 buffer_ir_table[48 * 4];
    u8 override_index;
    u8 data_buf[4] = {0xFF, 0xFF, 0xFF, 0xFF};
    memset(buffer_ir_table, 0xFF, sizeof(buffer_ir_table));

    /* override up with case 1 */
    override_index = 30;
    data_buf[0] = 0xFF;
    data_buf[1] = 0xFF;
    data_buf[2] = 0x33;
    data_buf[3] = 0xFF;
    memcpy(buffer_ir_table + 4 * override_index, data_buf, 4);
    /* override down with case 2 */
    override_index = 7;
    data_buf[0] = 0x11;
    data_buf[1] = 0xFF;
    data_buf[2] = 0x33;
    data_buf[3] = 0xFF;
    memcpy(buffer_ir_table + 4 * override_index, data_buf, 4);
    /* override left with case 3 */
    override_index = 36;
    data_buf[0] = 0x11;
    data_buf[1] = 0x22;
    data_buf[2] = 0x33;
    data_buf[3] = 0xFF;
    memcpy(buffer_ir_table + 4 * override_index, data_buf, 4);
    /* override right with case 4 */
    override_index = 1;
    data_buf[0] = 0x11;
    data_buf[1] = 0x22;
    data_buf[2] = 0x33;
    data_buf[3] = 0x44;
    memcpy(buffer_ir_table + 4 * override_index, data_buf, 4);

    frm_vpk_flash_erase_sector(APP_NEC_IR_CODE_TABLE);
    frm_vpk_flash_write_page(APP_NEC_IR_CODE_TABLE, sizeof(buffer_ir_table), buffer_ir_table);
}
