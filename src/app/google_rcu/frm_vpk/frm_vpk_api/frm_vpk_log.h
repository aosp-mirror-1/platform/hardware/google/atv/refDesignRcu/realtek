/*
 * Copyright 2020, Amazon.
 */

/** @file
*
* List of parameters and defined functions needed to access the
* log service.
*
*/
#ifndef _FRM_VPK_LOG_H_
#define _FRM_VPK_LOG_H_

/**
 * @brief API implements the console log printing.
 *
 */
int frm_vpk_print(char *fmt, ...);

#endif /* _FRM_VPK_LOG_H_ */
