/**
*********************************************************************************************************
*               Copyright(c) 2021, Realtek Semiconductor Corporation. All rights reserved.
**********************************************************************************************************
* @file     frm_vpk_hal_sflash.c
* @brief    frm vpk flash API implementation
* @details
* @author   chenjie
* @date     2021-01-09
* @version  v0.1
*********************************************************************************************************
*/

/*============================================================================*
 *                              Header Files
 *============================================================================*/
#include "frm_vpk_hal_sflash.h"
#include "flash_device.h"
#include "frm_vpk_log.h"
#include "dfu_flash.h"

/*============================================================================*
 *                              Macro Definitions
 *============================================================================*/

/*============================================================================*
 *                              Local Variables
 *============================================================================*/

/*============================================================================*
 *                              Global functions
 *============================================================================*/
/**
 * @brief         This function reads the content from a page to the buf.
 * @param[in]   addr    - the start address of the page.
 * @param[in]   len        - the length(in byte) of content needs to read out from the page.
 * @param[out]  buf        - the start address of the buffer.
 * @return         none.
 * @note        Attention: Before calling the FLASH function, please check the power supply voltage of the chip.
 *              Only if the detected voltage is greater than the safe voltage value, the FLASH function can be called.
 *              Taking into account the factors such as power supply fluctuations, the safe voltage value needs to be greater
 *              than the minimum chip operating voltage. For the specific value, please make a reasonable setting according
 *              to the specific application and hardware circuit.
 *
 *              Risk description: When the chip power supply voltage is relatively low, due to the unstable power supply,
 *              there may be a risk of error in the operation of the flash (especially for the write and erase operations.
 *              If an abnormality occurs, the firmware and user data may be rewritten, resulting in the final Product failure)
 */
void frm_vpk_flash_read_page(unsigned long addr, unsigned long len, unsigned char *buf)
{
    bool result = false;
    result = flash_read_locked((addr | FLASH_OFFSET_TO_NO_CACHE), len, buf);
    frm_vpk_print("[frm_vpk_flash_read_page]: addr=%x, len=%d, result=%d(1:success)", addr, len,
                  result);
}

/**
 * @brief         This function writes the buffer's content to the flash.
 * @param[in]   addr    - the start address of the area.
 * @param[in]   len        - the length(in byte) of content needs to write into the flash.
 * @param[in]   buf        - the start address of the content needs to write into.
 * @return         none.
 * @note        the function support cross-page writing,which means the len of buf can bigger than 256.
 *
 *              Attention: Before calling the FLASH function, please check the power supply voltage of the chip.
 *              Only if the detected voltage is greater than the safe voltage value, the FLASH function can be called.
 *              Taking into account the factors such as power supply fluctuations, the safe voltage value needs to be greater
 *              than the minimum chip operating voltage. For the specific value, please make a reasonable setting according
 *              to the specific application and hardware circuit.
 *
 *              Risk description: When the chip power supply voltage is relatively low, due to the unstable power supply,
 *              there may be a risk of error in the operation of the flash (especially for the write and erase operations.
 *              If an abnormality occurs, the firmware and user data may be rewritten, resulting in the final Product failure)
 */
void frm_vpk_flash_write_page(unsigned long addr, unsigned long len, unsigned char *buf)
{
    bool result = false;

    unlock_flash_bp_all();
    result = flash_write_locked((addr | FLASH_OFFSET_TO_NO_CACHE), len, buf);
    lock_flash_bp();

    frm_vpk_print("[frm_vpk_flash_write_page]: addr=%x, len=%d, result=%d(1:success)", addr, len,
                  result);
}

/**
 * @brief         This function serves to erase a sector.
 * @param[in]   addr    - the start address of the sector needs to erase.
 * @return         none.
 * @note        Attention: Before calling the FLASH function, please check the power supply voltage of the chip.
 *              Only if the detected voltage is greater than the safe voltage value, the FLASH function can be called.
 *              Taking into account the factors such as power supply fluctuations, the safe voltage value needs to be greater
 *              than the minimum chip operating voltage. For the specific value, please make a reasonable setting according
 *              to the specific application and hardware circuit.
 *
 *              Risk description: When the chip power supply voltage is relatively low, due to the unstable power supply,
 *              there may be a risk of error in the operation of the flash (especially for the write and erase operations.
 *              If an abnormality occurs, the firmware and user data may be rewritten, resulting in the final Product failure)
 */
void frm_vpk_flash_erase_sector(unsigned long addr)
{
    bool result = false;
    result = flash_erase_locked(FLASH_ERASE_SECTOR, addr);
    frm_vpk_print("[frm_vpk_flash_erase_sector]: addr=%x, result=%d(1:success)", addr, result);
}

/******************* (C) COPYRIGHT 2021 Realtek Semiconductor Corporation *****END OF FILE****/

