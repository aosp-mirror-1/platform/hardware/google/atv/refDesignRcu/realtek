/*
 * Copyright 2020, Amazon.
 */

/** @file
*
* List of parameters and defined functions needed to access the
* Serial Flash interface driver.
*
*/

#ifndef __FRM_VPK_SFLASH_H__
#define __FRM_VPK_SFLASH_H__

void frm_vpk_flash_read_page(unsigned long addr, unsigned long len, unsigned char *buf);
void frm_vpk_flash_write_page(unsigned long addr, unsigned long len, unsigned char *buf);
void frm_vpk_flash_erase_sector(unsigned long addr);

#endif // __FRM_VPK_SFLASH_H__
