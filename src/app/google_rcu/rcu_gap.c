/**
*********************************************************************************************************
*               Copyright(c) 2020, Realtek Semiconductor Corporation. All rights reserved.
*********************************************************************************************************
* @file      rcu_gap.c
* @brief     rcu gap implementation
* @details   including handler for advertising, disconnecting and updating parameters
* @author    barry_bian
* @date      2020-02-24
* @version   v1.0
* *********************************************************************************************************
*/

/*============================================================================*
 *                              Header Files
 *============================================================================*/
#include "rtl876x.h"
#include "gap.h"
#include "gap_adv.h"
#include "rcu_gap.h"
#include "board.h"
#include "gap_storage_le.h"
#include "gap_conn_le.h"
#include "string.h"
#include <trace.h>
#include "mem_config.h"
#include "rcu_application.h"
#include <gap_bond_le.h>
#include "os_timer.h"
#include "swtimer.h"
#include "led_driver.h"
#if FEATURE_SUPPORT_PRIVACY
#include "privacy_mgnt.h"
#endif
#include "app_custom.h"
#include <gap_privacy.h>
#include "battery_driver.h"

/*============================================================================*
 *                              Macros
 *============================================================================*/
/* What is the advertising interval when device is discoverable (units of 625us, 160=100ms) */
#define DEFAULT_ADVERTISING_INTERVAL_MIN 320 /* 20ms */
#define DEFAULT_ADVERTISING_INTERVAL_MAX 320 /* 30ms */

/*============================================================================*
 *                              Local Variables
 *============================================================================*/
/* GAP - SCAN RSP data (max size = 31 bytes) */
static uint8_t app_scan_rsp_data_len = 0;
static uint8_t app_scan_rsp_data[31] = {};

/* Pairing advertisement data (max size = 31 bytes, though this is
 best kept short to conserve power while advertisting) */
static uint8_t app_pairing_adv_data_len = 0;
static uint8_t app_pairing_adv_data[31] = {};

static const uint8_t app_adv_data_fixed_part[] =
{
    0x02,  /* length */
    0x01,  /* type="Flags" */
    0x06,  /* LE Limited Discoverable Mode, BR/EDR not supported */

    0x03,  /* length */
    0x19,  /* type="Appearance" */
    0x80, 0x01,  /* Remote Control */

    0x05,  /* length */
    0x02,  /* type="incomplete 16-bit UUIDs available" */
    0x12, 0x18,  /* HID Service */
    0x0F, 0x18,
};

/* GAP - Advertisement data (max size = 31 bytes, though this is
 best kept short to conserve power while advertisting) */
static uint8_t google_wake_adv_data[] =
{
    0x0C,  /* length     */
    0x16,  /* service data */
    0x36, 0xFD,  /* Wakeup Packet Service ID */
    0x01,  /* packet format version */
    0x01,  /* key index */
    0x01,  /* key press count */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00  /* master MAC address */
};
static uint8_t google_wake_adv_data_len = sizeof(google_wake_adv_data);

static uint8_t custom_wakeup_adv_data[31] = {};
static uint8_t custom_wakeup_adv_data_len = 0;

/*============================================================================*
 *                          Global Functions
 *============================================================================*/
void app_change_wakeup_adv_data(void)
{
    if ((app_global_data.rcu_status == RCU_STATUS_ADVERTISING)
        && (app_global_data.adv_type == ADV_UNDIRECT_WAKEUP))
    {
        le_adv_set_param(GAP_PARAM_ADV_DATA, google_wake_adv_data_len, google_wake_adv_data);
        le_adv_update_param();
    }
}

/******************************************************************
 * @brief   update adv and scan response data
 * @param   void
 * @retval  void
 */
void rcu_update_adv_rsp_data(void)
{
    /* set to default value */
    app_scan_rsp_data_len = 0;
    memset(app_scan_rsp_data, 0, sizeof(app_scan_rsp_data));
    app_pairing_adv_data_len = 0;
    memset(app_pairing_adv_data, 0, sizeof(app_pairing_adv_data));

    /* prepare scan response data */
    app_scan_rsp_data[0] = app_global_data.device_name_len + 1;
    app_scan_rsp_data[1] = 0x09;
    memcpy(&app_scan_rsp_data[2], app_global_data.device_name, app_global_data.device_name_len);
    app_scan_rsp_data_len = app_global_data.device_name_len + 2;

    /* prepare adv data */
    memcpy(app_pairing_adv_data, app_scan_rsp_data, app_scan_rsp_data_len);
    memcpy(&app_pairing_adv_data[app_scan_rsp_data_len], app_adv_data_fixed_part,
           sizeof(app_adv_data_fixed_part));
    app_pairing_adv_data_len = app_scan_rsp_data_len + sizeof(app_adv_data_fixed_part);
}

/******************************************************************
 * @brief    Initialize peripheral and gap bond manager related parameters
 * @param    none
 * @return   none
 * @retval   void
 */
void rcu_le_gap_init(void)
{
    uint8_t  device_name[GAP_DEVICE_NAME_LEN] = {0};
    memcpy(device_name, app_global_data.device_name, app_global_data.device_name_len);

    uint16_t appearance = GAP_GATT_APPEARANCE_GENERIC_REMOTE_CONTROL;

    //advertising parameters
    uint8_t  adv_evt_type = GAP_ADTYPE_ADV_IND;
    uint8_t  adv_direct_type = GAP_REMOTE_ADDR_LE_PUBLIC;
    uint8_t  adv_direct_addr[GAP_BD_ADDR_LEN] = {0};
    uint8_t  adv_chann_map = GAP_ADVCHAN_ALL;
    uint8_t  adv_filter_policy = GAP_ADV_FILTER_ANY;
    uint16_t adv_int_min = DEFAULT_ADVERTISING_INTERVAL_MIN;
    uint16_t adv_int_max = DEFAULT_ADVERTISING_INTERVAL_MIN;

    //GAP Bond Manager parameters
    uint8_t  auth_pair_mode = GAP_PAIRING_MODE_PAIRABLE;
    uint16_t auth_flags = GAP_AUTHEN_BIT_BONDING_FLAG;
    uint8_t  auth_io_cap = GAP_IO_CAP_NO_INPUT_NO_OUTPUT;
    uint8_t  auth_oob = false;
    uint8_t  auth_use_fix_passkey = false;
    uint32_t auth_fix_passkey = 0;
    uint8_t  auth_sec_req_enable = false;
    uint16_t auth_sec_req_flags = GAP_AUTHEN_BIT_BONDING_FLAG;
    uint8_t  slave_init_mtu_req = false;
    le_set_gap_param(GAP_PARAM_SLAVE_INIT_GATT_MTU_REQ, sizeof(slave_init_mtu_req),
                     &slave_init_mtu_req);//maximum MTU size default is 247

    //Register gap callback
    le_register_app_cb(app_gap_callback);

#if FEATURE_SUPPORT_PRIVACY
    privacy_init(app_privacy_callback, true);
#endif

    //Set RPA parameters
    if (app_global_data.en_rpa == 1)
    {
        uint16_t privacy_timeout = APP_RESOLVED_TIMEOUT;
        uint8_t gen_auto = 1;
        uint8_t  local_bd_type = GAP_LOCAL_ADDR_LE_RAP_OR_PUBLIC;
        le_privacy_set_param(GAP_PARAM_PRIVACY_TIMEOUT, sizeof(uint16_t), &privacy_timeout);
        le_bond_set_param(GAP_PARAM_BOND_GEN_LOCAL_IRK_AUTO, sizeof(gen_auto), &gen_auto);
        le_adv_set_param(GAP_PARAM_ADV_LOCAL_ADDR_TYPE, sizeof(local_bd_type), &local_bd_type);
    }

    //Set device name and device appearance
    le_set_gap_param(GAP_PARAM_DEVICE_NAME, GAP_DEVICE_NAME_LEN, device_name);
    le_set_gap_param(GAP_PARAM_APPEARANCE, sizeof(appearance), &appearance);

    //Set advertising parameters
    le_adv_set_param(GAP_PARAM_ADV_EVENT_TYPE, sizeof(adv_evt_type), &adv_evt_type);
    le_adv_set_param(GAP_PARAM_ADV_DIRECT_ADDR_TYPE, sizeof(adv_direct_type), &adv_direct_type);
    le_adv_set_param(GAP_PARAM_ADV_DIRECT_ADDR, sizeof(adv_direct_addr), adv_direct_addr);
    le_adv_set_param(GAP_PARAM_ADV_CHANNEL_MAP, sizeof(adv_chann_map), &adv_chann_map);
    le_adv_set_param(GAP_PARAM_ADV_FILTER_POLICY, sizeof(adv_filter_policy), &adv_filter_policy);
    le_adv_set_param(GAP_PARAM_ADV_INTERVAL_MIN, sizeof(adv_int_min), &adv_int_min);
    le_adv_set_param(GAP_PARAM_ADV_INTERVAL_MAX, sizeof(adv_int_max), &adv_int_max);
    le_adv_set_param(GAP_PARAM_ADV_DATA, app_pairing_adv_data_len, app_pairing_adv_data);
    le_adv_set_param(GAP_PARAM_SCAN_RSP_DATA, app_scan_rsp_data_len, app_scan_rsp_data);

    // Setup the GAP Bond Manager
    gap_set_param(GAP_PARAM_BOND_PAIRING_MODE, sizeof(auth_pair_mode), &auth_pair_mode);
    gap_set_param(GAP_PARAM_BOND_AUTHEN_REQUIREMENTS_FLAGS, sizeof(auth_flags), &auth_flags);
    gap_set_param(GAP_PARAM_BOND_IO_CAPABILITIES, sizeof(auth_io_cap), &auth_io_cap);
    gap_set_param(GAP_PARAM_BOND_OOB_ENABLED, sizeof(auth_oob), &auth_oob);
    le_bond_set_param(GAP_PARAM_BOND_FIXED_PASSKEY, sizeof(auth_fix_passkey), &auth_fix_passkey);
    le_bond_set_param(GAP_PARAM_BOND_FIXED_PASSKEY_ENABLE, sizeof(auth_use_fix_passkey),
                      &auth_use_fix_passkey);
    le_bond_set_param(GAP_PARAM_BOND_SEC_REQ_ENABLE, sizeof(auth_sec_req_enable), &auth_sec_req_enable);
    le_bond_set_param(GAP_PARAM_BOND_SEC_REQ_REQUIREMENT, sizeof(auth_sec_req_flags),
                      &auth_sec_req_flags);
}

/******************************************************************
 * @brief   start advertising
 * @param   adv_type - indicate to send which type of adv
 * @return  result of starting advertising
 * @retval  true or false
 */
bool rcu_start_adv(T_ADV_TYPE adv_type)
{
    bool result = false;
    if (app_global_data.en_ble_feature == 0)
    {
        APP_PRINT_INFO0("RCU disable BLE");
        return result;
    }

    uint8_t adv_event_type;
    uint8_t adv_channel_map;
    uint8_t adv_filter_policy;
    uint16_t adv_interval_min;
    uint16_t adv_interval_max;
    uint16_t local_bd_type;
    T_LE_KEY_ENTRY *p_le_key_entry = le_get_high_priority_bond();

    /* check current status, only allow to start adv if RCU_STATUS_IDLE */
    if (app_global_data.rcu_status != RCU_STATUS_IDLE)
    {
        APP_PRINT_WARN1("rcu_status %d is not allowed to start adv again!", app_global_data.rcu_status);
        return false;
    }

    switch (adv_type)
    {
    case ADV_DIRECT_HDC:
        {
            adv_event_type = GAP_ADTYPE_ADV_HDC_DIRECT_IND;
            adv_filter_policy = GAP_ADV_FILTER_ANY;
            adv_channel_map = GAP_ADVCHAN_ALL;

            le_adv_set_param(GAP_PARAM_ADV_EVENT_TYPE, sizeof(adv_event_type), &adv_event_type);
            le_adv_set_param(GAP_PARAM_ADV_FILTER_POLICY, sizeof(adv_filter_policy), &adv_filter_policy);
            le_adv_set_param(GAP_PARAM_ADV_CHANNEL_MAP, sizeof(adv_channel_map), &adv_channel_map);

#if FEATURE_SUPPORT_PRIVACY
            uint8_t addr_type = p_le_key_entry->remote_bd.addr[5] & RANDOM_ADDR_MASK;
            if ((p_le_key_entry->remote_bd.remote_bd_type == GAP_REMOTE_ADDR_LE_RANDOM) &&
                (addr_type == RANDOM_ADDR_MASK_RESOLVABLE))
            {
                APP_PRINT_INFO0("[rcu_start_adv] resolvable random addr!");
                if (app_privacy_resolution_state == PRIVACY_ADDR_RESOLUTION_DISABLED)
                {
                    privacy_set_addr_resolution(true);
                }

                le_adv_set_param(GAP_PARAM_ADV_DIRECT_ADDR_TYPE,
                                 sizeof(p_le_key_entry->resolved_remote_bd.remote_bd_type),
                                 &(p_le_key_entry->resolved_remote_bd.remote_bd_type));
                le_adv_set_param(GAP_PARAM_ADV_DIRECT_ADDR, sizeof(p_le_key_entry->resolved_remote_bd.addr),
                                 p_le_key_entry->resolved_remote_bd.addr);
            }
            else
#endif
            {
                APP_PRINT_INFO0("[rcu_start_adv] not resolvable random addr!");
                le_adv_set_param(GAP_PARAM_ADV_DIRECT_ADDR_TYPE, sizeof(p_le_key_entry->remote_bd.remote_bd_type),
                                 &(p_le_key_entry->remote_bd.remote_bd_type));
                le_adv_set_param(GAP_PARAM_ADV_DIRECT_ADDR, sizeof(p_le_key_entry->remote_bd.addr),
                                 p_le_key_entry->remote_bd.addr);
            }

            if (GAP_CAUSE_SUCCESS == le_adv_start())
            {
                APP_PRINT_INFO0("rcu start ADV_DIRECT_HDC success!");
                app_global_data.rcu_status = RCU_STATUS_ADVERTISING;
                app_global_data.adv_type = ADV_DIRECT_HDC;
                result = true;
            }
            else
            {
                APP_PRINT_WARN0("rcu start ADV_DIRECT_HDC fail!");
            }
        }
        break;

    case ADV_DIRECT_LDC:
        {
            adv_event_type = GAP_ADTYPE_ADV_LDC_DIRECT_IND;
            adv_filter_policy = GAP_ADV_FILTER_ANY;
            adv_channel_map = GAP_ADVCHAN_ALL;
            adv_interval_min = 0x20;  /* 20ms */
            adv_interval_max = 0x28;  /* 25ms */

            le_adv_set_param(GAP_PARAM_ADV_EVENT_TYPE, sizeof(adv_event_type), &adv_event_type);
            le_adv_set_param(GAP_PARAM_ADV_FILTER_POLICY, sizeof(adv_filter_policy), &adv_filter_policy);
            le_adv_set_param(GAP_PARAM_ADV_CHANNEL_MAP, sizeof(adv_channel_map), &adv_channel_map);
            le_adv_set_param(GAP_PARAM_ADV_INTERVAL_MIN, sizeof(adv_interval_min), &adv_interval_min);
            le_adv_set_param(GAP_PARAM_ADV_INTERVAL_MAX, sizeof(adv_interval_max), &adv_interval_max);

#if FEATURE_SUPPORT_PRIVACY
            uint8_t addr_type = p_le_key_entry->remote_bd.addr[5] & RANDOM_ADDR_MASK;
            if ((p_le_key_entry->remote_bd.remote_bd_type == GAP_REMOTE_ADDR_LE_RANDOM) &&
                (addr_type == RANDOM_ADDR_MASK_RESOLVABLE))
            {
                APP_PRINT_INFO0("[rcu_start_adv] resolvable random addr!");
                if (app_privacy_resolution_state == PRIVACY_ADDR_RESOLUTION_DISABLED)
                {
                    privacy_set_addr_resolution(true);
                }

                //Set RPA parameters
                if (app_global_data.en_rpa == 1)
                {
                    local_bd_type = GAP_LOCAL_ADDR_LE_RAP_OR_PUBLIC;
                    le_adv_set_param(GAP_PARAM_ADV_LOCAL_ADDR_TYPE, sizeof(local_bd_type), &local_bd_type);
                }

                le_adv_set_param(GAP_PARAM_ADV_DIRECT_ADDR_TYPE,
                                 sizeof(p_le_key_entry->resolved_remote_bd.remote_bd_type),
                                 &(p_le_key_entry->resolved_remote_bd.remote_bd_type));
                le_adv_set_param(GAP_PARAM_ADV_DIRECT_ADDR, sizeof(p_le_key_entry->resolved_remote_bd.addr),
                                 p_le_key_entry->resolved_remote_bd.addr);
            }
            else
#endif
            {
                APP_PRINT_INFO0("[rcu_start_adv] not resolvable random addr!");
                le_adv_set_param(GAP_PARAM_ADV_DIRECT_ADDR_TYPE, sizeof(p_le_key_entry->remote_bd.remote_bd_type),
                                 &(p_le_key_entry->remote_bd.remote_bd_type));
                le_adv_set_param(GAP_PARAM_ADV_DIRECT_ADDR, sizeof(p_le_key_entry->remote_bd.addr),
                                 p_le_key_entry->remote_bd.addr);
            }

            if (GAP_CAUSE_SUCCESS == le_adv_start())
            {
                APP_PRINT_INFO0("rcu start ADV_DIRECT_LDC success!");
                app_global_data.rcu_status = RCU_STATUS_ADVERTISING;
                app_global_data.adv_type = ADV_DIRECT_LDC;
                os_timer_restart(&adv_timer, ADV_UPDATE_INTERVAL_TIMEOUT);
                result = true;
            }
            else
            {
                APP_PRINT_WARN0("rcu start ADV_DIRECT_LDC fail!");
            }
        }
        break;
    case ADV_DIRECT_LDC_LT:
        {
            adv_event_type = GAP_ADTYPE_ADV_LDC_DIRECT_IND;
            adv_filter_policy = GAP_ADV_FILTER_ANY;
            adv_channel_map = GAP_ADVCHAN_ALL;
            adv_interval_min = 0x30;  /* 30ms */
            adv_interval_max = 0x35;  /* 35ms */

            le_adv_set_param(GAP_PARAM_ADV_EVENT_TYPE, sizeof(adv_event_type), &adv_event_type);
            le_adv_set_param(GAP_PARAM_ADV_FILTER_POLICY, sizeof(adv_filter_policy), &adv_filter_policy);
            le_adv_set_param(GAP_PARAM_ADV_CHANNEL_MAP, sizeof(adv_channel_map), &adv_channel_map);
            le_adv_set_param(GAP_PARAM_ADV_INTERVAL_MIN, sizeof(adv_interval_min), &adv_interval_min);
            le_adv_set_param(GAP_PARAM_ADV_INTERVAL_MAX, sizeof(adv_interval_max), &adv_interval_max);

#if FEATURE_SUPPORT_PRIVACY
            uint8_t addr_type = p_le_key_entry->remote_bd.addr[5] & RANDOM_ADDR_MASK;
            if ((p_le_key_entry->remote_bd.remote_bd_type == GAP_REMOTE_ADDR_LE_RANDOM) &&
                (addr_type == RANDOM_ADDR_MASK_RESOLVABLE))
            {
                APP_PRINT_INFO0("[rcu_start_adv] resolvable random addr!");
                if (app_privacy_resolution_state == PRIVACY_ADDR_RESOLUTION_DISABLED)
                {
                    privacy_set_addr_resolution(true);
                }

                //Set RPA parameters
                if (app_global_data.en_rpa == 1)
                {
                    local_bd_type = GAP_LOCAL_ADDR_LE_RAP_OR_PUBLIC;
                    le_adv_set_param(GAP_PARAM_ADV_LOCAL_ADDR_TYPE, sizeof(local_bd_type), &local_bd_type);
                }

                le_adv_set_param(GAP_PARAM_ADV_DIRECT_ADDR_TYPE,
                                 sizeof(p_le_key_entry->resolved_remote_bd.remote_bd_type),
                                 &(p_le_key_entry->resolved_remote_bd.remote_bd_type));
                le_adv_set_param(GAP_PARAM_ADV_DIRECT_ADDR, sizeof(p_le_key_entry->resolved_remote_bd.addr),
                                 p_le_key_entry->resolved_remote_bd.addr);
            }
            else
#endif
            {
                APP_PRINT_INFO0("[rcu_start_adv] not resolvable random addr!");
                le_adv_set_param(GAP_PARAM_ADV_DIRECT_ADDR_TYPE, sizeof(p_le_key_entry->remote_bd.remote_bd_type),
                                 &(p_le_key_entry->remote_bd.remote_bd_type));
                le_adv_set_param(GAP_PARAM_ADV_DIRECT_ADDR, sizeof(p_le_key_entry->remote_bd.addr),
                                 p_le_key_entry->remote_bd.addr);
            }

            if (GAP_CAUSE_SUCCESS == le_adv_start())
            {
                APP_PRINT_INFO0("rcu start ADV_DIRECT_LDC_LT success!");
                app_global_data.rcu_status = RCU_STATUS_ADVERTISING;
                app_global_data.adv_type = ADV_DIRECT_LDC_LT;
                os_timer_restart(&adv_timer, ADV_RECONNECT_TIMEOUT - ADV_UPDATE_INTERVAL_TIMEOUT);//90
                result = true;
            }
            else
            {
                APP_PRINT_WARN0("rcu start ADV_DIRECT_LDC_LT fail!");
            }
        }
        break;
    case ADV_UNDIRECT_RECONNECT:
        {
#if FEATURE_SUPPORT_PRIVACY
            uint8_t addr_type = p_le_key_entry->remote_bd.addr[5] & RANDOM_ADDR_MASK;
            if ((p_le_key_entry->remote_bd.remote_bd_type == GAP_REMOTE_ADDR_LE_RANDOM) &&
                (addr_type == RANDOM_ADDR_MASK_RESOLVABLE))
            {
                APP_PRINT_INFO0("[rcu_start_adv] resolvable random addr!");
                if (app_privacy_resolution_state == PRIVACY_ADDR_RESOLUTION_DISABLED)
                {
                    privacy_set_addr_resolution(true);
                }
            }
#endif
            adv_event_type = GAP_ADTYPE_ADV_IND;
            adv_channel_map = GAP_ADVCHAN_ALL;
            adv_filter_policy = GAP_ADV_FILTER_WHITE_LIST_ALL;
            adv_interval_min = 0x20;  /* 20ms */
            adv_interval_max = 0x20;  /* 20ms */

            le_adv_set_param(GAP_PARAM_ADV_EVENT_TYPE, sizeof(adv_event_type), &adv_event_type);
            le_adv_set_param(GAP_PARAM_ADV_CHANNEL_MAP, sizeof(adv_channel_map), &adv_channel_map);
            le_adv_set_param(GAP_PARAM_ADV_FILTER_POLICY, sizeof(adv_filter_policy), &adv_filter_policy);
            le_adv_set_param(GAP_PARAM_ADV_INTERVAL_MIN, sizeof(adv_interval_min), &adv_interval_min);
            le_adv_set_param(GAP_PARAM_ADV_INTERVAL_MAX, sizeof(adv_interval_max), &adv_interval_max);
            le_adv_set_param(GAP_PARAM_ADV_DATA, app_pairing_adv_data_len, app_pairing_adv_data);
            le_adv_set_param(GAP_PARAM_SCAN_RSP_DATA, app_scan_rsp_data_len, app_scan_rsp_data);

            if (GAP_CAUSE_SUCCESS == le_adv_start())
            {
                APP_PRINT_INFO0("rcu start ADV_UNDIRECT_RECONNECT success!");
                app_global_data.rcu_status = RCU_STATUS_ADVERTISING;
                app_global_data.adv_type = ADV_UNDIRECT_RECONNECT;
                os_timer_restart(&adv_timer, ADV_RECONNECT_TIMEOUT);
                result = true;
            }
            else
            {
                APP_PRINT_WARN0("rcu start ADV_UNDIRECT_RECONNECT fail!");
            }
        }
        break;

    case ADV_UNDIRECT_WAKEUP:
        {
            PROFILE_PRINT_INFO1("ADV_UNDIRECT_WAKEUP for wake up master, type is %d",
                                app_global_data.wakeup_adv_format);

            uint8_t peer_mac_addr[6];
            adv_event_type = GAP_ADTYPE_ADV_IND;
            adv_channel_map = GAP_ADVCHAN_ALL;
            adv_filter_policy = GAP_ADV_FILTER_WHITE_LIST_ALL;
            adv_interval_min = 0x20;  /* 20ms */
            adv_interval_max = 0x30;  /* 30ms */

#if FEATURE_SUPPORT_PRIVACY
            uint8_t addr_type = p_le_key_entry->remote_bd.addr[5] & RANDOM_ADDR_MASK;
            if ((p_le_key_entry->remote_bd.remote_bd_type == GAP_REMOTE_ADDR_LE_RANDOM) &&
                (addr_type == RANDOM_ADDR_MASK_RESOLVABLE))
            {
                APP_PRINT_INFO0("[rcu_start_adv] resolvable random addr!");

                if (app_privacy_resolution_state == PRIVACY_ADDR_RESOLUTION_DISABLED)
                {
                    privacy_set_addr_resolution(true);
                }
                memcpy(peer_mac_addr, p_le_key_entry->resolved_remote_bd.addr, 6);
            }
            else
#endif
            {
                APP_PRINT_INFO0("[rcu_start_adv] not resolvable random addr!");
                memcpy(peer_mac_addr, p_le_key_entry->remote_bd.addr, 6);
            }

            /* update wakeup adv data */
            if ((app_global_data.wakeup_adv_format == WAKEUP_FORMAT_GOOGLE_ONLY)
                || (app_global_data.wakeup_adv_format == WAKEUP_FORMAT_CUSTOM_AND_GOOGLE))
            {
                /* update google_wake_adv_data */
                google_wake_adv_data[5] = app_custom_wakeupkey_packet_index(app_global_data.wakeup_key_index);
                google_wake_adv_data[6] = (uint8_t)app_global_data.wakeup_key_count;
                memcpy(&google_wake_adv_data[7], peer_mac_addr, 6);
            }

            if ((app_global_data.wakeup_adv_format == WAKEUP_FORMAT_CUSTOM_ONLY)
                || (app_global_data.wakeup_adv_format == WAKEUP_FORMAT_CUSTOM_AND_GOOGLE))
            {
                /* update custom_wakeup_adv_data_len */
                if ((app_global_data.wakeup_adv_custom_data[0] > 0) &&
                    (app_global_data.wakeup_adv_custom_data[0] <= 31))
                {
                    custom_wakeup_adv_data_len = app_global_data.wakeup_adv_custom_data[0];
                    memcpy(custom_wakeup_adv_data, &app_global_data.wakeup_adv_custom_data[4],
                           custom_wakeup_adv_data_len);

                    uint8_t key_id_pos = app_global_data.wakeup_adv_custom_data[1];
                    if (key_id_pos <= 30)
                    {
                        custom_wakeup_adv_data[key_id_pos] = app_custom_wakeupkey_packet_index(
                                                                 app_global_data.wakeup_key_index);
                    }

                    uint8_t key_cnt_pos = app_global_data.wakeup_adv_custom_data[2];
                    if (key_id_pos <= 30)
                    {
                        custom_wakeup_adv_data[key_cnt_pos] = (uint8_t)app_global_data.wakeup_key_count;
                    }

                    uint8_t peer_addr_pos = app_global_data.wakeup_adv_custom_data[3];
                    if (peer_addr_pos <= 24)
                    {
                        memcpy(&custom_wakeup_adv_data[peer_addr_pos], peer_mac_addr, 6);
                    }
                }
            }

            if (app_global_data.wakeup_adv_format == WAKEUP_FORMAT_GOOGLE_ONLY)
            {
                le_adv_set_param(GAP_PARAM_ADV_DATA, google_wake_adv_data_len, google_wake_adv_data);
            }
            else if (app_global_data.wakeup_adv_format == WAKEUP_FORMAT_CUSTOM_AND_GOOGLE)
            {
                le_adv_set_param(GAP_PARAM_ADV_DATA, custom_wakeup_adv_data_len, custom_wakeup_adv_data);
                os_timer_restart(&adv_change_data_timer, ADV_CHANGE_DATA_TIMEOUT);
            }
            else if (app_global_data.wakeup_adv_format == WAKEUP_FORMAT_CUSTOM_ONLY)
            {
                le_adv_set_param(GAP_PARAM_ADV_DATA, custom_wakeup_adv_data_len, custom_wakeup_adv_data);
            }

            le_adv_set_param(GAP_PARAM_ADV_EVENT_TYPE, sizeof(adv_event_type), &adv_event_type);
            le_adv_set_param(GAP_PARAM_ADV_CHANNEL_MAP, sizeof(adv_channel_map), &adv_channel_map);
            le_adv_set_param(GAP_PARAM_ADV_FILTER_POLICY, sizeof(adv_filter_policy), &adv_filter_policy);
            le_adv_set_param(GAP_PARAM_ADV_INTERVAL_MIN, sizeof(adv_interval_min), &adv_interval_min);
            le_adv_set_param(GAP_PARAM_ADV_INTERVAL_MAX, sizeof(adv_interval_max), &adv_interval_max);
            le_adv_set_param(GAP_PARAM_SCAN_RSP_DATA, sizeof(app_scan_rsp_data), app_scan_rsp_data);

            if (GAP_CAUSE_SUCCESS == le_adv_start())
            {
                APP_PRINT_INFO1("rcu start ADV_UNDIRECT_WAKEUP success, wakeup_adv_format is %d",
                                app_global_data.wakeup_adv_format);
                app_global_data.rcu_status = RCU_STATUS_ADVERTISING;
                app_global_data.adv_type = ADV_UNDIRECT_WAKEUP;
                os_timer_restart(&adv_timer, ADV_UNDIRECT_POWER_TIMEOUT);
                result = true;
            }
            else
            {
                APP_PRINT_WARN0("rcu start ADV_UNDIRECT_WAKEUP fail!");
            }
        }
        break;

    case ADV_UNDIRECT_PAIRING:
        {
            if (bat_get_current_mode() == BAT_MODE_LOW_POWER)
            {
                APP_PRINT_INFO0("LOW POWER INDICATION: CAN NOT ENTER PAIRING MODE");
                return result;
            }

            PROFILE_PRINT_INFO0("ADV_UNDIRECT_PAIRING for pairing!");
#if FEATURE_SUPPORT_REMOVE_LINK_KEY_BEFORE_PAIRING
            if (app_global_data.is_link_key_existed)
            {
                app_global_data.is_link_key_existed = false;
                le_bond_clear_all_keys();
            }
#endif
#if FEATURE_SUPPORT_PRIVACY
            if (app_privacy_resolution_state == PRIVACY_ADDR_RESOLUTION_ENABLED)
            {
                privacy_set_addr_resolution(false);
            }
#endif
            adv_event_type = GAP_ADTYPE_ADV_IND;
            adv_channel_map = GAP_ADVCHAN_ALL;
            adv_filter_policy = GAP_ADV_FILTER_ANY;
            adv_interval_min = 0x20;  /* 20ms */
            adv_interval_max = 0x28;  /* 25ms */

            //Set RPA parameters
            if (app_global_data.en_rpa == 1)
            {
                local_bd_type = GAP_LOCAL_ADDR_LE_RAP_OR_PUBLIC;
                le_adv_set_param(GAP_PARAM_ADV_LOCAL_ADDR_TYPE, sizeof(local_bd_type), &local_bd_type);
            }

            le_adv_set_param(GAP_PARAM_ADV_EVENT_TYPE, sizeof(adv_event_type), &adv_event_type);
            le_adv_set_param(GAP_PARAM_ADV_CHANNEL_MAP, sizeof(adv_channel_map), &adv_channel_map);
            le_adv_set_param(GAP_PARAM_ADV_FILTER_POLICY, sizeof(adv_filter_policy), &adv_filter_policy);
            le_adv_set_param(GAP_PARAM_ADV_INTERVAL_MIN, sizeof(adv_interval_min), &adv_interval_min);
            le_adv_set_param(GAP_PARAM_ADV_INTERVAL_MAX, sizeof(adv_interval_max), &adv_interval_max);
            le_adv_set_param(GAP_PARAM_ADV_DATA, app_pairing_adv_data_len, app_pairing_adv_data);
            le_adv_set_param(GAP_PARAM_SCAN_RSP_DATA, app_scan_rsp_data_len, app_scan_rsp_data);

            if (GAP_CAUSE_SUCCESS == le_adv_start())
            {
                APP_PRINT_INFO0("rcu start ADV_UNDIRECT_PAIRING success!");
                app_global_data.rcu_status = RCU_STATUS_ADVERTISING;
                app_global_data.adv_type = ADV_UNDIRECT_PAIRING;

                if (true == app_global_data.is_link_key_existed)
                {
                    os_timer_restart(&adv_timer, ADV_UPDATE_INTERVAL_TIMEOUT);
                }
                else
                {
                    os_timer_restart(&adv_timer, ADV_UPDATE_INTERVAL_TIMEOUT);
                }
#if SUPPORT_LED_INDICATION_FEATURE
                LED_BLINK(LED_TYPE_BLINK_PAIR_ADV, 0);
#endif
                result = true;
            }
            else
            {
                APP_PRINT_WARN0("rcu start ADV_UNDIRECT_PAIRING fail!");
            }
        }
        break;
    case ADV_UNDIRECT_PAIRING_LT:
        {
            PROFILE_PRINT_INFO0("ADV_UNDIRECT_PAIRING_LT for pairing!");
#if FEATURE_SUPPORT_REMOVE_LINK_KEY_BEFORE_PAIRING
            if (app_global_data.is_link_key_existed)
            {
                app_global_data.is_link_key_existed = false;
                le_bond_clear_all_keys();
            }
#endif
#if FEATURE_SUPPORT_PRIVACY
            if (app_privacy_resolution_state == PRIVACY_ADDR_RESOLUTION_ENABLED)
            {
                privacy_set_addr_resolution(false);
            }
#endif
            adv_event_type = GAP_ADTYPE_ADV_IND;
            adv_channel_map = GAP_ADVCHAN_ALL;
            adv_filter_policy = GAP_ADV_FILTER_ANY;
            adv_interval_min = 0x30;  /* 30ms */
            adv_interval_max = 0x35;  /* 35ms */

            //Set RPA parameters
            if (app_global_data.en_rpa == 1)
            {
                local_bd_type = GAP_LOCAL_ADDR_LE_RAP_OR_PUBLIC;
                le_adv_set_param(GAP_PARAM_ADV_LOCAL_ADDR_TYPE, sizeof(local_bd_type), &local_bd_type);
            }

            le_adv_set_param(GAP_PARAM_ADV_EVENT_TYPE, sizeof(adv_event_type), &adv_event_type);
            le_adv_set_param(GAP_PARAM_ADV_CHANNEL_MAP, sizeof(adv_channel_map), &adv_channel_map);
            le_adv_set_param(GAP_PARAM_ADV_FILTER_POLICY, sizeof(adv_filter_policy), &adv_filter_policy);
            le_adv_set_param(GAP_PARAM_ADV_INTERVAL_MIN, sizeof(adv_interval_min), &adv_interval_min);
            le_adv_set_param(GAP_PARAM_ADV_INTERVAL_MAX, sizeof(adv_interval_max), &adv_interval_max);
            le_adv_set_param(GAP_PARAM_ADV_DATA, app_pairing_adv_data_len, app_pairing_adv_data);
            le_adv_set_param(GAP_PARAM_SCAN_RSP_DATA, app_scan_rsp_data_len, app_scan_rsp_data);

            if (GAP_CAUSE_SUCCESS == le_adv_start())
            {
                APP_PRINT_INFO0("rcu start ADV_UNDIRECT_PAIRING_LT success!");
                app_global_data.rcu_status = RCU_STATUS_ADVERTISING;
                app_global_data.adv_type = ADV_UNDIRECT_PAIRING_LT;

                if (true == app_global_data.is_link_key_existed)
                {
                    os_timer_restart(&adv_timer, ADV_UNDIRECT_REPAIRING_TIMEOUT - ADV_UPDATE_INTERVAL_TIMEOUT);
                }
                else
                {
                    os_timer_restart(&adv_timer, ADV_UNDIRECT_PAIRING_TIMEOUT - ADV_UPDATE_INTERVAL_TIMEOUT);
                }

#if SUPPORT_LED_INDICATION_FEATURE
                LED_BLINK(LED_TYPE_BLINK_PAIR_ADV, 0);
#endif
                result = true;
            }
            else
            {
                APP_PRINT_WARN0("rcu start ADV_UNDIRECT_PAIRING_LT fail!");
            }
        }
        break;
    default:
        /* invalid adv type */
        APP_PRINT_WARN1("[rcu_start_adv] unknown adv type: %d", adv_type);
        break;
    }

    return result;
}

/******************************************************************
 * @brief   stop advertising
 * @param   stop_adv_reason - indicate to the reason to stop adv
 * @return  result of stoping advertising
 * @retval  true or false
 */
bool rcu_stop_adv(T_STOP_ADV_REASON stop_adv_reason)
{
    bool result = false;
    app_global_data.stop_adv_reason = stop_adv_reason;

    if (app_global_data.rcu_status == RCU_STATUS_ADVERTISING)
    {
        if (le_adv_stop() == GAP_CAUSE_SUCCESS)
        {
            APP_PRINT_INFO0("[rcu_stop_adv] stop adv command sent successfully");
            app_global_data.rcu_status = RCU_STATUS_STOP_ADVERTISING;
            result = true;
        }
        else
        {
            APP_PRINT_WARN0("[rcu_stop_adv] stop adv command sent failed");
        }
    }
    else
    {
        APP_PRINT_WARN1("[rcu_stop_adv] Invalid RCU status: %d", app_global_data.rcu_status);
    }

    return result;
}

/******************************************************************
 * @brief   terminate connection
 * @param   disconn_reason - indicate to the reason to terminate connection
 * @return  result of terminating connection
 * @retval  true or false
 */
bool rcu_terminate_connection(T_DISCONN_REASON disconn_reason)
{
    bool result = false;
    app_global_data.disconn_reason = disconn_reason;

    if ((app_global_data.rcu_status == RCU_STATUS_CONNECTED)
        || (app_global_data.rcu_status == RCU_STATUS_PAIRED))
    {
        if (le_disconnect(0) == GAP_CAUSE_SUCCESS)
        {
            APP_PRINT_INFO0("[rcu_terminate_connection] terminate command sent successfully");
            result = true;
        }
        else
        {
            APP_PRINT_WARN0("[rcu_terminate_connection] terminate command sent failed");
        }
    }
    else
    {
        APP_PRINT_WARN1("[rcu_terminate_connection] Invalid RCU status: %d", app_global_data.rcu_status);
    }

    return result;

}

/******************************************************************
 * @brief   update peripheral connection parameters
 * @param   interval
 * @param   latency
 * @param   timeout
 * @return  none
 * @retval  void
 */
void rcu_update_conn_params(uint16_t interval, uint16_t latency, uint16_t timeout)
{
    if (GAP_CAUSE_SUCCESS != le_update_conn_param(0, interval, interval, latency, timeout / 10,
                                                  interval * 2 - 2,
                                                  interval * 2 - 2))
    {
        if (app_global_data.updt_conn_params_retry_cnt < MAX_UPDATE_CONN_PARAMS_RETRY_CNT)
        {
            app_global_data.updt_conn_params_retry_cnt++;
            APP_PRINT_WARN0("[rcu_update_conn_params] send HCI command retry");
            os_timer_restart(&update_conn_params_timer, UPDATE_CONN_PARAMS_TIMEOUT);
        }
        else
        {
            APP_PRINT_WARN0("[rcu_update_conn_params] send HCI command failed");
        }
    }
}

/******************* (C) COPYRIGHT 2020 Realtek Semiconductor Corporation *****END OF FILE****/
