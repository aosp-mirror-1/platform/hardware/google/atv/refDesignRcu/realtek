/**
*********************************************************************************************************
*               Copyright(c) 2017, Realtek Semiconductor Corporation. All rights reserved.
**********************************************************************************************************
* @file      bibuffer.c
* @brief    double buffer data struct.
* @details
* @author  elliot_chen
* @date     2017-06-05
* @version  v1.0
*********************************************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "bibuffer.h"

/* Globals -------------------------------------------------------------------*/
//RAM_BUFFERON_BSS_SECTION BiBuf_TypeDef    BiBuf __attribute__((used));
BiBuf_TypeDef   BiBuf;

/**
  * @brief  Initializes double buffer to their default reset values.
  * @param None.
  * @retval None
  */
void BiBuffer_Init(void)
{
    memset(&BiBuf, 0, sizeof(BiBuf_TypeDef));
    BiBuffer_SetBuf0Status(READ_DONE);
    BiBuffer_SetBuf1Status(READ_DONE);
}

/**
  * @brief  Get buffer address.
  * @param None.
  * @retval buffer address.
  */
uint32_t BiBuffer_GetBuf0Addr(void)
{
    return (uint32_t)BiBuf.buf0;
}

/**
  * @brief Set buffer status.
  * @param None.
  * @retval None.
  */
void BiBuffer_SetBuf0Status(BUF_STATUS status)
{
    BiBuf.buf0_stat = status;
}

/**
  * @brief  Get buffer status.
  * @param None.
  * @retval None.
  */
BUF_STATUS BiBuffer_GetBuf0Status(void)
{
    return BiBuf.buf0_stat;
}

/**
  * @brief  Get buffer address.
  * @param None.
  * @retval buffer address.
  */
uint32_t BiBuffer_GetBuf1Addr(void)
{
    return (uint32_t)BiBuf.buf1;
}

/**
  * @brief Set buffer status.
  * @param None.
  * @retval None.
  */
void BiBuffer_SetBuf1Status(BUF_STATUS status)
{
    BiBuf.buf1_stat = status;
}

/**
  * @brief  Get buffer status.
  * @param None.
  * @retval None.
  */
BUF_STATUS BiBuffer_GetBuf1Status(void)
{
    return BiBuf.buf1_stat;
}

/******************* (C) COPYRIGHT 2017 Realtek Semiconductor Corporation *****END OF FILE****/

