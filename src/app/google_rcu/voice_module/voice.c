/**
*********************************************************************************************************
*               Copyright(c) 2020, Realtek Semiconductor Corporation. All rights reserved.
**********************************************************************************************************
* @file     voice.c
* @brief    This is the entry of user code which the voice module resides in.
* @details
* @author
* @date     2020-03-05
* @version  v1.0
*********************************************************************************************************
*/

/*============================================================================*
 *                              Header Files
 *============================================================================*/
#include <trace.h>
#include <string.h>
#include <rtl876x_pinmux.h>
#include "board.h"
#include "voice.h"
#include "voice_driver.h"
#include "profile_server.h"
#include "profile_server.h"
#include "app_msg.h"
#include "rcu_application.h"
#include <app_section.h>
#include "swtimer.h"
#include "key_handle.h"
#include "os_timer.h"
#include "loop_queue.h"
#include "led_driver.h"
#include "ima_adpcm_enc.h"
#include "atvv_service.h"

#if SUPPORT_VOICE_FEATURE

/*============================================================================*
 *                              Local Variables
 *============================================================================*/
/* voice_exception_timer is used to detect audio transfer and active remote exception*/
static TimerHandle_t voice_exception_timer = NULL;

static T_LOOP_QUEUE_DEF *p_voice_queue;

/*============================================================================*
 *                              Global Variables
 *============================================================================*/
T_VOICE_GLOBAL_DATA voice_global_data;

/*============================================================================*
 *                              Functions Declaration
 *============================================================================*/
static void voice_handle_init_data(void);
static void voice_handle_init_encode_param(void);
static void voice_handle_deinit_encode_param(void);
static bool voice_handle_notify_voice_data(void);
static void voice_handle_encode_raw_data(uint8_t *p_input_data, int32_t input_size,
                                         uint8_t *p_output_data, int32_t *p_output_size);
static void voice_handle_gdma_event(T_IO_MSG io_driver_msg_recv);
static void voice_handle_exception_timer_cb(TimerHandle_t p_timer) DATA_RAM_FUNCTION;
bool voice_handle_start_mic(void);
void voice_handle_stop_mic(void);
bool voice_handle_atvv_audio_start(ATV_AUDIO_START_REASON reason);
bool voice_handle_atvv_audio_stop(ATV_AUDIO_STOP_REASON reason);
bool voice_handle_atvv_mic_open_error(ATV_MIC_OPEN_ERROR reason);
void voice_handle_atvv_init_data(void);
static void voice_handle_atv_audio_sync(uint16_t seq_id, uint16_t val_prev, uint8_t index);

/*============================================================================*
 *                              Local Functions
 *============================================================================*/
/******************************************************************
 * @brief   Initialize voice handle data.
 * @param   none
 * @return  none
 * @retval  void
 */
void voice_handle_init_data(void)
{
    APP_PRINT_INFO0("[voice_handle_init_data] init data");
    memset(&voice_global_data, 0, sizeof(voice_global_data));
}

/******************************************************************
 * @brief   init voice encode parameters.
 * @param   none
 * @return  none
 * @retval  void
 */
void voice_handle_init_encode_param(void)
{
    voice_global_data.is_encoder_init = false;
    memset(&ima_adpcm_global_state, 0, sizeof(ima_adpcm_global_state));
    voice_global_data.is_encoder_init = true;
}

/******************************************************************
 * @brief   deinit voice encode parameters.
 * @param   none
 * @return  none
 * @retval  void
 */
void voice_handle_deinit_encode_param(void)
{
    if (voice_global_data.is_encoder_init == true)
    {
        voice_global_data.is_encoder_init = false;
    }
}

/******************************************************************
 * @brief   voice handle out queue.
 * @param   none
 * @return  result
 * @retval  true or false
 */
bool voice_handle_notify_voice_data(void)
{
    bool result = false;
    uint8_t gap_link_credits = 0;
    uint8_t reserved_credits = 2;

    if (atvv_global_data.app_support_version == ATVV_VERSION_1_0)
    {
        reserved_credits += 1;  /* add one more for AUDIO_SYNC notification */
    }

    le_get_gap_param(GAP_PARAM_LE_REMAIN_CREDITS, &gap_link_credits);

    if (p_voice_queue == NULL)
    {
        APP_PRINT_WARN0("[voice_handle_out_queue] Queue buffer is NOT initialiezed!");
        result = false;
    }
    else if (loop_queue_is_empty(p_voice_queue) == true)
    {
        APP_PRINT_INFO0("[voice_handle_out_queue] Voice Queue is empty.");
        result = false;
    }
    else if (gap_link_credits <=
             reserved_credits)  /* reserve at least notification FIFO for key event */
    {
        APP_PRINT_WARN1("[voice_handle_out_queue] gap_link_credits is not enough is %d!", gap_link_credits);
        result = false;
    }
    else
    {
        uint32_t queue_item_cnt = loop_queue_get_vailid_data_size(p_voice_queue) / p_voice_queue->item_size;
        uint32_t loop_cnt = (queue_item_cnt <= gap_link_credits - reserved_credits) ? queue_item_cnt :
                            (gap_link_credits - reserved_credits);

        APP_PRINT_INFO3("[voice_handle_out_queue] gap_link_credits is %d, queue_item_cnt is %d, loop_cnt is %d",
                        gap_link_credits, queue_item_cnt, loop_cnt);

        while ((loop_cnt > 0) && (loop_queue_is_empty(p_voice_queue) == false))
        {
            loop_cnt--;
            /* attampt to send voice data */
            uint8_t notify_data_len = p_voice_queue->item_size;
            uint8_t *p_notify_buffer = os_mem_alloc(RAM_TYPE_DATA_ON, notify_data_len);

            if ((p_notify_buffer != NULL) &&
                (true == (loop_queue_copy_buf(p_voice_queue, p_notify_buffer, p_voice_queue->item_size))))
            {
                if (atvv_global_data.app_support_version == ATVV_VERSION_1_0)
                {
                    uint16_t seq_id = ((uint16_t)p_notify_buffer[0] << 8) + p_notify_buffer[1];
                    if (seq_id % (VOICE_AUDIO_SYNC_PERIOD + 1) == VOICE_AUDIO_SYNC_PERIOD)
                    {
                        uint16_t val_prev = ((uint16_t)p_notify_buffer[3] << 8) + p_notify_buffer[4];
                        uint8_t frame_index = p_notify_buffer[5];
                        voice_handle_atv_audio_sync(seq_id, val_prev, frame_index);
                    }

                    result = server_send_data(0, app_global_data.atvv_srv_id, GATT_SVC_ATVV_CHAR_RX_VALUE_INDEX, \
                                              p_notify_buffer + VOICE_FRAME_HEADER_SIZE, notify_data_len - VOICE_FRAME_HEADER_SIZE,
                                              GATT_PDU_TYPE_NOTIFICATION);
                }
                else
                {
                    result = server_send_data(0, app_global_data.atvv_srv_id, GATT_SVC_ATVV_CHAR_RX_VALUE_INDEX, \
                                              p_notify_buffer, notify_data_len, GATT_PDU_TYPE_NOTIFICATION);
                }

                if (result == true)
                {
                    result = loop_queue_read_buf(p_voice_queue, p_notify_buffer, p_voice_queue->item_size);
                }

                os_mem_free(p_notify_buffer);
            }
            else
            {
                result = false;
            }
        }
    }

    return result;
}

/******************************************************************
 * @brief   voice handle encode raw datae.
 * @param   p_input_data - point of input data
 * @param   input_size - input size
 * @param   p_output_data - point of output data
 * @param   p_output_size - point of output size
 * @return  none
 * @retval  void
 */
void voice_handle_encode_raw_data(uint8_t *p_input_data, int32_t input_size,
                                  uint8_t *p_output_data, int32_t *p_output_size)
{
    int32_t tmp_size;
    p_output_data[0] = (uint8_t)(ima_adpcm_global_state.seq_id >> 8);//cppcheck-suppress [ctuArrayIndex]
    p_output_data[1] = (uint8_t)(ima_adpcm_global_state.seq_id);
    ima_adpcm_global_state.seq_id++;
    p_output_data[2] = 0;
    p_output_data[3] = (uint8_t)(ima_adpcm_global_state.valprev >> 8);
    p_output_data[4] = (uint8_t) ima_adpcm_global_state.valprev;
    p_output_data[5] = ima_adpcm_global_state.index;

    tmp_size = ima_adpcm_encode((void *)p_input_data, &p_output_data[VOICE_FRAME_HEADER_SIZE],
                                VOICE_PCM_FRAME_SIZE / 2,
                                &ima_adpcm_global_state);
    *p_output_size = tmp_size + VOICE_FRAME_HEADER_SIZE;

    APP_PRINT_INFO1("[voice_handle_encode_raw_data] *p_output_size = %d", *p_output_size);
}

/******************************************************************
 * @brief   voice handle GDMA evnet.
 * @param   io_driver_msg_recv - gdma message
 * @return  none
 * @retval  void
 */
void voice_handle_gdma_event(T_IO_MSG io_driver_msg_recv)
{
    if (!voice_driver_global_data.is_voice_driver_working)
    {
        return;
    }

    int32_t output_size = 0;
    uint8_t encode_output_buffer[p_voice_queue->item_size];

    /* skip first several GMDA interrupts, to wait codec stable */
    if (voice_global_data.voice_gdma_int_cnt < VOICE_GDMA_INT_SKIP_CNT)
    {
        voice_global_data.voice_gdma_int_cnt++;
        return;
    }

    if (true == voice_global_data.is_pending_to_stop_recording)
    {
        if ((loop_queue_is_empty(p_voice_queue) == true) ||
            (voice_global_data.is_allowed_to_notify_voice_data == false))
        {
            /* voice buffer data has all been sent after voice key up */
            if (ATV_ASSISTANT_INTERACTION_MODEL_HOLD_TO_TALK == atvv_global_data.assistant_interaction_model)
            {
                voice_handle_atvv_audio_stop(ATV_STOP_REASON_HTT);
            }
        }
        else
        {
            voice_handle_notify_voice_data();
        }
    }
    else if (voice_global_data.is_encoder_init == true)
    {
        /* encode raw data */
        voice_handle_encode_raw_data(io_driver_msg_recv.u.buf, VOICE_GDMA_FRAME_SIZE, encode_output_buffer,
                                     &output_size);

        if (output_size == p_voice_queue->item_size)
        {
            /* set is_overflow_data_abandoned to false, to drop oldest data if queue is full */
            loop_queue_write_buf(p_voice_queue, encode_output_buffer, p_voice_queue->item_size, false);

            if (voice_global_data.is_allowed_to_notify_voice_data == true)
            {
                voice_handle_notify_voice_data();
            }
            else
            {
                APP_PRINT_INFO0("[voice_handle_gdma_event] not allowed to notify voice data");
            }
        }
        else
        {
            APP_PRINT_WARN2("[voice_handle_gdma_event] encode failed %d %d", output_size,
                            p_voice_queue->item_size);
        }
    }
    else
    {
        APP_PRINT_WARN0("[voice_handle_gdma_event] Encoder is NOT initialized");
    }
}

/******************************************************************
 * @brief   voice exception timer callback.
 * @param   p_timer - point of timer
 * @return  none
 * @retval  void
 * @note    do NOT execute time consumption functions in timer callback
 */
void voice_handle_exception_timer_cb(TimerHandle_t p_timer)
{
    if (voice_driver_global_data.is_voice_driver_working == true)
    {
        APP_PRINT_INFO0("[voice_handle_exception_timer_cb] audio transfer timeout");
        voice_handle_atvv_audio_stop(ATV_STOP_REASON_AUDIO_TRANSFER_TIMEOUT);
    }
    else
    {
        APP_PRINT_INFO0("[voice_handle_exception_timer_cb] active remote timeout");
        voice_handle_atvv_mic_open_error(ATV_MIC_OPEN_ERROR_REMOTE_IS_NOT_ACTIVE);
        app_set_latency_status(LATENCY_VOICE_PROC_BIT, LANTENCY_ON);
    }
}

/*============================================================================*
 *                              Global Functions
 *============================================================================*/
/******************************************************************
 * @brief   voice handle start mic and recording.
 * @param   none
 * @return  result
 * @retval  true or false
 */
bool voice_handle_start_mic(void)
{
    uint16_t item_size = 0;
    uint16_t queue_size = 0;

    if (voice_driver_global_data.is_voice_driver_working == true)
    {
        APP_PRINT_INFO0("[voice_handle_start_mic] Voice driver is working, start failed!");
        return false;
    }

    APP_PRINT_INFO0("[voice_handle_start_mic] start recording!");

    voice_handle_init_data();
    voice_handle_init_encode_param();
    item_size = VOICE_REPORT_FRAME_SIZE;
    if (item_size == 0)
    {
        APP_PRINT_WARN0("[voice_handle_start_mic] Invalid item_size!");
        return false;
    }
    queue_size = VOICE_QUEUE_MAX_BUFFER_SIZE / item_size;
    p_voice_queue = loop_queue_init(queue_size, item_size, RAM_TYPE_DATA_ON);

    voice_driver_init();

    return true;
}

/******************************************************************
 * @brief   voice handle stop mic and recording.
 * @param   none
 * @return  result
 * @retval  true or false
 */
void voice_handle_stop_mic(void)
{
    APP_PRINT_INFO0("[voice_handle_stop_mic] stop recording!");

#if SUPPORT_LED_INDICATION_FEATURE
    if (atvv_global_data.atv_start_reason == ATV_START_REASON_MIC_OPEN_REQUEST)
    {
        LED_BLINK_EXIT(LED_TYPE_BLINK_VOICE_ON_REQUEST);
    }
    else if (atvv_global_data.atv_start_reason == ATV_START_REASON_HTT)
    {
        LED_BLINK_EXIT(LED_TYPE_BLINK_VOICE_HTT);
    }
    else if (atvv_global_data.atv_start_reason == ATV_START_REASON_PTT)
    {
        LED_BLINK_EXIT(LED_TYPE_BLINK_VOICE_PTT);
    }
#endif

    if (voice_exception_timer)
    {
        os_timer_stop(&voice_exception_timer);
    }

    if (voice_driver_global_data.is_voice_driver_working == false)
    {
        APP_PRINT_INFO0("[voice_handle_stop_mic] Voice driver is not working, stop failed!");
    }
    else
    {
        voice_driver_deinit();
        voice_handle_deinit_encode_param();
        loop_queue_deinit(&p_voice_queue);
    }
}

/******************************************************************
 * @brief   vocie key press msg handle.
 * @param   none
 * @return  result
 * @retval  true or false
 */
bool voice_handle_mic_key_pressed(void)
{
    bool ret = true;

    if (true == voice_driver_global_data.is_voice_driver_working)
    {
        APP_PRINT_WARN0("[voice_handle_mic_key_pressed] Voice driver is Working, start recording failed!");
        return false;
    }

    APP_PRINT_INFO1("[voice_handle_mic_key_pressed] assistant_interaction_model is %d",
                    atvv_global_data.assistant_interaction_model);

    if (ATV_ASSISTANT_INTERACTION_MODEL_ON_REQUEST == atvv_global_data.assistant_interaction_model)
    {
        /* send ATV_CTL_OPCODE_START_SEARCH */
        memset(atvv_global_data.char_ctl_data_buff, 0, ATVV_CHAR_CTL_DATA_LEN);
        atvv_global_data.char_ctl_data_buff[0] = ATV_CTL_OPCODE_START_SEARCH;
        server_send_data(0, app_global_data.atvv_srv_id, GATT_SVC_ATVV_CHAR_CTL_VALUE_INDEX,
                         atvv_global_data.char_ctl_data_buff, 1, GATT_PDU_TYPE_NOTIFICATION);

        key_handle_notify_hid_key_event_by_index(KEY_ID_Assistant);
        key_handle_notify_hid_release_event();

        app_set_latency_status(LATENCY_VOICE_PROC_BIT,
                               LANTENCY_OFF);  /* off latency to speed up voice process */
        /* start voice exception timer */
        if (voice_exception_timer == NULL)
        {
            if (true == os_timer_create(&voice_exception_timer, "voice_exception_timer",  1, \
                                        VOICE_ACTIVE_REMOTE_TIMEOUT, false, voice_handle_exception_timer_cb))
            {
                os_timer_restart(&voice_exception_timer, VOICE_ACTIVE_REMOTE_TIMEOUT);
            }
            else
            {
                APP_PRINT_ERROR0("[voice_handle_mic_key_pressed] voice_exception_timer creat failed!");
            }
        }
        else
        {
            os_timer_restart(&voice_exception_timer, VOICE_ACTIVE_REMOTE_TIMEOUT);
        }
    }
    else if (ATV_ASSISTANT_INTERACTION_MODEL_PRESS_TO_TALK ==
             atvv_global_data.assistant_interaction_model)
    {
        if (app_global_data.is_atv_voice_notify_en == true)
        {
            voice_handle_atvv_audio_start(ATV_START_REASON_PTT);
        }
        else
        {
            voice_handle_atvv_mic_open_error(ATV_MIC_OPEN_ERROR_ATVV_CHAR_AUDIO_IS_DISABLE);
            ret = false;
        }
    }
    else if (ATV_ASSISTANT_INTERACTION_MODEL_HOLD_TO_TALK ==
             atvv_global_data.assistant_interaction_model)
    {
        if (app_global_data.is_atv_voice_notify_en == true)
        {
            voice_handle_atvv_audio_start(ATV_START_REASON_HTT);
        }
        else
        {
            voice_handle_atvv_mic_open_error(ATV_MIC_OPEN_ERROR_ATVV_CHAR_AUDIO_IS_DISABLE);
            ret = false;
        }
    }

    return ret;
}

/******************************************************************
 * @brief   vocie key release msg handle.
 * @param   none
 * @return  none
 * @retval  void
 */
void voice_handle_mic_key_released(void)
{
    if (voice_driver_global_data.is_voice_driver_working == false)
    {
        APP_PRINT_WARN0("[voice_handle_mic_key_released] Voice driver is not working!");
        return;
    }

    if (ATV_START_REASON_MIC_OPEN_REQUEST ==
        atvv_global_data.atv_start_reason)
    {
        key_handle_notify_hid_release_event();
    }
    else if (ATV_START_REASON_HTT ==
             atvv_global_data.atv_start_reason)
    {
        if (loop_queue_is_empty(p_voice_queue) ||
            (false == voice_global_data.is_allowed_to_notify_voice_data) ||
            (p_voice_queue == NULL) ||
            (app_global_data.mtu_size - 3 < p_voice_queue->item_size))
        {
            /* stop voice recording immediately */
            voice_handle_atvv_audio_stop(ATV_STOP_REASON_HTT);
        }
        else
        {
            /* stop voice in queue, and wait for all buffered voice data send */
            voice_global_data.is_pending_to_stop_recording = true;
        }
    }
}

/******************************************************************
 * @brief   Initialize atv global data.
 * @param   none
 * @return  none
 * @retval  void
 */
void voice_handle_atvv_init_data(void)
{
    APP_PRINT_INFO0("[voice_handle_atvv_init_data] init data");

    memset(&atvv_global_data, 0, sizeof(T_ATVV_GLOBAL_DATA));
    atvv_global_data.atv_start_reason = ATV_START_REASON_INVALID;

#if (ATV_VOICE_VERSION == ATV_VERSION_1_0)
    atvv_global_data.app_support_version = ATVV_VERSION_1_0;
#elif (ATV_VOICE_VERSION == ATV_VERSION_0_4)
    atvv_global_data.app_support_version = ATVV_VERSION_0_4;
#endif

    atvv_global_data.codec_support = ATTV_CODEC_MASK_8K_ADPCM | ATTV_CODEC_MASK_16K_ADPCM;

#if (ATV_VOICE_INTERACTION_MODEL == ATV_INTERACTION_MODEL_ON_REQUEST)
    atvv_global_data.assistant_interaction_model = ATV_ASSISTANT_INTERACTION_MODEL_ON_REQUEST;
#elif (ATV_VOICE_INTERACTION_MODEL == ATV_INTERACTION_MODEL_PRESS_TO_TALK)
    atvv_global_data.assistant_interaction_model = ATV_ASSISTANT_INTERACTION_MODEL_PRESS_TO_TALK;
#elif (ATV_VOICE_INTERACTION_MODEL == ATV_INTERACTION_MODEL_HOLD_TO_TALK)
    atvv_global_data.assistant_interaction_model = ATV_ASSISTANT_INTERACTION_MODEL_HOLD_TO_TALK;
#endif
}

/******************************************************************
 * @brief   ATV voice handle start voice and response to master.
 * @param   none
 * @return  result
 * @retval  true or false
 */
bool voice_handle_atvv_audio_start(ATV_AUDIO_START_REASON reason)
{
    bool result = false;
    uint8_t stream_id = 0;

    APP_PRINT_INFO1("[voice_handle_atvv_audio_start] ATV_CTL_OPCODE_AUDIO_START, reason is 0x%x",
                    reason);

    atvv_global_data.atv_start_reason = reason;

    voice_handle_start_mic();

    app_set_latency_status(LATENCY_VOICE_PROC_BIT, LANTENCY_OFF);
    voice_global_data.is_allowed_to_notify_voice_data = true;

    /* send Audio Start Notification */
    memset(atvv_global_data.char_ctl_data_buff, 0, ATVV_CHAR_CTL_DATA_LEN);
    atvv_global_data.char_ctl_data_buff[0] = ATV_CTL_OPCODE_AUDIO_START;
    if (ATVV_VERSION_1_0 == atvv_global_data.app_support_version)
    {
        if (reason == ATV_START_REASON_MIC_OPEN_REQUEST)
        {
            stream_id = 0x00;
        }
        else
        {
            atvv_global_data.stream_id = (atvv_global_data.stream_id % 0x80) + 1;
            stream_id = atvv_global_data.stream_id;
        }
        APP_PRINT_INFO1("[voice_handle_atvv_audio_start] rcu stream_id is 0x%x",
                        atvv_global_data.stream_id);
        atvv_global_data.char_ctl_data_buff[1] = reason;
        atvv_global_data.char_ctl_data_buff[2] = CODEC_SAMPLE_RATE_SEL;
        atvv_global_data.char_ctl_data_buff[3] = stream_id;

        atvv_global_data.codec_used = CODEC_SAMPLE_RATE_SEL;

        result = server_send_data(0, app_global_data.atvv_srv_id, GATT_SVC_ATVV_CHAR_CTL_VALUE_INDEX,
                                  atvv_global_data.char_ctl_data_buff, 4, GATT_PDU_TYPE_NOTIFICATION);
    }
    else
    {
        result = server_send_data(0, app_global_data.atvv_srv_id, GATT_SVC_ATVV_CHAR_CTL_VALUE_INDEX,
                                  atvv_global_data.char_ctl_data_buff, 1, GATT_PDU_TYPE_NOTIFICATION);
    }


    if (result == false)
    {
        APP_PRINT_WARN0("[voice_handle_atvv_audio_start] ATV_CTL_OPCODE_AUDIO_START notify failed!");
    }

    /* start voice exception timer */
    if (voice_exception_timer == NULL)
    {
        if (true == os_timer_create(&voice_exception_timer, "voice_exception_timer",  1, \
                                    VOICE_TRANSFER_TIMEOUT, false, voice_handle_exception_timer_cb))
        {
            os_timer_restart(&voice_exception_timer, VOICE_TRANSFER_TIMEOUT);
        }
        else
        {
            APP_PRINT_ERROR0("[voice_handle_mic_key_pressed] voice_exception_timer creat failed!");
        }
    }
    else
    {
        os_timer_restart(&voice_exception_timer, VOICE_TRANSFER_TIMEOUT);
    }
#if SUPPORT_LED_INDICATION_FEATURE
    if (result)
    {
        if (reason == ATV_START_REASON_MIC_OPEN_REQUEST)
        {
            LED_BLINK(LED_TYPE_BLINK_VOICE_ON_REQUEST, 0);
        }
        else if (reason == ATV_START_REASON_HTT)
        {
            LED_BLINK(LED_TYPE_BLINK_VOICE_HTT, 0);
        }
        else if (reason == ATV_START_REASON_PTT)
        {
            LED_BLINK(LED_TYPE_BLINK_VOICE_PTT, 0);
        }
    }
#endif
    return result;
}

/******************************************************************
 * @brief   ATV voice handle stop voice and response to master.
 * @param   none
 * @return  result
 * @retval  true or false
 */
bool voice_handle_atvv_audio_stop(ATV_AUDIO_STOP_REASON reason)
{
    bool notify_result = false;
    APP_PRINT_INFO1("[voice_handle_atvv_audio_stop] ATV_CTL_OPCODE_AUDIO_STOP, reason is 0x%x", reason);
    memset(atvv_global_data.char_ctl_data_buff, 0, ATVV_CHAR_CTL_DATA_LEN);
    atvv_global_data.char_ctl_data_buff[0] = ATV_CTL_OPCODE_AUDIO_STOP;
    if (ATVV_VERSION_1_0 == atvv_global_data.app_support_version)
    {
        atvv_global_data.char_ctl_data_buff[1] = reason;

        notify_result = server_send_data(0, app_global_data.atvv_srv_id, GATT_SVC_ATVV_CHAR_CTL_VALUE_INDEX,
                                         atvv_global_data.char_ctl_data_buff, 2, GATT_PDU_TYPE_NOTIFICATION);
    }
    else
    {
        notify_result = server_send_data(0, app_global_data.atvv_srv_id, GATT_SVC_ATVV_CHAR_CTL_VALUE_INDEX,
                                         atvv_global_data.char_ctl_data_buff, 1, GATT_PDU_TYPE_NOTIFICATION);
    }

    if (notify_result == false)
    {
        APP_PRINT_WARN0("[voice_handle_atvv_audio_stop] ATV_CTL_OPCODE_AUDIO_STOP notify failed!");
    }
    voice_global_data.is_allowed_to_notify_voice_data = false;
    voice_handle_stop_mic();
    app_set_latency_status(LATENCY_VOICE_PROC_BIT, LANTENCY_ON);

    atvv_global_data.atv_start_reason = ATV_START_REASON_INVALID;

    return notify_result;
}

/******************************************************************
 * @brief   ATV voice handle mic open error and response to master.
 * @param   none
 * @return  result
 * @retval  true or false
 */
bool voice_handle_atvv_mic_open_error(ATV_MIC_OPEN_ERROR reason)
{
    bool notify_result = false;
    APP_PRINT_INFO1("[voice_handle_atvv_mic_open_error] MIC Open Error, reason is 0x%x!", reason);
    memset(atvv_global_data.char_ctl_data_buff, 0, ATVV_CHAR_CTL_DATA_LEN);
    atvv_global_data.char_ctl_data_buff[0] = ATV_CTL_OPCODE_MIC_OPEN_ERROR;
    atvv_global_data.char_ctl_data_buff[1] = (uint8_t)(reason >> 8);
    atvv_global_data.char_ctl_data_buff[2] = (uint8_t)reason;
    notify_result = server_send_data(0, app_global_data.atvv_srv_id, GATT_SVC_ATVV_CHAR_CTL_VALUE_INDEX,
                                     atvv_global_data.char_ctl_data_buff, 3, GATT_PDU_TYPE_NOTIFICATION);
    if (notify_result == false)
    {
        APP_PRINT_WARN0("[voice_handle_atvv_mic_open_error] ATV_CTL_OPCODE_MIC_OPEN_ERROR notify failed!");
    }
    return notify_result;
}

/******************************************************************
 * @brief   send AUDIO_SYNC for ATV
 * @param   none
 * @return  none
 * @retval  void
 */
void voice_handle_atv_audio_sync(uint16_t seq_id, uint16_t val_prev, uint8_t index)
{
    APP_PRINT_INFO0("[voice_handle_atv_audio_sync] ATV_CTL_OPCODE_AUDIO_SYNC");
    memset(atvv_global_data.char_ctl_data_buff, 0, ATVV_CHAR_CTL_DATA_LEN);
    atvv_global_data.char_ctl_data_buff[0] = ATV_CTL_OPCODE_AUDIO_SYNC;
    atvv_global_data.char_ctl_data_buff[1] = atvv_global_data.codec_used;
    atvv_global_data.char_ctl_data_buff[2] = (uint8_t)(seq_id >> 8);
    atvv_global_data.char_ctl_data_buff[3] = (uint8_t)seq_id;
    atvv_global_data.char_ctl_data_buff[4] = (uint8_t)(val_prev >> 8);
    atvv_global_data.char_ctl_data_buff[5] = (uint8_t)val_prev;
    atvv_global_data.char_ctl_data_buff[6] = index;
    bool notify_result = server_send_data(0, app_global_data.atvv_srv_id,
                                          GATT_SVC_ATVV_CHAR_CTL_VALUE_INDEX,
                                          atvv_global_data.char_ctl_data_buff, 7,
                                          GATT_PDU_TYPE_NOTIFICATION);
    if (notify_result == false)
    {
        APP_PRINT_WARN0("[voice_handle_atv_audio_sync] ATV_CTL_OPCODE_AUDIO_SYNC notify failed!");
    }
}

/******************************************************************
 * @brief   handle DPAD select event
 * @param   none
 * @return  none
 * @retval  void
 */
void voice_handle_atv_dpad_select(void)
{
    APP_PRINT_INFO1("[voice_handle_atv_dpad_select] atv version is %d",
                    atvv_global_data.app_support_version);

    if (ATVV_VERSION_0_4 == atvv_global_data.app_support_version)
    {
        bool result = false;

        /* send Audio Start Notification */
        memset(atvv_global_data.char_ctl_data_buff, 0, ATVV_CHAR_CTL_DATA_LEN);
        atvv_global_data.char_ctl_data_buff[0] = ATV_CTL_OPCODE_AUDIO_START;

        result = server_send_data(0, app_global_data.atvv_srv_id, GATT_SVC_ATVV_CHAR_CTL_VALUE_INDEX,
                                  atvv_global_data.char_ctl_data_buff, 1, GATT_PDU_TYPE_NOTIFICATION);
        if (result == false)
        {
            APP_PRINT_WARN0("[voice_handle_atv_dpad_select] ATV_CTL_OPCODE_AUDIO_START notify failed!");
        }
    }

    if (temp_off_latency_timer != NULL)
    {
        /* turn off latency to speed up voice process */
        app_set_latency_status(LATENCY_TEMP_OFF_BIT, LANTENCY_OFF);
        /* start temp_off_latency_timer */
        os_timer_restart(&temp_off_latency_timer, TEMP_OFF_LANTENCY_TIMEOUT);
    }
}

/******************************************************************
 * @brief   voice module msg handle.
 * @param   msg_type - voice message type
 * @param   p_data - point to voice message data
 * @return  result
 * @retval  true or false
 */
bool voice_handle_messages(T_VOICE_MSG_TYPE msg_type, void *p_data)
{
    bool ret = true;

    APP_PRINT_INFO1("[voice_handle_messages] msg_type is %d", msg_type);

    switch (msg_type)
    {
    case VOICE_MSG_PERIPHERAL_GDMA:
        {
            voice_handle_gdma_event(*(T_IO_MSG *)p_data);
        }
        break;
    case VOICE_MSG_BT_SEND_COMPLETE:
        {
            if (voice_global_data.is_allowed_to_notify_voice_data)
            {
                voice_handle_notify_voice_data();
            }
        }
        break;

    default:
        APP_PRINT_INFO0("[voice_handle_messages] unknown msg type!");
        ret = false;
        break;
    }
    return ret;
}

/******************************************************************
 * @fn       voice_handle_atvv_srv_cb
 * @brief    ATV service callbacks are handled in this function.
 * @param    p_data  - pointer to callback data
 * @return   cb_result
 * @retval   T_APP_RESULT
 */
T_APP_RESULT voice_handle_atvv_srv_cb(T_ATVV_CALLBACK_DATA *p_data)
{
    bool notify_result = false;
    T_APP_RESULT cb_result = APP_RESULT_SUCCESS;

    switch (p_data->msg_type)
    {
    case SERVICE_CALLBACK_TYPE_INDIFICATION_NOTIFICATION:
        {
            /* add user code here */
            switch (p_data->msg_data.notification_indification_index)
            {
            case ATVV_CHAR_RX_NOTIFY_ENABLE:
                APP_PRINT_INFO0("[voice_handle_atvv_srv_cb] ATVV_CHAR_RX_NOTIFY_ENABLE");
                app_global_data.is_atv_voice_notify_en = true;
                break;
            case ATVV_CHAR_RX_NOTIFY_DISABLE:
                APP_PRINT_INFO0("[voice_handle_atvv_srv_cb] ATVV_CHAR_RX_NOTIFY_DISABLE");
                app_global_data.is_atv_voice_notify_en = false;
                if (ATVV_VERSION_1_0 == atvv_global_data.app_support_version)
                {
                    if (voice_driver_global_data.is_voice_driver_working == true)
                    {
                        voice_handle_atvv_audio_stop(ATV_STOP_REASON_ATVV_CHAR_AUDIO_DISABLE);
                    }
                }
                break;
            default:
                break;
            }
        }
        break;

    case SERVICE_CALLBACK_TYPE_READ_CHAR_VALUE:
        {
            /* add user code here */
        }
        break;

    case SERVICE_CALLBACK_TYPE_WRITE_CHAR_VALUE:
        {
            uint8_t report_len = p_data->msg_data.write.write_parameter.report_data.len;
            uint8_t *report_data_ptr = p_data->msg_data.write.write_parameter.report_data.report;

            APP_PRINT_INFO1("[voice_handle_atvv_srv_cb] SERVICE_CALLBACK_TYPE_WRITE_CHAR_VALUE report_len = %d",
                            report_len);

            if ((report_len > 0) && (report_len <= ATVV_CHAR_WRITE_DATA_LEN))
            {
                APP_PRINT_INFO1("[voice_handle_atvv_srv_cb] report data = %b", TRACE_BINARY(report_len,
                                                                                            report_data_ptr));

                switch (report_data_ptr[0])
                {
                case ATV_TX_OPCODE_GET_CAPS:
                    {
                        /* parse received data */
                        uint8_t parsed_version_major = report_data_ptr[1];
                        uint8_t parsed_version_minor = report_data_ptr[2];
                        uint16_t parsed_codecs = ((uint16_t)report_data_ptr[3] << 8) + report_data_ptr[4];

                        APP_PRINT_INFO3("[voice_handle_atvv_srv_cb] ATV_TX_OPCODE_GET_CAPS V%d.%d codec = %d",
                                        parsed_version_major, parsed_version_minor, parsed_codecs);

                        /* choose atv version */
                        uint8_t app_support_major_version = (uint8_t)(atvv_global_data.app_support_version >> 8);
                        if (app_support_major_version > parsed_version_major)
                        {
                            app_support_major_version = parsed_version_major;
                            if (app_support_major_version == 0)
                            {
                                atvv_global_data.app_support_version = ATVV_VERSION_0_4;
                            }
                            else
                            {
                                atvv_global_data.app_support_version = ATVV_VERSION_1_0;
                            }
                        }
                        APP_PRINT_INFO3("[voice_handle_atvv_srv_cb] APP use ATV version V%d.%d codec = %d",
                                        (uint8_t)(atvv_global_data.app_support_version >> 8), (uint8_t)atvv_global_data.app_support_version,
                                        atvv_global_data.codec_support);

                        /* send response */
                        if (ATVV_VERSION_1_0 == atvv_global_data.app_support_version)
                        {
#if (ATV_VOICE_INTERACTION_MODEL == ATV_INTERACTION_MODEL_ON_REQUEST)
                            atvv_global_data.assistant_interaction_model = ATV_ASSISTANT_INTERACTION_MODEL_ON_REQUEST;
#elif (ATV_VOICE_INTERACTION_MODEL == ATV_INTERACTION_MODEL_PRESS_TO_TALK)
                            atvv_global_data.assistant_interaction_model = ATV_ASSISTANT_INTERACTION_MODEL_PRESS_TO_TALK;
#elif (ATV_VOICE_INTERACTION_MODEL == ATV_INTERACTION_MODEL_HOLD_TO_TALK)
                            atvv_global_data.assistant_interaction_model = ATV_ASSISTANT_INTERACTION_MODEL_HOLD_TO_TALK;
#endif
                            atvv_global_data.codec_used = ATTV_CODEC_MASK_16K_ADPCM;
                            uint8_t mast_support_assistant_interaction_model = report_data_ptr[5];
                            uint16_t audio_frame_size = VOICE_FRAME_SIZE_AFTER_ENC * VOICE_PCM_FRAME_CNT;

                            if (atvv_global_data.assistant_interaction_model > mast_support_assistant_interaction_model)
                            {
                                atvv_global_data.assistant_interaction_model = mast_support_assistant_interaction_model;
                            }
                            APP_PRINT_INFO2("[voice_handle_atvv_srv_cb] assistant_interaction_model-- master support: %d, rcu use: %d",
                                            mast_support_assistant_interaction_model, atvv_global_data.assistant_interaction_model);

                            memset(atvv_global_data.char_ctl_data_buff, 0, ATVV_CHAR_CTL_DATA_LEN);
                            atvv_global_data.char_ctl_data_buff[0] = ATV_CTL_OPCODE_GET_CAPS_RESP;
                            atvv_global_data.char_ctl_data_buff[1] = (uint8_t)(atvv_global_data.app_support_version >> 8);
                            atvv_global_data.char_ctl_data_buff[2] = (uint8_t)atvv_global_data.app_support_version;
                            atvv_global_data.char_ctl_data_buff[3] = (uint8_t)
                                                                     atvv_global_data.codec_support; /* codecs supported */
                            atvv_global_data.char_ctl_data_buff[4] =
                                atvv_global_data.assistant_interaction_model; /*assistant interaction model*/
                            atvv_global_data.char_ctl_data_buff[5] = (uint8_t)(audio_frame_size >> 8);
                            atvv_global_data.char_ctl_data_buff[6] = (uint8_t) audio_frame_size;  /* bytes/audio frame size*/
                            atvv_global_data.char_ctl_data_buff[7] = ATVV_DLE_AND_MTU_UPDATE_DISABLE; /* ectra configuration */
                            atvv_global_data.char_ctl_data_buff[8] = 0x00;  /* reserved*/
                        }
                        else if (ATVV_VERSION_0_4 == atvv_global_data.app_support_version)
                        {
                            atvv_global_data.assistant_interaction_model = ATV_ASSISTANT_INTERACTION_MODEL_ON_REQUEST;

                            memset(atvv_global_data.char_ctl_data_buff, 0, ATVV_CHAR_CTL_DATA_LEN);
                            atvv_global_data.char_ctl_data_buff[0] = ATV_CTL_OPCODE_GET_CAPS_RESP;
                            atvv_global_data.char_ctl_data_buff[1] = (uint8_t)(atvv_global_data.app_support_version >> 8);
                            atvv_global_data.char_ctl_data_buff[2] = (uint8_t)atvv_global_data.app_support_version;
                            atvv_global_data.char_ctl_data_buff[3] = (uint8_t)(atvv_global_data.codec_support >> 8);
                            atvv_global_data.char_ctl_data_buff[4] = (uint8_t)
                                                                     atvv_global_data.codec_support;  /* codec_support */
                            atvv_global_data.char_ctl_data_buff[5] = 0x00;
                            atvv_global_data.char_ctl_data_buff[6] = (uint8_t)VOICE_REPORT_FRAME_SIZE;  /* bytes/frame */
                            atvv_global_data.char_ctl_data_buff[7] = 0x00;
                            atvv_global_data.char_ctl_data_buff[8] = 0x14;  /* bytes/char */
                        }

                        notify_result = server_send_data(0, app_global_data.atvv_srv_id, GATT_SVC_ATVV_CHAR_CTL_VALUE_INDEX,
                                                         atvv_global_data.char_ctl_data_buff, 9, GATT_PDU_TYPE_NOTIFICATION);
                        if (notify_result == false)
                        {
                            APP_PRINT_WARN0("[voice_handle_atvv_srv_cb] ATV_TX_OPCODE_GET_CAPS notify failed!");
                        }
                    }
                    break;

                case ATV_TX_OPCODE_MIC_OPEN:
                    {
                        APP_PRINT_INFO0("[voice_handle_atvv_srv_cb] ATV_TX_OPCODE_MIC_OPEN");
                        if (ATVV_VERSION_1_0 == atvv_global_data.app_support_version)
                        {
                            if ((atvv_global_data.atv_start_reason != ATV_START_REASON_PTT)
                                && (atvv_global_data.atv_start_reason != ATV_START_REASON_HTT))
                            {
                                if (app_global_data.is_atv_voice_notify_en == true)
                                {
                                    atvv_global_data.audio_consumption_mode = report_data_ptr[1];
                                    if (voice_driver_global_data.is_voice_driver_working == true)
                                    {
                                        voice_handle_atvv_audio_stop(ATV_STOP_REASON_AUDIO_START_COMMAND);
                                    }
                                    voice_handle_atvv_audio_start(ATV_START_REASON_MIC_OPEN_REQUEST);
                                }
                                else
                                {
                                    voice_handle_atvv_mic_open_error(ATV_MIC_OPEN_ERROR_ATVV_CHAR_AUDIO_IS_DISABLE);
                                }
                            }
                            else
                            {
                                APP_PRINT_WARN0("[voice_handle_atvv_srv_cb] atv assistant_interaction_model is not ATV_ASSISTANT_INTERACTION_MODEL_ON_REQUEST");
                                voice_handle_atvv_mic_open_error(ATV_MIC_OPEN_ERROR_PTT_HTT_IS_IN_PROGRESS);
                            }
                        }
                        else if (ATVV_VERSION_0_4 == atvv_global_data.app_support_version)
                        {
                            uint16_t parsed_codecs = ((uint16_t)report_data_ptr[1] << 8) + report_data_ptr[2];
                            APP_PRINT_INFO1("[voice_handle_atvv_srv_cb] ATV_TX_OPCODE_MIC_OPEN parsed_codecs = %d",
                                            parsed_codecs);

                            if ((parsed_codecs & atvv_global_data.codec_support) && app_global_data.is_atv_voice_notify_en)
                            {
                                atvv_global_data.codec_used = parsed_codecs;
                                if (parsed_codecs == ATTV_CODEC_MASK_8K_ADPCM)
                                {
                                    voice_driver_codec_params.codec_sample_rate = SAMPLE_RATE_8KHz;
                                }
                                else if (parsed_codecs == ATTV_CODEC_MASK_16K_ADPCM)
                                {
                                    voice_driver_codec_params.codec_sample_rate = SAMPLE_RATE_16KHz;
                                }
                                voice_handle_atvv_audio_start(ATV_START_REASON_MIC_OPEN_REQUEST);
                            }
                            else
                            {
                                voice_handle_atvv_mic_open_error(ATV_MIC_OPEN_ERROR_RESERVED);
                            }
                        }
                    }
                    break;

                case ATV_TX_OPCODE_MIC_CLOSE:
                    {
                        APP_PRINT_INFO0("[voice_handle_atvv_srv_cb] ATV_TX_OPCODE_MIC_CLOSE");
                        APP_PRINT_INFO2("[voice_handle_atvv_srv_cb] stream_id-- master: 0x%x, rcu: 0x%x",
                                        report_data_ptr[1],
                                        atvv_global_data.stream_id);
                        if (ATVV_VERSION_1_0 == atvv_global_data.app_support_version)
                        {
                            if (voice_driver_global_data.is_voice_driver_working == true)
                            {
                                if ((report_data_ptr[1] == 0xff)
                                    || ((report_data_ptr[1] == 0)
                                        && (atvv_global_data.atv_start_reason == ATV_START_REASON_MIC_OPEN_REQUEST))
                                    || ((report_data_ptr[1] == atvv_global_data.stream_id)
                                        && (atvv_global_data.atv_start_reason != ATV_START_REASON_MIC_OPEN_REQUEST)))
                                {
                                    voice_handle_atvv_audio_stop(ATV_STOP_REASON_MIC_CLOSE_MESSAGE);
                                }
                            }
                        }
                        else if (ATVV_VERSION_0_4 == atvv_global_data.app_support_version)
                        {
                            voice_handle_atvv_audio_stop(ATV_STOP_REASON_MIC_CLOSE_MESSAGE);
                        }
                    }
                    break;

                case ATV_TX_OPCODE_EXTEND:
                    {
                        APP_PRINT_INFO0("[voice_handle_atvv_srv_cb] ATV_TX_OPCODE_EXTEND");
                        APP_PRINT_INFO2("[voice_handle_atvv_srv_cb] stream_id-- master: 0x%x, rcu: 0x%x",
                                        report_data_ptr[1],
                                        atvv_global_data.stream_id);
                        if (ATVV_VERSION_1_0 == atvv_global_data.app_support_version)
                        {
                            if (voice_driver_global_data.is_voice_driver_working == true)
                            {
                                if ((report_data_ptr[1] == 0xff)
                                    || ((report_data_ptr[1] == 0x00) &&
                                        (atvv_global_data.atv_start_reason == ATV_START_REASON_MIC_OPEN_REQUEST))
                                    || ((report_data_ptr[1] == atvv_global_data.stream_id) &&
                                        (atvv_global_data.atv_start_reason != ATV_START_REASON_MIC_OPEN_REQUEST)))
                                {
                                    APP_PRINT_INFO0("[voice_handle_atvv_audio_stop] ATV_TX_OPCODE_EXTEND");
                                    os_timer_restart(&voice_exception_timer, VOICE_TRANSFER_TIMEOUT);
                                }
                            }
                        }
                    }
                    break;

                default:
                    break;
                }
            }
        }
        break;

    default:
        break;
    }

    return cb_result;
}

#endif

/******************* (C) COPYRIGHT 2020 Realtek Semiconductor Corporation *****END OF FILE****/

