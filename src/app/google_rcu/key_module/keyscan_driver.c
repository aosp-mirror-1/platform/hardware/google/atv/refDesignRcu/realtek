/**
*********************************************************************************************************
*               Copyright(c) 2020, Realtek Semiconductor Corporation. All rights reserved.
**********************************************************************************************************
* @file     keyscan_driver.c
* @brief    keyscan module driver
* @details
* @author   barry_bian
* @date     2020-02-25
* @version  v1.0
*********************************************************************************************************
*/

/*============================================================================*
 *                              Header Files
 *============================================================================*/
#include <board.h>
#include <string.h>
#include <trace.h>
#include <os_msg.h>
#include <app_msg.h>
#include <os_timer.h>
#include <keyscan_driver.h>
#include <rtl876x_rcc.h>
#include <rtl876x_keyscan.h>
#include <swtimer.h>
#include <rtl876x_pinmux.h>
#include <rtl876x_nvic.h>
#include <app_task.h>
#include <app_section.h>
#include "key_handle.h"

/*============================================================================*
 *                              Local Variables
 *============================================================================*/
static TimerHandle_t keyscan_timer;
static uint8_t KeyScanRowPINs[KEYPAD_MAX_COLUMN_SIZE] = {ROW0, ROW1, ROW2, ROW3, ROW4, ROW5};
static uint8_t KeyScanColumnPINs[KEYPAD_MAX_COLUMN_SIZE] = {COLUMN0, COLUMN1, COLUMN2, COLUMN3, COLUMN4, COLUMN5, COLUMN6};

/*============================================================================*
 *                              Global Variables
 *============================================================================*/
T_KEYSCAN_GLOBAL_DATA keyscan_global_data;

/*============================================================================*
 *                              Functions Declaration
 *============================================================================*/
#define keyscan_interrupt_handler Keyscan_Handler

static void keyscan_timer_callback(TimerHandle_t pxTimer) DATA_RAM_FUNCTION;
static void keyscan_row_pad_config(PAD_Mode AON_PAD_Mode,
                                   PAD_PWR_Mode AON_PAD_PwrOn,
                                   PAD_Pull_Mode AON_PAD_Pull,
                                   PAD_OUTPUT_ENABLE_Mode AON_PAD_E,
                                   PAD_OUTPUT_VAL AON_PAD_O) DATA_RAM_FUNCTION;
static void keyscan_column_pad_config(PAD_Mode AON_PAD_Mode,
                                      PAD_PWR_Mode AON_PAD_PwrOn,
                                      PAD_Pull_Mode AON_PAD_Pull,
                                      PAD_OUTPUT_ENABLE_Mode AON_PAD_E,
                                      PAD_OUTPUT_VAL AON_PAD_O) DATA_RAM_FUNCTION;

void keyscan_interrupt_handler(void) DATA_RAM_FUNCTION;
bool keyscan_check_dlps(void) DATA_RAM_FUNCTION;
void keyscan_enter_dlps_config(void) DATA_RAM_FUNCTION;
void keyscan_exit_dlps_config(void) DATA_RAM_FUNCTION;
void keyscan_init_data(void) DATA_RAM_FUNCTION;
void keyscan_init_pad_config(void) DATA_RAM_FUNCTION;
void keyscan_init_driver(uint32_t manual_sel, uint32_t is_debounce) DATA_RAM_FUNCTION;

/*============================================================================*
 *                              Local Functions
 *============================================================================*/
/******************************************************************
 * @brief  keyscan row pad config
 * @param  AON_PAD_Mode
 * @param  AON_PAD_PwrOn
 * @param  AON_PAD_Pull
 * @param  AON_PAD_E
 * @param  AON_PAD_O
 * @return none
 * @retval void
 */
static void keyscan_row_pad_config(PAD_Mode AON_PAD_Mode,
                                   PAD_PWR_Mode AON_PAD_PwrOn,
                                   PAD_Pull_Mode AON_PAD_Pull,
                                   PAD_OUTPUT_ENABLE_Mode AON_PAD_E,
                                   PAD_OUTPUT_VAL AON_PAD_O)
{
    for (uint8_t index = 0; index < key_handle_global_data.keyscan_row_size; index++)
    {
        Pad_Config(KeyScanRowPINs[index], AON_PAD_Mode, AON_PAD_PwrOn, AON_PAD_Pull, AON_PAD_E, AON_PAD_O);
    }
}

/******************************************************************
 * @brief  keyscan column pad config
 * @param  AON_PAD_Mode
 * @param  AON_PAD_PwrOn
 * @param  AON_PAD_Pull
 * @param  AON_PAD_E
 * @param  AON_PAD_O
 * @return none
 * @retval void
 */
static void keyscan_column_pad_config(PAD_Mode AON_PAD_Mode,
                                      PAD_PWR_Mode AON_PAD_PwrOn,
                                      PAD_Pull_Mode AON_PAD_Pull,
                                      PAD_OUTPUT_ENABLE_Mode AON_PAD_E,
                                      PAD_OUTPUT_VAL AON_PAD_O)
{
    for (uint8_t index = 0; index < key_handle_global_data.keyscan_column_size; index++)
    {
        Pad_Config(KeyScanColumnPINs[index], AON_PAD_Mode, AON_PAD_PwrOn, AON_PAD_Pull, AON_PAD_E,
                   AON_PAD_O);
    }
}

/******************************************************************
 * @brief  keyscan enable wakeup config function
 * @param  none
 * @return none
 * @retval void
 */
static void keyscan_enable_wakeup_config(void)
{
    /* @note: no key is pressed, use PAD wake up function with debounce,
    but pad debunce time should be smaller than ble connect interval */
    System_WakeUpDebounceTime(0x08);

    for (uint8_t index = 0; index < key_handle_global_data.keyscan_row_size; index++)
    {
        System_WakeUpPinEnable(KeyScanRowPINs[index], PAD_WAKEUP_POL_LOW, PAD_WK_DEBOUNCE_ENABLE);
    }
}

/******************************************************************
 * @brief  keyscan disable wakeup config function
 * @param  none
 * @return none
 * @retval void
 */
static void keyscan_disable_wakeup_config(void)
{
    for (uint8_t index = 0; index < key_handle_global_data.keyscan_row_size; index++)
    {
        System_WakeUpPinDisable(KeyScanRowPINs[index]);
    }
}

/*============================================================================*
 *                              Global Functions
 *============================================================================*/
/******************************************************************
 * @fn       keyscan_init_data
 * @brief    initialize keyscan driver data
 * @param    none
 * @return   none
 * @retval   void
 */
void keyscan_init_data(void)
{
    KEYSCAN_DBG_BUFFER(MODULE_APP, LEVEL_INFO, "[keyscan_init_data] init data", 0);
    memset(&keyscan_global_data, 0, sizeof(keyscan_global_data));
    keyscan_global_data.is_allowed_to_enter_dlps = true;
    keyscan_global_data.is_all_key_released = true;
}

/******************************************************************
 * @fn       keyscan_init_driver
 * @brief    keyscan module initial
 * @param    uint32_t manual_mode - KeyScan_Manual_Sel_Key or KeyScan_Manual_Sel_Bit
 * @param    uint32_t is_debounce
 * @return   none
 * @retval   void
 * @note     when system in dlsp mode, keyscan debunce time should be smaller
 *           than wake up interval time (like ble interval), which can be modified
 *           through KeyScan_InitStruct.debouncecnt.
 */
void keyscan_init_driver(uint32_t manual_sel, uint32_t is_debounce)
{
    if (false == keyscan_global_data.is_pinmux_setted)
    {
        keyscan_init_pad_config();
    }

    /* turn on keyscan clock */
    RCC_PeriphClockCmd(APBPeriph_KEYSCAN, APBPeriph_KEYSCAN_CLOCK, ENABLE);

    KEYSCAN_InitTypeDef  KeyScan_InitStruct;
    KeyScan_StructInit(&KeyScan_InitStruct);
    KeyScan_InitStruct.colSize         = key_handle_global_data.keyscan_column_size;
    KeyScan_InitStruct.rowSize         = key_handle_global_data.keyscan_row_size;
    KeyScan_InitStruct.scanmode        = KeyScan_Manual_Scan_Mode;
    if (KeyScan_Manual_Scan_Mode == KeyScan_InitStruct.scanmode)
    {
        KeyScan_InitStruct.manual_sel = manual_sel;
        if (manual_sel == KeyScan_Manual_Sel_Bit)
        {
            KEYSCAN_DBG_BUFFER(MODULE_APP, LEVEL_INFO, "[keyscan_init_driver] KeyScan_Manual_Sel_Bit", 0);
        }
        else
        {
            KEYSCAN_DBG_BUFFER(MODULE_APP, LEVEL_INFO, "[keyscan_init_driver] KeyScan_Manual_Sel_Key", 0);
        }
    }

    KeyScan_InitStruct.clockdiv         = 0x26;  /* 128kHz = 5MHz/(clockdiv+1) */
    KeyScan_InitStruct.delayclk         = 0x0f;  /* 8kHz = 5MHz/(clockdiv+1)/(delayclk+1) */

    KeyScan_InitStruct.debounceEn       = is_debounce;
    KeyScan_InitStruct.scantimerEn  = KeyScan_ScanInterval_Disable;
    KeyScan_InitStruct.detecttimerEn    = KeyScan_Release_Detect_Disable;

    KeyScan_InitStruct.scanInterval     = 0x190;  /* 50ms = scanInterval/8kHz */
    KeyScan_InitStruct.debouncecnt      = 0x30;   /* 6ms = debouncecnt/8kHz */
    KeyScan_InitStruct.releasecnt       = 0x50;   /* 10ms = releasecnt/8kHz */
    KeyScan_InitStruct.keylimit         = KEYSCAN_FIFO_LIMIT;

    KeyScan_Init(KEYSCAN, &KeyScan_InitStruct);
    KeyScan_INTConfig(KEYSCAN, KEYSCAN_INT_SCAN_END, ENABLE);
    KeyScan_ClearINTPendingBit(KEYSCAN, KEYSCAN_INT_SCAN_END);
    KeyScan_INTMask(KEYSCAN, KEYSCAN_INT_SCAN_END, DISABLE);  /* Mask keyscan interrupt */
    KeyScan_Cmd(KEYSCAN, ENABLE);
}

/******************************************************************
 * @brief    keyscan nvic config
 * @param    none
 * @return   none
 * @retval   void
 */
void keyscan_nvic_config(void)
{
    NVIC_InitTypeDef NVIC_InitStruct;

    NVIC_InitStruct.NVIC_IRQChannel = KeyScan_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelPriority = 3;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;

    NVIC_Init(&NVIC_InitStruct);
}

/******************************************************************
 * @brief    keyscan pinmux config
 * @param    none
 * @return   none
 * @retval   void
 */
void keyscan_pinmux_config(void)
{
    for (uint8_t index = 0; index < key_handle_global_data.keyscan_row_size; index++)
    {
        Pinmux_Config(KeyScanRowPINs[index], KEY_ROW_0 + index);
    }

    for (uint8_t index = 0; index < key_handle_global_data.keyscan_column_size; index++)
    {
        Pinmux_Config(KeyScanColumnPINs[index], KEY_COL_0 + index);
    }
}

/******************************************************************
 * @brief    keyscan init pad config
 * @param    none
 * @return   none
 * @retval   void
 */
void keyscan_init_pad_config(void)
{
    keyscan_row_pad_config(PAD_PINMUX_MODE, PAD_IS_PWRON, PAD_PULL_UP, PAD_OUT_DISABLE, PAD_OUT_LOW);
    keyscan_column_pad_config(PAD_PINMUX_MODE, PAD_IS_PWRON, PAD_PULL_NONE, PAD_OUT_ENABLE,
                              PAD_OUT_LOW);
    keyscan_global_data.is_pinmux_setted = true;
}

/******************************************************************
 * @brief    keyscan enter DLPS config
 * @param    none
 * @return   none
 * @retval   void
 */
void keyscan_enter_dlps_config(void)
{
    KEYSCAN_DBG_BUFFER(MODULE_APP, LEVEL_INFO, "[keyscan_enter_dlps_config] enter DLPS pad config", 0);
    keyscan_column_pad_config(PAD_SW_MODE, PAD_IS_PWRON, PAD_PULL_NONE, PAD_OUT_ENABLE, PAD_OUT_LOW);

    if (keyscan_global_data.is_all_key_released == true)
    {
        keyscan_enable_wakeup_config();
        keyscan_row_pad_config(PAD_SW_MODE, PAD_IS_PWRON, PAD_PULL_UP, PAD_OUT_DISABLE, PAD_OUT_LOW);
    }
    else
    {
        /* any key is pressed, disable key row pins, just wait keyscan sw timer to wake */
        keyscan_disable_wakeup_config();
        keyscan_row_pad_config(PAD_SW_MODE, PAD_IS_PWRON, PAD_PULL_DOWN, PAD_OUT_DISABLE, PAD_OUT_LOW);
    }

    keyscan_global_data.is_pinmux_setted = false;
}

/******************************************************************
 * @brief    keyscan exit DLPS config
 * @param    none
 * @return   none
 * @retval   void
 */
void keyscan_exit_dlps_config(void)
{
    if (true == keyscan_global_data.is_all_key_released)
    {
        KEYSCAN_DBG_BUFFER(MODULE_APP, LEVEL_INFO,
                           "[keyscan_exit_dlps_config] start keyscan if all key released", 0);
        keyscan_init_pad_config();
        /*key trigger manual mode init*/
        keyscan_init_driver(KeyScan_Manual_Sel_Key, KeyScan_Debounce_Enable);
    }

    keyscan_disable_wakeup_config();
}

/******************************************************************
 * @brief    keyscan check DLPS callback
 * @param    none
 * @return   bool
 * @retval   true or false
 */
bool keyscan_check_dlps(void)
{
    return keyscan_global_data.is_allowed_to_enter_dlps;
}

/******************************************************************
 * @brief    keyscan interrupt handler
 * @param    none
 * @return   none
 * @retval   void
 */
void keyscan_interrupt_handler(void)
{
    KEYSCAN_DBG_BUFFER(MODULE_APP, LEVEL_INFO, "[keyscan_interrupt_handler] interrupt handler", 0);

    T_IO_MSG bee_io_msg;
    bee_io_msg.type = IO_MSG_TYPE_KEYSCAN;

    if (KeyScan_GetFlagState(KEYSCAN, KEYSCAN_INT_FLAG_SCAN_END) == SET)
    {
        KEYSCAN_DBG_BUFFER(MODULE_APP, LEVEL_INFO,
                           "[keyscan_interrupt_handler] KEYSCAN_INT_FLAG_SCAN_END interrupt", 0);
        keyscan_global_data.is_allowed_to_enter_dlps = true;

        memset(&keyscan_global_data.cur_fifo_data, 0, sizeof(T_KEYSCAN_FIFO_DATA));

        /* read keyscan FIFO data length */
        keyscan_global_data.cur_fifo_data.len = KeyScan_GetFifoDataNum(KEYSCAN);
        if (keyscan_global_data.cur_fifo_data.len > KEYSCAN_FIFO_LIMIT)
        {
            /* set flag to default status and reinit keyscan module to key trigger manual mode with debounce enabled */
            keyscan_init_data();
            keyscan_init_driver(KeyScan_Manual_Sel_Key, KeyScan_Debounce_Enable);
            return;
        }
        else if (keyscan_global_data.cur_fifo_data.len != 0)
        {
            keyscan_global_data.is_all_key_released = false;

            /* read keyscan fifo data */
            KeyScan_Read(KEYSCAN, (uint16_t *) & (keyscan_global_data.cur_fifo_data.key[0]),
                         keyscan_global_data.cur_fifo_data.len);

            KeyScan_INTConfig(KEYSCAN, KEYSCAN_INT_SCAN_END, DISABLE);
            KeyScan_Cmd(KEYSCAN, DISABLE);

            /* start sw timer to check press status */
            if (!os_timer_restart(&keyscan_timer, KEYSCAN_SW_INTERVAL))
            {
                APP_PRINT_ERROR0("[keyscan_interrupt_handler] restart keyscan_timer failed!");
                /* set flag to default status and reinit keyscan module to key trigger manual mode with debounce enabled */
                keyscan_init_data();
                keyscan_init_driver(KeyScan_Manual_Sel_Key, KeyScan_Debounce_Enable);
                return;
            }

            if (false == keyscan_global_data.is_allowed_to_repeat_report)
            {
                if (!memcmp(&keyscan_global_data.cur_fifo_data, &keyscan_global_data.pre_fifo_data,
                            sizeof(T_KEYSCAN_FIFO_DATA)))
                {
                    /* same keyscan FIFO data, just return */
                    return;
                }
                else
                {
                    /* updata previous keyscan FIFO data */
                    memcpy(&keyscan_global_data.pre_fifo_data, &keyscan_global_data.cur_fifo_data,
                           sizeof(T_KEYSCAN_FIFO_DATA));
                }
            }

            bee_io_msg.subtype = IO_MSG_KEYSCAN_RX_PKT;
            bee_io_msg.u.buf   = (void *)(&keyscan_global_data.pre_fifo_data);
            if (false == app_send_msg_to_apptask(&bee_io_msg))
            {
                APP_PRINT_ERROR0("[keyscan_interrupt_handler] send IO_MSG_KEYSCAN_RX_PKT message failed!");
                os_timer_stop(&keyscan_timer);
                /* set flag to default status and reinit keyscan module to key trigger manual mode with debounce enabled */
                keyscan_init_data();
                keyscan_init_driver(KeyScan_Manual_Sel_Key, KeyScan_Debounce_Enable);
                return;
            }
        }
        else
        {
            KEYSCAN_DBG_BUFFER(MODULE_APP, LEVEL_INFO, "[keyscan_interrupt_handler] fifo is tempty", 0);

            if (keyscan_global_data.is_all_key_released == false)
            {
                KEYSCAN_DBG_BUFFER(MODULE_APP, LEVEL_INFO, "[keyscan_interrupt_handler] key release", 0);
                APP_PRINT_INFO0("[keyscan_interrupt_handler] key release");

                keyscan_global_data.is_all_key_released = true;
                bee_io_msg.subtype = IO_MSG_KEYSCAN_ALLKEYRELEASE;
                if (false == app_send_msg_to_apptask(&bee_io_msg))
                {
                    APP_PRINT_ERROR0("[keyscan_interrupt_handler] Send IO_MSG_KEYSCAN_ALLKEYRELEASE message failed!");
                    /* set flag to default status and reinit keyscan module to key trigger manual mode with debounce enabled */
                    keyscan_init_data();
                    keyscan_init_driver(KeyScan_Manual_Sel_Key, KeyScan_Debounce_Enable);
                    return;
                }
            }

            os_timer_stop(&keyscan_timer);
            /* reinit keyscan module to key trigger manual mode with debounce enabled */
            memset(&keyscan_global_data.pre_fifo_data, 0, sizeof(T_KEYSCAN_FIFO_DATA));
            keyscan_init_driver(KeyScan_Manual_Sel_Key, KeyScan_Debounce_Enable);
            return;
        }
    }
    else
    {
        KEYSCAN_DBG_BUFFER(MODULE_APP, LEVEL_WARN,
                           "[keyscan_interrupt_handler] not KEYSCAN_INT_FLAG_SCAN_END or KEYSCAN_INT_FLAG_SCAN_END interrupt",
                           0);
        /* set flag to default status and reinit keyscan module to key trigger manual mode with debounce enabled */
        keyscan_init_data();
        keyscan_init_driver(KeyScan_Manual_Sel_Key, KeyScan_Debounce_Enable);
        return;
    }
}

/******************************************************************
 * @brief    keyscan init timer
 * @param    none
 * @return   none
 * @retval   void
 */
void keyscan_init_timer(void)
{
    KEYSCAN_DBG_BUFFER(MODULE_APP, LEVEL_INFO, "[keyscan_init_timer] init timer", 0);
    /*keyscan_timer is used for keyscan dlps*/
    if (false == os_timer_create(&keyscan_timer, "keyscan_timer",  1, \
                                 KEYSCAN_SW_INTERVAL, false, keyscan_timer_callback))
    {
        APP_PRINT_ERROR0("[keyscan_init_timer] timer creat failed!");
    }
}

/******************************************************************
 * @brief    keyscan timer callback
 * @param    none
 * @return   none
 * @retval   void
 */
void keyscan_timer_callback(TimerHandle_t pxTimer)
{
    KEYSCAN_DBG_BUFFER(MODULE_APP, LEVEL_INFO, "[keyscan_timer_callback] start release timer", 0);
    keyscan_global_data.is_allowed_to_enter_dlps = false;
    /* start register trigger manual mode */
    keyscan_init_driver(KeyScan_Manual_Sel_Bit, KeyScan_Debounce_Disable);
}

/******************* (C) COPYRIGHT 2020 Realtek Semiconductor Corporation *****END OF FILE****/
