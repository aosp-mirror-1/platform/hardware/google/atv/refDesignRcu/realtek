/**
*********************************************************************************************************
*               Copyright(c) 2017, Realtek Semiconductor Corporation. All rights reserved.
*********************************************************************************************************
* @file      swtimer.h
* @brief     header file of software timer implementation
* @details
* @author    KEN_MEI
* @date      2017-02-08
* @version   v0.1
* *********************************************************************************************************
*/

#ifndef _SWTIMER__
#define _SWTIMER__

#ifdef __cplusplus
extern "C" {
#endif

/*============================================================================*
 *                        Header Files
 *============================================================================*/
#include <board.h>

/*============================================================================*
 *                              Macros
 *============================================================================*/
#define ADV_RECONNECT_TIMEOUT 120000  /* 120s */
#define ADV_UNDIRECT_POWER_TIMEOUT 6000  /* 6s */
#define ADV_UNDIRECT_PAIRING_TIMEOUT 600000  /* 600s */
#define ADV_UNDIRECT_REPAIRING_TIMEOUT 120000  /* 120s */
#define ADV_CHANGE_DATA_TIMEOUT 3000 /* 3s */
#define PAIRING_EXCEPTION_TIMEOUT 10000  /* 10s */
#define NO_ACTION_DISCON_TIMEOUT 300000  /* 300s */
#define UPDATE_CONN_PARAMS_TIMEOUT 12 * 1000  /* 12s */
#define UPDATE_CONN_PARAMS_SHORT_TIMEOUT 5 * 1000  /* 5s */
#define DFU_PROCESS_WAIT_TIMEOUT   5000  /* 5s */
#define PAIR_FAIL_DISCONN_TIMEOUT  2000  /* 2s */
#define SYSTEM_RESET_TIMEOUT  500 /* 500ms */
#define TEMP_OFF_LANTENCY_TIMEOUT  500  /* 500ms */
#define APP_RESOLVED_TIMEOUT  360  /* 360s */
#define ADV_UPDATE_INTERVAL_TIMEOUT 30000  /* 30s */
#define CHECK_BACK_KEY_PRESSED_WHEN_HW_RESET_TIMEOUT  50  /* 50ms*/
/*============================================================================*
 *                         Types
 *============================================================================*/
typedef void *TimerHandle_t;

/*============================================================================*
*                        Export Global Variables
*============================================================================*/
extern TimerHandle_t adv_timer;
extern TimerHandle_t adv_change_data_timer;
#if FEATURE_SUPPORT_NO_ACTION_DISCONN
extern TimerHandle_t no_act_disconn_timer;
#endif
extern TimerHandle_t update_conn_params_timer;
extern TimerHandle_t next_state_check_timer;
extern TimerHandle_t system_rest_timer;
extern TimerHandle_t temp_off_latency_timer;
extern TimerHandle_t back_key_pressed_when_HW_reset_timer;
/*============================================================================*
 *                         Functions
 *============================================================================*/
void sw_timer_init(void);

#ifdef __cplusplus
}
#endif

#endif /*_SWTIMER__*/

/******************* (C) COPYRIGHT 2017 Realtek Semiconductor Corporation *****END OF FILE****/
