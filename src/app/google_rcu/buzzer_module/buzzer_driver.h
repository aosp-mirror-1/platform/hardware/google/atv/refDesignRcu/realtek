
/*
 *  Routines to access hardware
 *
 *  Copyright (c) 2018 Realtek Semiconductor Corp.
 *
 *  This module is a confidential and proprietary property of RealTek and
 *  possession or use of this module requires written permission of RealTek.
 */
#ifndef _BUZZER_DRIVER_H_
#define _BUZZER_DRIVER_H_

#ifdef __cplusplus
extern "C" {
#endif

/*============================================================================*
 *                        Header Files
 *============================================================================*/
#include <stdint.h>
#include <stdbool.h>
#include <swtimer.h>
#include <rtl876x.h>
#include <board.h>

#if SUPPORT_BUZZER_FEATURE

/*============================================================================*
 *                         Macros
 *============================================================================*/
#define PWM_TIMER_NUM           TIM4
#define PWM_OUT_PIN_PINMUX      TIM_PWM4

/* Config PWM_PERIOD and PWM_DUTY_CYCLE */
#define PWM_FREQUENCY           1000 //uint:hz
#define PWM_PERIOD              1000 //uint:us
#define PWM_DUTY_CYCLE          50      //uint:percent
#define PWM_HIGH_COUNT          (((PWM_PERIOD)*((PWM_DUTY_CYCLE*40)/100))-1)    //PWM CLOCK = 40000000
#define PWM_LOW_COUNT           (((PWM_PERIOD)*(((100-PWM_DUTY_CYCLE)*40)/100))-1)

#define BUZZER_PWM_STOP_TIMEOUT 1 /* 1ms */

/*============================================================================*
 *                         Types
 *============================================================================*/
/**
 * @brief  Buzzer global data struct definition.
 */
typedef struct
{
    bool is_allowed_to_enter_dlps;  /* to indicate whether to allow to enter dlps or not */
    bool is_working;                /* to indicate whether buzzer driver is working or not */
    uint32_t current_peirod_in_us;  /* to indicate the current PWM period in us */
    uint16_t freq;                  /* to indicate the frequency */
    uint8_t  duty_cycle;            /* to indicate the duty_cycle */
} T_BUZZER_LOCAL_DATA;

/*============================================================================*
*                        Export Global Variables
*============================================================================*/


/*============================================================================*
 *                         Functions
 *============================================================================*/
void buzzer_init_data(void);
void buzzer_pinmux_config(void);
void buzzer_deinit_pad_config(void);
void buzzer_init_pad_config(void);
bool buzzer_check_dlps_allowed(void);
void buzzer_start_pwm_output(uint16_t frequency, uint8_t duty_cycle);
void buzzer_stop_pwm_output(void);
void buzzer_set_frequency(uint16_t frequency);
void buzzer_set_duty_cycle(uint8_t duty_cycle);
uint16_t buzzer_get_frequency(void);
uint8_t buzzer_get_duty_cycle(void);
bool buzzer_is_working_check(void);

#endif

#ifdef __cplusplus
}
#endif

#endif

