/**
*********************************************************************************************************
*               Copyright(c) 2015, Realtek Semiconductor Corporation. All rights reserved.
**********************************************************************************************************
* @file     buzzer_driver.c
* @brief    This is the entry of user code which the buzzer module resides in.
* @details
* @author   chenjie
* @date     2021-10-14
* @version  v0.1
*********************************************************************************************************
*/

/*============================================================================*
 *                        Header Files
 *============================================================================*/
#include <board.h>
#include <string.h>
#include <trace.h>
#include <os_msg.h>
#include <app_msg.h>
#include <os_timer.h>
#include <buzzer_driver.h>
#include <rtl876x_rcc.h>
#include <swtimer.h>
#include <rtl876x_pinmux.h>
#include <rtl876x_nvic.h>
#include <app_task.h>
#include "rtl876x_tim.h"
#include <rcu_application.h>

#if SUPPORT_BUZZER_FEATURE

/*============================================================================*
 *                         Macros
 *============================================================================*/

/*============================================================================*
 *                              Local Variables
 *============================================================================*/

/*============================================================================*
*                              Global Variables
*============================================================================*/
T_BUZZER_LOCAL_DATA buzzer_local_data;

/*============================================================================*
 *                              Functions Declaration
 *============================================================================*/
static bool buzzer_get_pwm_count(uint32_t period_in_us, uint32_t duty_cycle,
                                 uint32_t *p_pwm_high_cnt, uint32_t *p_pwm_low_cnt);

/*============================================================================*
*                              External Functions
*============================================================================*/


/*============================================================================*
*                              Local Functions
*============================================================================*/
/******************************************************************
 * @fn       buzzer_init_driver
 * @brief    buzzer module initial
 */
static void buzzer_init_driver(uint16_t frequency, uint8_t duty_cycle)
{
    RCC_PeriphClockCmd(APBPeriph_TIMER, APBPeriph_TIMER_CLOCK, ENABLE);

    TIM_TimeBaseInitTypeDef TIM_InitStruct;
    uint32_t period;
    uint32_t cur_pwm_high_cnt = 0;
    uint32_t cur_pwm_low_cnt = 0;
    period = (uint32_t)(1000000 / frequency);

    if (false == buzzer_get_pwm_count(period, (uint32_t)duty_cycle,
                                      &cur_pwm_high_cnt, &cur_pwm_low_cnt))
    {
        cur_pwm_high_cnt = PWM_HIGH_COUNT;
        cur_pwm_low_cnt = PWM_LOW_COUNT;
    }

    TIM_StructInit(&TIM_InitStruct);
    TIM_InitStruct.TIM_PWM_En   = PWM_ENABLE;
    TIM_InitStruct.TIM_Mode     = TIM_Mode_UserDefine;
    TIM_InitStruct.TIM_PWM_High_Count   = cur_pwm_high_cnt;
    TIM_InitStruct.TIM_PWM_Low_Count    = cur_pwm_low_cnt;
    TIM_InitStruct.PWM_Deazone_Size     = 0;
    TIM_InitStruct.PWMDeadZone_En       = DEADZONE_DISABLE;  //enable to use pwn p/n output
    TIM_TimeBaseInit(PWM_TIMER_NUM, &TIM_InitStruct);

    TIM_Cmd(PWM_TIMER_NUM, ENABLE);
}

/******************************************************************
 * @fn       buzzer_deinit_driver
 * @brief    buzzer module de-initial
 */
static void buzzer_deinit_driver(void)
{
    TIM_Cmd(PWM_TIMER_NUM, DISABLE);
}

/*============================================================================*
*                              Global Functions
*============================================================================*/
/**
* @fn  buzzer_init_data
* @brief  Initialize buzzer driver data
*/
void buzzer_init_data(void)
{
    APP_PRINT_INFO0("[buzzer_init_data] init data");
    memset(&buzzer_local_data, 0, sizeof(buzzer_local_data));
    buzzer_local_data.is_allowed_to_enter_dlps = true;
    buzzer_local_data.current_peirod_in_us = PWM_PERIOD;
    buzzer_local_data.duty_cycle = PWM_DUTY_CYCLE;
    buzzer_local_data.freq = PWM_FREQUENCY;
}

/******************************************************************
 * @brief    buzzer pinmux config
 */
void buzzer_pinmux_config(void)
{
    Pinmux_Config(BUZZER_PWM_OUTPUT_PIN, PWM_OUT_PIN_PINMUX);
}

/******************************************************************
 * @brief    buzzer init pad config
 */
void buzzer_init_pad_config(void)
{
    Pad_Config(BUZZER_PWM_OUTPUT_PIN, PAD_PINMUX_MODE, PAD_IS_PWRON, PAD_PULL_NONE, PAD_OUT_DISABLE,
               PAD_OUT_LOW);
}

/******************************************************************
 * @brief    buzzer deinit pad config
 */
void buzzer_deinit_pad_config(void)
{
    Pad_Config(BUZZER_PWM_OUTPUT_PIN, PAD_SW_MODE, PAD_IS_PWRON, PAD_PULL_DOWN, PAD_OUT_DISABLE,
               PAD_OUT_LOW);
}

/******************************************************************
 * @brief    buzzer check DLPS config
 */
bool buzzer_check_dlps_allowed(void)
{
    return buzzer_local_data.is_allowed_to_enter_dlps;
}

/******************************************************************
 * @brief    buzzer start pwm output
 */
void buzzer_start_pwm_output(uint16_t frequency, uint8_t duty_cycle)
{
    APP_PRINT_INFO2("[buzzer_start_pwm_output] start PWM output!, frequency = %d duty_cycle = %d",
                    frequency, duty_cycle);

    if (buzzer_local_data.is_working == false)
    {
        buzzer_init_data();
        buzzer_init_pad_config();
        buzzer_pinmux_config();
        buzzer_init_driver(frequency, duty_cycle);
    }

    buzzer_local_data.is_allowed_to_enter_dlps = false;
    buzzer_local_data.is_working = true;
}

/******************************************************************
 * @brief    buzzer stop pwm output
 */
void buzzer_stop_pwm_output(void)
{
    APP_PRINT_INFO0("[buzzer_stop_pwm_output] stop PWM output!");
    if (buzzer_local_data.is_working == true)
    {
        buzzer_deinit_driver();
        buzzer_deinit_pad_config();
        buzzer_local_data.is_working = false;
        buzzer_local_data.is_allowed_to_enter_dlps = true;
    }
}

/******************************************************************
 * @brief    buzzer get pwm count
 */
bool buzzer_get_pwm_count(uint32_t period_in_us, uint32_t duty_cycle, uint32_t *p_pwm_high_cnt,
                          uint32_t *p_pwm_low_cnt)
{
    if ((period_in_us == 0)
        || (duty_cycle > 100))
    {
        return false;
    }

    *p_pwm_high_cnt = (((period_in_us) * (duty_cycle * 40) / 100) - 1);
    *p_pwm_low_cnt = (((period_in_us) * ((100 - duty_cycle) * 40) / 100) - 1);

    return true;
}

/******************************************************************
 * @brief    buzzer set frequency
 */
void buzzer_set_frequency(uint16_t frequency)
{
    buzzer_local_data.freq = frequency;
}

/******************************************************************
 * @brief    buzzer set duty cycle
 */
void buzzer_set_duty_cycle(uint8_t duty_cycle)
{
    buzzer_local_data.duty_cycle = duty_cycle;
}

/******************************************************************
 * @brief    buzzer get frequency
 */
uint16_t buzzer_get_frequency(void)
{
    return buzzer_local_data.freq;
}

/******************************************************************
 * @brief    buzzer get duty cycle
 */
uint8_t buzzer_get_duty_cycle(void)
{
    return buzzer_local_data.duty_cycle;
}

/******************************************************************
 * @brief    buzzer is working check
 */
bool buzzer_is_working_check(void)
{
    return buzzer_local_data.is_working;
}

#endif
