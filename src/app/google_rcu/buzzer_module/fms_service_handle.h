/*
 *  Routines to access hardware
 *
 *  Copyright (c) 2019 Realtek Semiconductor Corp.
 *
 *  This module is a confidential and proprietary property of RealTek and
 *  possession or use of this module requires written permission of RealTek.
 */

#ifndef _FMS_HANDLE_H_
#define _FMS_HANDLE_H_

#ifdef __cplusplus
extern "C" {
#endif

/*============================================================================*
 *                        Header Files
 *============================================================================*/
#include "fms.h"
#include "profile_server.h"
#include "swtimer.h"
#include "frm_define.h"

/*============================================================================*
 *                              Macro Definitions
 *============================================================================*/
#define DEFAULT_FMS_PERIODICAL_CONN_TIMEOUT     60  /* 60 minutes */
#define MIN_FMS_PERIODICAL_CONN_TIMEOUT         0x0005  /* uint: minute */
#define MAX_FMS_PERIODICAL_CONN_TIMEOUT         0x05A0  /* uint: minute */

/*============================================================================*
 *                         Types
 *============================================================================*/


/*============================================================================*
*                        Export Global Variables
*============================================================================*/


/*============================================================================*
 *                         Functions
 *============================================================================*/
void fms_handle_init_data(void);
T_APP_RESULT fms_handle_srv_cb(T_FMS_CALLBACK_DATA *p_data);
uint16_t fms_handle_get_current_mode(void);
void fms_handle_disconnect_event(void);
void fms_handle_connect_event(void);
bool fms_handle_notify_status(uint16_t status, uint16_t error_code);
void fms_handle_init_timer(void);

#ifdef __cplusplus
}
#endif

#endif
