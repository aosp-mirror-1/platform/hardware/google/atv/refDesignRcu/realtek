/**
*********************************************************************************************************
*               Copyright(c) 2017, Realtek Semiconductor Corporation. All rights reserved.
**********************************************************************************************************
* @file     fms.c
* @brief    Find Me service source file.
* @details  Interfaces to access Find Me service.
* @author   Chenjie jin
* @date     2022-05-31
* @version  v1.0
*********************************************************************************************************
*/

#include "board.h"
#include "string.h"
#include "trace.h"
#include "gatt.h"
#include "profile_server.h"
#include "fms.h"
#include "fms_service_handle.h"
#include "gap.h"

/********************************************************************************************************
* local static variables defined here, only used in this source file.
********************************************************************************************************/
static uint16_t Fms_Ctl_mode;
static P_FUN_SERVER_GENERAL_CB pfn_fms_cb = NULL;
static const uint8_t GATT_UUID_FMS_SERVICE[16] = {GATT_UUID128_FMS_SERVICE};

/********************************************************************************************************
* global variables
********************************************************************************************************/
/**< @brief  profile/service definition.
*   here is an example of fms service table
*   including Write
*/
const T_ATTRIB_APPL fms_svc_attr_tbl[] =
{
    /*--------------------------FMS Service ---------------------------*/
    /* <<Primary Service>>, .. 0*/
    {
        (ATTRIB_FLAG_VOID | ATTRIB_FLAG_LE),        /* wFlags */
        {                                           /* type_value */
            LO_WORD(GATT_UUID_PRIMARY_SERVICE),
            HI_WORD(GATT_UUID_PRIMARY_SERVICE),
        },
        UUID_128BIT_SIZE,                           /* bValueLen */
        (void *)GATT_UUID_FMS_SERVICE,               /* pValueContext */
        GATT_PERM_READ                              /* wPermissions */
    },

    /* <<Characteristic>> 1 */
    {
        ATTRIB_FLAG_VALUE_INCL,                     /* wFlags */
        {                                           /* type_value */
            LO_WORD(GATT_UUID_CHARACTERISTIC),
            HI_WORD(GATT_UUID_CHARACTERISTIC),
            GATT_CHAR_PROP_READ | GATT_CHAR_PROP_WRITE_NO_RSP,            /* characteristic properties */
            /* characteristic UUID not needed here, is UUID of next attrib. */
        },
        1,                                          /* bValueLen */
        NULL,
        GATT_PERM_READ                              /* wPermissions */
    },

    /* FMS Control 2 */
    {
        ATTRIB_FLAG_VALUE_APPL | ATTRIB_FLAG_UUID_128BIT, /* wFlags */
        {                                           /* type_value */
            GATT_UUID128_FMS_CONTROL
        },
        0,                                          /* variable size */
        NULL,
        (GATT_PERM_READ | GATT_PERM_WRITE)          /* wPermissions */
    },

    /* <<Characteristic>> 3 */
    {
        ATTRIB_FLAG_VALUE_INCL,                     /* wFlags */
        {                                           /* type_value */
            LO_WORD(GATT_UUID_CHARACTERISTIC),
            HI_WORD(GATT_UUID_CHARACTERISTIC),
            GATT_CHAR_PROP_WRITE_NO_RSP | GATT_CHAR_PROP_NOTIFY,    /* characteristic properties */
            /* characteristic UUID not needed here, is UUID of next attrib. */
        },
        1,                                          /* bValueLen */
        NULL,
        GATT_PERM_READ                              /* wPermissions */
    },

    /* FMS Data 4 */
    {
        ATTRIB_FLAG_VALUE_APPL | ATTRIB_FLAG_UUID_128BIT, /* wFlags */
        {                                           /* type_value */
            GATT_UUID128_FMS_DATA
        },
        0,                                          /* variable size */
        NULL,
        (GATT_PERM_READ | GATT_PERM_WRITE)          /* wPermissions */
    },

    /* client characteristic configuration  5 */
    {
        (ATTRIB_FLAG_VALUE_INCL |                   /* flags */
         ATTRIB_FLAG_CCCD_APPL),
        {                                           /* type_value */
            LO_WORD(GATT_UUID_CHAR_CLIENT_CONFIG),
            HI_WORD(GATT_UUID_CHAR_CLIENT_CONFIG),
            /* NOTE: this value has an instantiation for each client, a write to */
            /* this attribute does not modify this default value:                */
            LO_WORD(GATT_CLIENT_CHAR_CONFIG_DEFAULT), /* client char. config. bit field */
            HI_WORD(GATT_CLIENT_CHAR_CONFIG_DEFAULT)
        },
        2,                                          /* bValueLen */
        NULL,
        (GATT_PERM_READ | GATT_PERM_WRITE)          /* permissions */
    }
};

/**
 * @brief read characteristic data from service.
 *
 * @param conn_id           Connection ID.
 * @param service_id        ServiceID to be read.
 * @param attrib_index      Attribute index of getting characteristic data.
 * @param offset            Offset of characteritic to be read.
 * @param p_length          Length of getting characteristic data.
 * @param pp_value          Pointer to pointer of characteristic value to be read.
 * @return T_APP_RESULT
*/
T_APP_RESULT fms_attr_read_cb(uint8_t conn_id, T_SERVER_ID service_id, uint16_t attrib_index,
                              uint16_t offset, uint16_t *p_length, uint8_t **pp_value)
{
    T_APP_RESULT  cause  = APP_RESULT_SUCCESS;
    *p_length = 0;

    PROFILE_PRINT_INFO2("fms_attr_read_cb attrib_index = %d offset %x", attrib_index, offset);

    switch (attrib_index)
    {
    case GATT_SVC_FMS_CONTROL_INDEX:
        {
            /* update Fms_Ctl_mode */
            Fms_Ctl_mode = fms_handle_get_current_mode();

            *pp_value = (uint8_t *)&Fms_Ctl_mode;
            *p_length = 2;
        }
        break;

    default:
        cause = APP_RESULT_ATTR_NOT_FOUND;
        break;
    }

    return cause;
}

/**
 * @brief write characteristic data from service.
 *
 * @param ServiceID          ServiceID to be written.
 * @param iAttribIndex       Attribute index of characteristic.
 * @param wLength            length of value to be written.
 * @param pValue             value to be written.
 * @return Profile procedure result
*/
T_APP_RESULT  fms_attr_write_cb(uint8_t conn_id, uint8_t service_id, uint16_t attrib_index,
                                T_WRITE_TYPE write_type,
                                uint16_t length, uint8_t *p_value, P_FUN_WRITE_IND_POST_PROC *p_write_ind_post_proc)
{
    bool handle = true;
    T_FMS_CALLBACK_DATA callback_data;
    T_APP_RESULT  cause = APP_RESULT_SUCCESS;

    APP_PRINT_INFO1("[fms_attr_write_cb] attrib_index is %d", attrib_index);

    if (GATT_SVC_FMS_CONTROL_INDEX == attrib_index)
    {
        callback_data.msg_type = SERVICE_CALLBACK_TYPE_WRITE_CHAR_VALUE;
        callback_data.msg_data.write_msg.write_type = FMS_WRITE_CHAR_CONTROL;
        callback_data.msg_data.write_msg.write_parameter.report_data.len = length;
        callback_data.msg_data.write_msg.write_parameter.report_data.report = p_value;
    }
    else if (GATT_SVC_FMS_DATA_INDEX == attrib_index)
    {
        callback_data.msg_type = SERVICE_CALLBACK_TYPE_WRITE_CHAR_VALUE;
        callback_data.msg_data.write_msg.write_type = FMS_WRITE_CHAR_DATA;
        callback_data.msg_data.write_msg.write_parameter.report_data.len = length;
        callback_data.msg_data.write_msg.write_parameter.report_data.report = p_value;
    }

    if ((pfn_fms_cb) && (handle == true))
    {
        pfn_fms_cb(service_id, (void *)&callback_data);
    }

    return cause;

}

/**
 * @brief update CCCD bits from stack.
 *
 * @param ServiceId          Service ID.
 * @param Index          Attribute index of characteristic data.
 * @param wCCCBits         CCCD bits from stack.
 * @return None
*/
void fms_cccd_update_cb(uint8_t conn_id, T_SERVER_ID service_id, uint16_t index,
                        uint16_t ccc_bits)
{
    bool handle = true;
    T_FMS_CALLBACK_DATA callback_data;
    callback_data.msg_type = SERVICE_CALLBACK_TYPE_INDIFICATION_NOTIFICATION;

    APP_PRINT_INFO2("[fms_cccd_update_cb] index = %d, ccc_bits %x", index, ccc_bits);
    switch (index)
    {
    case GATT_SVC_FMS_CCCD_INDEX:
        {
            if (ccc_bits & GATT_CLIENT_CHAR_CONFIG_NOTIFY)
            {
                callback_data.msg_type = SERVICE_CALLBACK_TYPE_INDIFICATION_NOTIFICATION;
                callback_data.msg_data.notification_indification_index = FMS_DATA_NOTIFY_ENABLE;
            }
            else
            {
                callback_data.msg_type = SERVICE_CALLBACK_TYPE_INDIFICATION_NOTIFICATION;
                callback_data.msg_data.notification_indification_index = FMS_DATA_NOTIFY_DISABLE;
            }
            break;
        }
    default:
        {
            handle = false;
            break;
        }
    }

    if (pfn_fms_cb && (handle == true))
    {
        /* Notify Application */
        pfn_fms_cb(service_id, (void *)&callback_data);
    }

    return;
}

/**
 * @brief FMS Service Callbacks.
*/
const T_FUN_GATT_SERVICE_CBS fms_cbs =
{
    fms_attr_read_cb,    // Read callback function pointer
    fms_attr_write_cb,   // Write callback function pointer
    fms_cccd_update_cb   // CCCD update callback function pointer
};

/**
  * @brief       Add FMS service to the BLE stack database.
  *
  *
  * @param[in]   p_func  Callback when service attribute was read, write or cccd update.
  * @return Service id generated by the BLE stack: @ref T_SERVER_ID.
  * @retval 0xFF Operation failure.
  * @retval Others Service id assigned by stack.
  *
  * <b>Example usage</b>
  * \code{.c}
     void profile_init()
     {
         server_init(1);
         bas_id = fms_add_service(app_handle_profile_message);
     }
  * \endcode
  */
uint8_t fms_add_service(void *p_func)
{
    uint8_t service_id;
    if (false == server_add_service(&service_id,
                                    (uint8_t *)fms_svc_attr_tbl,
                                    sizeof(fms_svc_attr_tbl),
                                    fms_cbs))
    {
        APP_PRINT_INFO1("fms_add_service: service_id %d", service_id);
        service_id = 0xff;
        return service_id;
    }
    pfn_fms_cb = (P_FUN_SERVER_GENERAL_CB)p_func;
    return service_id;
}
