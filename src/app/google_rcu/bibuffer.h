/**
*********************************************************************************************************
*               Copyright(c) 2017, Realtek Semiconductor Corporation. All rights reserved.
*********************************************************************************************************
* @file      bibuffer.h
* @brief    header file of double buffer data struct.
* @details
* @author    elliot chen
* @date      2017-06-05
* @version   v1.0
* *********************************************************************************************************
*/

#ifndef __BI_BUFFER_H
#define __BI_BUFFER_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "string.h"
#include "trace.h"

/* Defines ------------------------------------------------------------------*/

/**
 * @brief Enable print log or not
 */

#define PRINT_BI_BUFFER_LOG
#ifdef PRINT_BI_BUFFER_LOG
#define BI_BUFFER(MODULE, LEVEL, pFormat, para_num,...) DBG_BUFFER_##LEVEL(MODULE, pFormat, para_num, ##__VA_ARGS__)
#else
#define BI_BUFFER(MODULE, LEVEL, pFormat, para_num,...) ((void)0)
#endif

/**
 * @brief Configure double buffer parameters
 */

#define BI_BUFFER_MAX_SIZE              (512)

/**
  * @brief  double buffer status
  */

typedef enum
{
    READ_DONE   = 0,
    READING     = 1,
    WRITING     = 2,
    WRITE_DONE  = 3,
} BUF_STATUS;

/**
 * @brief Loop queue data struct
 */

typedef struct
{
    volatile BUF_STATUS buf0_stat;
    volatile BUF_STATUS buf1_stat;
    uint8_t buf0[BI_BUFFER_MAX_SIZE];
    uint8_t buf1[BI_BUFFER_MAX_SIZE];
} BiBuf_TypeDef;

void BiBuffer_Init(void);
uint32_t BiBuffer_GetBuf0Addr(void);
void BiBuffer_SetBuf0Status(BUF_STATUS status);
BUF_STATUS BiBuffer_GetBuf0Status(void);
uint32_t BiBuffer_GetBuf1Addr(void);
void BiBuffer_SetBuf1Status(BUF_STATUS status);
BUF_STATUS BiBuffer_GetBuf1Status(void);

#ifdef __cplusplus
}
#endif

#endif /*__BI_BUFFER_H */

/******************* (C) COPYRIGHT 2017 Realtek Semiconductor Corporation *****END OF FILE****/

