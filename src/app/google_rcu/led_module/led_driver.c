/**
*********************************************************************************************************
*               Copyright(c) 2020, Realtek Semiconductor Corporation. All rights reserved.
**********************************************************************************************************
* @file     led_driver.c
* @brief    this file provides led module function driver for users.
* @details  multi prio, multi event, multi led support.
* @author   Yuyin_zhang
* @date     2020-03-03
* @version  v1.1
* @note     the more led number and led event, the more time cpu spend on tick handle
*           For example: led_num = 8, led_event_num = 10, max time is 112us.
*********************************************************************************************************
*/

/*============================================================================*
 *                              Header Files
 *============================================================================*/
#include "board.h"
#include "trace.h"
#include "os_timer.h"
#include "swtimer.h"
#include "led_driver.h"
#include "led_pwm_driver.h"
#include <app_section.h>

#if SUPPORT_LED_INDICATION_FEATURE
/*============================================================================*
 *                              Local Variables
 *============================================================================*/
static const T_LED_EVENT_STG led_event_arr[LED_TYPE_MAX] =
{
    {LED_COLOR_RED,   LED_DEFAULT_BRIGHTNESS_DUTY, LED_TYPE_BLINK_ERROR,              LED_LOOP_BITS_ERROR,              LED_BIT_MAP_ERROR},
    {LED_COLOR_GREEN, LED_DEFAULT_BRIGHTNESS_DUTY, LED_TYPE_BLINK_KEY_PRESS_BLE,      LED_LOOP_BITS_KEY_PRESS_BLE,      LED_BIT_MAP_KEY_PRESS_BLE},
    {LED_COLOR_RED,   LED_DEFAULT_BRIGHTNESS_DUTY, LED_TYPE_BLINK_KEY_PRESS_IR,       LED_LOOP_BITS_KEY_PRESS_IR,       LED_BIT_MAP_KEY_PRESS_IR},
    {LED_COLOR_ORANGE, LED_DEFAULT_BRIGHTNESS_DUTY, LED_TYPE_BLINK_KEY_PRESS_PROG,     LED_LOOP_BITS_KEY_PRESS_PROG,     LED_BIT_MAP_KEY_PRESS_PROG},
    {LED_COLOR_GREEN, LED_DEFAULT_BRIGHTNESS_DUTY, LED_TYPE_BLINK_COMB_KEY_PRESS,     LED_LOOP_BITS_COMB_KEY_PRESS,     LED_BIT_MAP_COMB_KEY_PRESS},
    {LED_COLOR_GREEN, LED_DEFAULT_BRIGHTNESS_DUTY, LED_TYPE_BLINK_CONFIRMATION,       LED_LOOP_BITS_CONFIRMATION,       LED_BIT_MAP_CONFIRMATION},
    {LED_COLOR_GREEN, LED_DEFAULT_BRIGHTNESS_DUTY, LED_TYPE_BLINK_PAIR_ADV,           LED_LOOP_BITS_PAIR_ADV,           LED_BIT_MAP_PAIR_ADV},
    {LED_COLOR_GREEN, LED_DEFAULT_BRIGHTNESS_DUTY, LED_TYPE_BLINK_VOICE_HTT,          LED_LOOP_BITS_VOICE_HTT,          LED_BIT_MAP_VOICE_HTT},
    {LED_COLOR_GREEN, LED_DEFAULT_BRIGHTNESS_DUTY, LED_TYPE_BLINK_VOICE_PTT,          LED_LOOP_BITS_VOICE_PTT,          LED_BIT_MAP_VOICE_PTT},
    {LED_COLOR_GREEN, LED_DEFAULT_BRIGHTNESS_DUTY, LED_TYPE_BLINK_VOICE_ON_REQUEST,   LED_LOOP_BITS_VOICE_ON_REQUEST,   LED_BIT_MAP_VOICE_ON_REQUEST},
    {LED_COLOR_RED,   LED_DEFAULT_BRIGHTNESS_DUTY, LED_TYPE_BLINK_TEST_RED,           LED_LOOP_BITS_TEST_RED,           LED_BIT_MAP_TEST_RED},
    {LED_COLOR_GREEN, LED_DEFAULT_BRIGHTNESS_DUTY, LED_TYPE_BLINK_TEST_GREEN,         LED_LOOP_BITS_TEST_GREEN,         LED_BIT_MAP_TEST_GREEN},
    {LED_COLOR_GREEN, LED_DEFAULT_BRIGHTNESS_DUTY, LED_TYPE_ON,                       LED_LOOP_BITS_ON,                 LED_BIT_MAP_ON},
};

static uint32_t led_global_tick_cnt = 0;
static T_LED_DATA_STG led_ctrl_data = {0};
static TimerHandle_t led_ctrl_timer;

/*============================================================================*
 *                              Functions Declaration
 *============================================================================*/
static uint32_t led_get_next_event(const uint32_t bitmap);
static void led_ctrl_timer_cb(TimerHandle_t pxTimer) DATA_RAM_FUNCTION;

/*============================================================================*
 *                              Local Functions
 *============================================================================*/
uint32_t get_led_brightness_duty(uint8_t led_index, LED_TYPE type)
{
    uint32_t led_brightness_duty = LED_DEFAULT_BRIGHTNESS_DUTY;
    if (led_event_arr[type].led_color == LED_COLOR_ORANGE)
    {
        if (led_index == LED_R_INDEX)
        {
            led_brightness_duty = LED_DEFAULT_BRIGHTNESS_DUTY;
        }
        else if (led_index == LED_G_INDEX)
        {
            led_brightness_duty = LED_DEFAULT_BRIGHTNESS_DUTY;
        }
        else if (led_index == LED_B_INDEX)
        {
            led_brightness_duty = 0;
        }
    }
    else
    {
        led_brightness_duty = led_event_arr[type].led_brightness;
    }

    return led_brightness_duty;
}

/******************************************************************
 * @brief   get next led blink event.
 * @param   bitmap - led event bit map, indicate which led event bit map to get.
 * @return  return led event index.
 * @retval  uint32_t
 */
uint32_t led_get_next_event(uint32_t bitmap)
{
    for (uint32_t index = 0; index < LED_TYPE_MAX; index++)
    {
        uint32_t bit_mask = 0;
        bit_mask |= (1 << index);
        if (bit_mask & bitmap)
        {
#if LED_DEBUG
            APP_PRINT_INFO1("[led_get_next_event] index = %d", index);
#endif
            return index;
        }
    }
    return LED_TYPE_MAX;
}

/******************************************************************
 * @brief   callback led timer timeout.
 * @param   pxTimer.
 * @return  none
 * @retval  void
 */
void led_ctrl_timer_cb(TimerHandle_t pxTimer)
{
    /* update led blink tick */
    led_global_tick_cnt++;

    /* get led type */
    uint32_t type = led_get_next_event(led_ctrl_data.led_map);

    if (type < LED_TYPE_MAX)
    {
        /* process current led event */
        uint32_t map = led_event_arr[type].led_bit_map;
        uint32_t tick_mask = (1 << (led_global_tick_cnt % led_event_arr[type].led_loop_cnt));
        LED_COLOR_LIST led_color = led_event_arr[type].led_color;

        for (uint8_t led_index = 0; led_index < LED_NUM_MAX; led_index++)
        {
            if (((tick_mask & map)) && (led_color & (1 << led_index)))
            {
                uint32_t pwm_high_cnt = 0;
                uint32_t pwm_low_cnt = 0;
                uint32_t led_brightness_duty = get_led_brightness_duty(led_index, type);
#if (LED_ACTIVE_LEVEL == 1)
                pwm_high_cnt = led_brightness_duty * LED_DUTY_FACTOR;
                pwm_low_cnt = PWM_MAX_COUNT - pwm_high_cnt;
#else
                pwm_high_cnt = PWM_MAX_COUNT - led_brightness_duty * LED_DUTY_FACTOR;
                pwm_low_cnt = PWM_MAX_COUNT - pwm_high_cnt;
#endif

                led_module_pwm_start(led_index, pwm_high_cnt, pwm_low_cnt);
            }
            else
            {
                led_module_pwm_stop(led_index);
            }
        }

        /* update led_ctrl_data */
        for (uint8_t index = 0; index < LED_TYPE_MAX; index++)
        {
            /* update led state, when new tick come */
            if (0 == ((1 << index) & led_ctrl_data.led_map))
            {
                continue;
            }

            if ((led_ctrl_data.led_cnt_arr[index].led_loop_cnt > 0)
                && (led_ctrl_data.led_cnt_arr[index].cur_tick_cnt == (led_global_tick_cnt %
                                                                      led_event_arr[index].led_loop_cnt)))
            {
                led_ctrl_data.led_cnt_arr[index].led_loop_cnt --;
                if (0 == led_ctrl_data.led_cnt_arr[index].led_loop_cnt)
                {
                    /* clear event bit */
                    led_ctrl_data.led_map &= ~(1 << index);
                    /* turn off led */
#ifdef LED_R_PIN
                    led_module_pwm_stop(LED_R_INDEX);
#endif
#ifdef LED_G_PIN
                    led_module_pwm_stop(LED_G_INDEX);
#endif
#ifdef LED_B_PIN
                    led_module_pwm_stop(LED_B_INDEX);
#endif
                }
            }
        }
    }

    if (led_ctrl_data.led_map)
    {
        /* led timer restart */
        os_timer_restart(&led_ctrl_timer, LED_PERIOD);
    }

#if LED_DEBUG
    APP_PRINT_INFO3("[led_ctrl_timer_cb] led_global_tick_cnt = %d, type = %d, led_map = 0x%02X",
                    led_global_tick_cnt, type, led_ctrl_data.led_map);
#endif
}

/*============================================================================*
 *                              Global Functions
 *============================================================================*/
/******************************************************************
 * @brief   led module init.
 * @param   none
 * @return  none
 * @retval  void
 */
void led_module_init(void)
{
    bool retval = false;

    led_pwm_init();
    led_global_tick_cnt = 0;

    /* create led tick timer */
    retval = os_timer_create(&led_ctrl_timer, "led_ctrl_timer", 1, LED_PERIOD, false,
                             led_ctrl_timer_cb);
    if (!retval)
    {
        APP_PRINT_INFO1("[Led] led_ctrl_timer ret is %d", retval);
    }
}

/******************************************************************
 * @brief    led driver check DLPS callback
 * @param    none
 * @return   bool
 * @retval   true or false
 */
bool led_driver_dlps_check(void)
{
    return led_pwm_driver_dlps_check();
}

/******************************************************************
 * @brief   start led event.
 * @param   led_color - led color.
 * @param   type - led type to set.
 * @param   cnt  - led blink count.
 * @return  result of starting led.
 * @retval  T_LED_RET_CAUSE
 */
T_LED_RET_CAUSE led_blink_start(LED_TYPE type, uint8_t cnt)
{
    uint32_t pre_led_map = led_ctrl_data.led_map;

    if (LED_TYPE_MAX <= type)
    {
        return LED_ERR_TYPE;
    }

    if (led_event_arr[type].led_bit_map == LED_BIT_MAP_INVALID)
    {
        return LED_ERR_TYPE;
    }

    /* set led event map flag */
    led_ctrl_data.led_map |= (1 << type);

    if (cnt > 0)
    {
        led_ctrl_data.led_cnt_arr[type].led_loop_cnt = cnt;
        led_ctrl_data.led_cnt_arr[type].cur_tick_cnt = led_global_tick_cnt %
                                                       led_event_arr[type].led_loop_cnt;
    }
    else
    {
        led_ctrl_data.led_cnt_arr[type].led_loop_cnt = 0;
        led_ctrl_data.led_cnt_arr[type].cur_tick_cnt = 0;
    }

    /* start led tick */
    if ((pre_led_map == 0) && (led_ctrl_data.led_map != 0))
    {
        led_ctrl_timer_cb(&led_ctrl_timer);
    }

#if LED_DEBUG
    APP_PRINT_INFO2("[led_blink_start] type = %d, cnt = %d", type, cnt);
#endif

    return LED_SUCCESS;

}

/******************************************************************
 * @brief   this function terminate led blink according to the led type.
 * @param   led_color - led color.
 * @param   type - led type to set.
 * @return  result of exiting led.
 * @retval  T_LED_RET_CAUSE
 */
T_LED_RET_CAUSE led_blink_exit(LED_TYPE type)
{
    if (LED_TYPE_MAX <= type)
    {
        return LED_ERR_TYPE;
    }

    /* stop led */
    if (led_ctrl_data.led_map & (1 << type))
    {
#if LED_DEBUG
        APP_PRINT_INFO1("[led_blink_exit] type = %d", type);
#endif
        /* clear current led type bit */
        led_ctrl_data.led_map &= ~(1 << type);

        /* check whether need close led timer */
        if (led_ctrl_data.led_map == 0)
        {
            os_timer_stop(&led_ctrl_timer);
#ifdef LED_R_PIN
            led_module_pwm_stop(LED_R_INDEX);
#endif
#ifdef LED_G_PIN
            led_module_pwm_stop(LED_G_INDEX);
#endif
#ifdef LED_B_PIN
            led_module_pwm_stop(LED_B_INDEX);
#endif
        }

        return LED_SUCCESS;
    }
    return LED_SUCCESS;
}

#endif

/******************* (C) COPYRIGHT 2020 Realtek Semiconductor Corporation *****END OF FILE****/
