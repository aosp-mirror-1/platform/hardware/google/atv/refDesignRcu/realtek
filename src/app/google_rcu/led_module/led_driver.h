/**
*********************************************************************************************************
*               Copyright(c) 2020, Realtek Semiconductor Corporation. All rights reserved.
**********************************************************************************************************
* @file     led_driver.h
* @brief
* @details
* @author
* @date     2020-03-04
* @version  v1.0
* @note
*********************************************************************************************************
*/
#ifndef _LED_H_
#define _LED_H_

#include "stdint.h"
#include "board.h"

#if SUPPORT_LED_INDICATION_FEATURE
/**
*   @brief user guide for led driver
*   The driver support multi prio, multi event, and multi led.
*   If you want to control a led with a event as you designed, there is two way to
*   achieve this, modify the exist event and redefine a new event.
*
*   The led driver is drived by software timer, the timer period is 50ms by default.
*
*   #define  LED_PERIOD   50 //you a change the value according your requirement
*
*   For the whole led driver system, there is 5 place you might modify.
*
*   1. Define led num and led pin, which is in file board.h;
*      For example, define 2 leds as followed:
*      #define  LED_NUM_MAX   0x02
*      #define  LED_INDEX(n)   (n<<8)

*      //uint16_t, first byte led index, last byte led pin
*      #define  LED_1         (LED_INDEX(0) | P2_2)
*      #define  LED_2         (LED_INDEX(1) | P0_2)
*
*   2. Define led type index, which is in file led_driver.h, these types must be
*      defined in sequence.
*      For example,
*      #define  LED_TYPE_BLINK_OTA_UPDATE       (8)
*
*   3. Define led loop bits num, in file led_driver.h
*      //max value 32, min value 1, it indicate that there is how many bits to loop
*      //from LSB
*      #define LED_LOOP_BITS_OTA_UPDATE          (15)
*
*   4. Define led event bit map, in file led_driver.h
*      // it must be cooperated with led loop bits num
*      #define  LED_BIT_MAP_OTA_UPDATE       (0x4210)//on 50ms, off 200ms, period 50ms
*
*   5. Update led event table, led_event_arr[LED_TYPE_MAX], in file led_driver.c
*      Before you use the event you define ,you need to add led type, led loop bit num,
*      and led event bit map into event table.
*      const T_LED_EVENT_STG led_event_arr[LED_TYPE_MAX] =
*      {
*                ··· ···
*        {LED_TYPE_BLINK_OTA_UPDATE, LED_LOOP_BITS_OTA_UPDATE,  LED_BIT_MAP_OTA_UPDATE},
*      };
*
*   There are three interfaces for Led driver, as follow.
*
*   void led_module_init(void); // called when system boot;
*   T_LED_RET_CAUSE led_blink_start(uint16_t index, LED_TYPE type, uint8_t cnt);
*   T_LED_RET_CAUSE led_blink_exit(uint16_t index, LED_TYPE type);
*/

#define  LED_DEBUG                      0
/*software timer period*/
#define  LED_PERIOD                     50  /*50ms*/
/*led default duty for brightness*/
#define  LED_DEFAULT_BRIGHTNESS_DUTY    LED_DEFAULT_DUTY
/*led index*/
#define  LED_R_INDEX    0
#define  LED_G_INDEX    1
#define  LED_B_INDEX    2

/*led event types, the less value, the higher prio of the event*/
typedef   uint8_t      LED_TYPE;

typedef enum
{
    LED_TYPE_BLINK_ERROR,
    LED_TYPE_BLINK_KEY_PRESS_BLE,
    LED_TYPE_BLINK_KEY_PRESS_IR,
    LED_TYPE_BLINK_KEY_PRESS_PROG,
    LED_TYPE_BLINK_COMB_KEY_PRESS,
    LED_TYPE_BLINK_CONFIRMATION,
    LED_TYPE_BLINK_PAIR_ADV,
    LED_TYPE_BLINK_VOICE_HTT,
    LED_TYPE_BLINK_VOICE_PTT,
    LED_TYPE_BLINK_VOICE_ON_REQUEST,
    LED_TYPE_BLINK_TEST_RED,
    LED_TYPE_BLINK_TEST_GREEN,
    LED_TYPE_ON,
    LED_TYPE_MAX
} LED_TYPE_ENUM;

/*led loop bit num for each event, max value 32*/
#define   LED_LOOP_BITS_ERROR                     2
#define   LED_LOOP_BITS_CONFIRMATION              4
#define   LED_LOOP_BITS_PAIR_ADV                  10
#define   LED_LOOP_BITS_VOICE_HTT                 32
#define   LED_LOOP_BITS_VOICE_PTT                 20
#define   LED_LOOP_BITS_VOICE_ON_REQUEST          32
#define   LED_LOOP_BITS_KEY_PRESS_BLE             4
#define   LED_LOOP_BITS_KEY_PRESS_IR              4
#define   LED_LOOP_BITS_KEY_PRESS_PROG            4
#define   LED_LOOP_BITS_COMB_KEY_PRESS            32
#define   LED_LOOP_BITS_TEST_RED                  24
#define   LED_LOOP_BITS_TEST_GREEN                24
#define   LED_LOOP_BITS_ON                        32

/*led bit map 32bits, High bits(low priority) ---  Low bits(high priority) */
#define LED_BIT_MAP_ERROR                  0x00000002
#define LED_BIT_MAP_CONFIRMATION           0x0000000C
#define LED_BIT_MAP_PAIR_ADV               0x0000001f
#define LED_BIT_MAP_VOICE_HTT              0xffffffff
#define LED_BIT_MAP_VOICE_PTT              0x000003ff
#define LED_BIT_MAP_VOICE_ON_REQUEST       0xffffffff
#define LED_BIT_MAP_KEY_PRESS_BLE          0x0000000C
#define LED_BIT_MAP_KEY_PRESS_IR           0x0000000C
#define LED_BIT_MAP_KEY_PRESS_PROG         0x0000000C
#define LED_BIT_MAP_COMB_KEY_PRESS         0xffffffff
#define LED_BIT_MAP_TEST_RED               0x000000ff
#define LED_BIT_MAP_TEST_GREEN             0x0000ff00
#define LED_BIT_MAP_ON                     0xffffffff

#define LED_BIT_MAP_INVALID                0x00000000


/* LED Color mask bit*/
#define  LED_MASK_RED   0x01 /**< 0001b(0x01) */
#define  LED_MASK_GREEN 0x02 /**< 0010b(0x02) */
#define  LED_MASK_BLUE  0x04 /**< 0100b(0x04) */

/**
 * @anchor LED_COLOR_LIST
 * @name LED color list
 * LSB 0 : bit for RED LED
 * LSB 1 : bit for GREEN LED
 * LSB 2 : bit for BLUE LED
 * LSB 3 - 7 : May need to control brightness of each LED
 * @{ */
typedef enum
{
    LED_COLOR_INVALID = 0x00,  /**< invalid */
    LED_COLOR_RED     = 0x01, /**< red,       0001b(0x01) */
    LED_COLOR_GREEN   = 0x02, /**< green,     0010b(0x02) */
    LED_COLOR_YELLOW  = 0x03, /**< yellow,    0011b(0x03), sRGB(255,255,0) */
    LED_COLOR_ORANGE   = 0x13, /**< orange,     10011b(0x13), sRGB(255,128,0) */
    LED_COLOR_MAX_OPTIONS = 0x14, /**< led color value should be less than 0x14 */
} LED_COLOR_LIST;

typedef struct
{
    LED_COLOR_LIST led_color;
    uint32_t   led_brightness;
    uint8_t   led_type_index;
    uint8_t   led_loop_cnt;
    uint32_t  led_bit_map;
} T_LED_EVENT_STG;

/*struct support for led blink count*/
typedef struct
{
    uint8_t led_loop_cnt;
    uint8_t cur_tick_cnt;
} T_LED_CNT_STG;

/*support for each led data*/
typedef struct
{
    T_LED_CNT_STG led_cnt_arr[LED_TYPE_MAX];
    uint32_t led_map;
} T_LED_DATA_STG;

/*led return code*/
typedef enum
{
    LED_SUCCESS                      = 0,
    LED_ERR_TYPE                     = 1,
    LED_ERR_INDEX                    = 2,
    LED_ERR_CODE_MAX
} T_LED_RET_CAUSE;

/*============================================================================*
 *                        <Led Module Interface>
 *============================================================================*/
void led_module_init(void);
bool led_driver_dlps_check(void);
T_LED_RET_CAUSE led_blink_start(LED_TYPE type, uint8_t cnt);
T_LED_RET_CAUSE led_blink_exit(LED_TYPE type);

#define LED_BLINK(type, n)       led_blink_start(type, n)
#define LED_BLINK_EXIT(type)     led_blink_exit(type)

#endif

#endif

/******************* (C) COPYRIGHT 2020 Realtek Semiconductor Corporation *****END OF FILE****/
