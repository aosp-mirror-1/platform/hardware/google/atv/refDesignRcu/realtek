/**
*****************************************************************************************
*     Copyright(c) 2018, Realtek Semiconductor Corporation. All rights reserved.
*****************************************************************************************
   * @file      app_flags.h
   * @brief     This file is used to config app functions.
   * @author    berni
   * @date      2018-04-27
   * @version   v1.0
   **************************************************************************************
   * @attention
   * <h2><center>&copy; COPYRIGHT 2018 Realtek Semiconductor Corporation</center></h2>
   **************************************************************************************
  */
#ifndef _APP_FLAGS_H_
#define _APP_FLAGS_H_

#include "upperstack_config.h"

/** @defgroup  BT5_CENTRAL_Config BT5 Central App Configuration
    * @brief This file is used to config app functions.
    * @{
    */
/*============================================================================*
 *                              Constants
 *============================================================================*/
/** @brief Configure APP LE link number */
#define APP_MAX_LINKS  1

#define LE_AUDIO_LIB_SUPPORT 1
#define BT_GATT_CLIENT_SUPPORT 1
#define LE_AUDIO_BROADCAST_ASSISTANT_ROLE 1

/** @} */ /* End of group BT5_CENTRAL_Config */

#endif
