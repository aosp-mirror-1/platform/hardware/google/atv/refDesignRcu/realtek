/**
*****************************************************************************************
*     Copyright(c) 2018, Realtek Semiconductor Corporation. All rights reserved.
*****************************************************************************************
   * @file      link_mgr.h
   * @brief     Define struct and functions about link.
   * @author    berni
   * @date      2018-04-27
   * @version   v1.0
   **************************************************************************************
   * @attention
   * <h2><center>&copy; COPYRIGHT 2018 Realtek Semiconductor Corporation</center></h2>
   **************************************************************************************
  */
#ifndef _LINK_MANAGER_H_
#define _LINK_MANAGER_H_
/*============================================================================*
 *                              Header Files
 *============================================================================*/
#include "gap_conn_le.h"
#include "app_msg.h"
#include "ble_audio_def.h"
#include "bass_def.h"

/*============================================================================*
 *                              Constants
 *============================================================================*/
/** @brief  Define device list table size. */
#define APP_MAX_DEVICE_INFO 30

typedef struct
{
    bool                used;
    T_GAP_CONN_STATE    state;
#if BT_GATT_CLIENT_SUPPORT
    bool                mtu_received;
    bool                auth_cmpl;
#endif
#if LE_AUDIO_BROADCAST_ASSISTANT_ROLE
    uint8_t             source_id;
    uint8_t             brs_char_num;
    T_PA_SYNC_STATE     pa_sync_state;
    uint32_t            bis_sync_state;
#endif
    uint8_t             conn_id;
    uint16_t            conn_handle;
    uint16_t            mtu_size;
    uint8_t             remote_bd_type;
    uint8_t             remote_bd[GAP_BD_ADDR_LEN];  /* current remote BD */
} T_BLE_LINK;

typedef struct
{
    T_BLE_LINK le_link[APP_MAX_LINKS];
} T_APP_DB;

/** @addtogroup  BT5_CENTRAL_SCAN_MGR
    * @{
    */
/** @brief  Extended Scan Mode List.*/
typedef enum
{
    SCAN_UNTIL_DISABLED, /**< If Duration parameter is zero, continue scanning until scanning is disabled. */
    SCAN_UNTIL_DURATION_EXPIRED, /**< If Duration parameter is non-zero and Period parameter is zero, continue scanning until duration has expired. */
} T_EXT_SCAN_MODE;

/**
 * @brief  Device list block definition.
 */
typedef struct
{
    uint8_t      bd_addr[GAP_BD_ADDR_LEN];  /**< remote BD */
    uint8_t      bd_type;              /**< remote BD type*/
    uint8_t      adv_sid;
    uint8_t      idx;
    uint16_t     uuid16;
    uint8_t      broadcast_id[BROADCAST_ID_LEN];
} T_DEV_INFO;
/** @} */

/*============================================================================*
 *                              Variables
 *============================================================================*/
/** @brief  Device list table, used to save discovered device informations. */
extern T_DEV_INFO dev_list[APP_MAX_DEVICE_INFO];
/** @brief  The number of device informations saved in dev_list. */
extern uint8_t dev_list_count;
extern T_APP_DB app_db;

/*============================================================================*
 *                              Functions
 *============================================================================*/
/**
* @brief   Add device information to device list.
*
* @param[in] bd_addr Peer device address.
* @param[in] bd_type Peer device address type.
* @retval true Success.
* @retval false Failed, device list is full.
*/
bool link_mgr_add_device(uint8_t *bd_addr, uint8_t bd_type, T_DEV_INFO **pp_dev);

/**
 * @brief Clear device list.
 * @retval void
 */
void link_mgr_clear_device_list(void);

T_BLE_LINK *ble_link_find_by_conn_id(uint8_t conn_id);
T_BLE_LINK *ble_link_find_by_conn_handle(uint8_t conn_handle);
T_BLE_LINK *ble_link_alloc_by_conn_id(uint8_t conn_id);
T_BLE_LINK *ble_link_find_by_addr(uint8_t *bd_addr, uint8_t bd_type);
bool ble_link_free(T_BLE_LINK *p_link);

#endif
