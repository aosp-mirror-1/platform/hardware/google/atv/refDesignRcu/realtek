/**
*****************************************************************************************
*     Copyright(c) 2017, Realtek Semiconductor Corporation. All rights reserved.
*****************************************************************************************
   * @file      ble_audio_ba_app.h
   * @brief     This file handles BLE BT5 central application routines.
   * @author    jane
   * @date      2017-06-06
   * @version   v1.0
   **************************************************************************************
   * @attention
   * <h2><center>&copy; COPYRIGHT 2017 Realtek Semiconductor Corporation</center></h2>
   **************************************************************************************
  */

#ifndef _BLE_AUDIO_BA_ROLE_H_
#define _BLE_AUDIO_BA_ROLE_H_

#ifdef __cplusplus
extern "C" {
#endif
#include "bass_def.h"

bool app_ble_audio_profile_init(void);
void app_ble_audio_handle_adv_report(T_LE_EXT_ADV_REPORT_INFO *p_report);
bool app_ble_audio_pa_sync(T_DEV_INFO *p_dev_info);
bool app_ble_audio_stop_pa_sync(bool need_release);
//bass cp
bool app_ble_audio_add_source(T_BLE_LINK *p_link, T_BASS_PA_SYNC pa_sync, uint32_t bis_array);
bool app_ble_audio_modify_source(T_BLE_LINK *p_link, T_BASS_PA_SYNC pa_sync, uint32_t bis_array);

#ifdef __cplusplus
}
#endif

#endif

